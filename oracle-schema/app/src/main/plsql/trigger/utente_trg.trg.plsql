create or replace trigger utente_trg 
before insert or update of mail on utente 
for each row 
begin
  :new.mail := lower(trim(:new.mail));
end;
/