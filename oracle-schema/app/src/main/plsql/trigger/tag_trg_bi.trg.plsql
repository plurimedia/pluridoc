create or replace trigger trg_tags_bi before insert on tags for each row
begin
   if inserting then
      if :new.tag_id is null then
         select seq_tag.nextval into :new.tag_id from dual;
         :new.nome := lower(trim(:new.nome));
      end if;
   end if;
end;
/
