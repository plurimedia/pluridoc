create or replace view v_documento_path as
SELECT 
    p.cid||'/'||p.canale_id||'/'||d."NAME" percorso, p.azienda_id
FROM v_cartella_path p, documento d
where d.cartella_id = p.cartella_id
union
select p.cid || '/' || p.canale_id || '/' || d.name_compressed, p.azienda_id
  from v_cartella_path p, documento d
 where d.cartella_id = p.cartella_id
   and d.name_compressed is not null
/