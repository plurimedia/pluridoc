create or replace view v_report_sintesi_mese as
select r.canali, r.documenti, r.report_sintesi_id, r.utenti, r.cdate, r.azienda_id,
       to_number(substr(r.report_sintesi_id, 5, 2)) mese, to_number(substr(r.report_sintesi_id, 1, 4)) anno
  from report_sintesi r 
 where r.cdate in (select max(r2.cdate)
                     from report_sintesi r2
                    where r2.azienda_id = r.azienda_id
                    group by to_char(r2.cdate, 'yyyymm'))
/