create or replace view v_cartella_path as
 select car.cartella_id, car.parent_id, car.nome, level lv, car.canale_id, can.azienda_id,
        azi.cid, substr(sys_connect_by_path(car.nome, '/'), 2) percorso
   from cartella car, canale can, azienda azi
  where car.canale_id  = can.canale_id
    and can.azienda_id = azi.azienda_id
  start with car.parent_id is null
connect by prior car.cartella_id = car.parent_id
  order siblings by lower(car.nome)
/