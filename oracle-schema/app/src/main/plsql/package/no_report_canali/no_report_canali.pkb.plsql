create or replace package body no_report_canali as

   type id_table is table of number;

   function lazy_read(p_id in number)
     return pljson as

      v_json pljson;

   begin

      for c_cur in (select c.* ,
                    (select count(*)
                       from utenti_canali uc, utente u
                      where uc.canale_id = c.canale_id
                        and uc.utente_id = u.utente_id
                        and u.azienda_id = ng.g_azienda_id
                        and u.blocked    = 0) n_utenti,
                    nvl((select sum(doc_size) from documento where canale_id=c.canale_id), 0) total_size,
                    (select sum(d.doc_size)
                       from documento d, canale n
                      where n.canale_id  = d.canale_id
                        and n.azienda_id = ng.g_azienda_id) total_channels_size
                    from canale c
                    where canale_id = p_id) loop

         v_json:= pljson();
         v_json.put('canale_id', c_cur.canale_id);
         v_json.put('nome', c_cur.nome);
         v_json.put('total_size', c_cur.total_size);
         v_json.put('n_utenti', c_cur.n_utenti);
         v_json.put('total_channels_size', c_cur.total_channels_size);

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2,
                  p_out  out clob) as

      v_json_list pljson_list:= pljson_list();
      l_pars      pljson:= pljson(p_pars);
      v_ids       id_table := id_table();

   begin

      select canale_id
        bulk collect into v_ids
        from canale
       where azienda_id = ng.g_azienda_id
       order by lower(nome);

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_json_list.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_json_list.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id  in number,
                  p_out out clob) as

      v_json pljson:= lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

end no_report_canali;
/