create or replace package body no_azienda as

   type id_table is table of number;

   function get_json_string(p_j    in pljson,
                            p_name in varchar2)
     return varchar2 as
   begin
     return p_j.get(p_name).get_string;
     exception when others then return null;
   end get_json_string;

   function lazy_read(p_id in number)
     return pljson as

      v_json pljson;
      v_cnt  number;

   begin

      for c_cur in (select *
                      from azienda
                     where azienda_id = p_id) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.azienda_id);
         v_json.put('cid', c_cur.cid);
         v_json.put('footer', c_cur.footer);
         v_json.put('shortname', c_cur.shortname);
         v_json.put('mail_account', c_cur.mail_account);
         v_json.put('logo_login', c_cur.logo_login);
         v_json.put('mail_from', c_cur.mail_from);
         v_json.put('mail_from_name', c_cur.mail_from_name);

         select count(*)
           into v_cnt
           from canale
          where pubblico   = 1
            and azienda_id = p_id;

         v_json.put('pubblici', v_cnt);

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2,
                  p_out  out clob) as

      v_jsonl pljson_list:= pljson_list();
      l_pars  pljson:= pljson(p_pars);
      v_ids   id_table := id_table();
      l_cid   varchar2(250);

   begin

      l_cid := get_json_string(l_pars, 'cid');
      
      select azienda_id
        bulk collect into v_ids
        from azienda
       where l_cid = cid;

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_jsonl.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id  in number,
                  p_out out clob) as

      v_json pljson:= lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

   procedure create_(p_obj  in varchar2,
                     p_json out varchar2) as

      v_rec  azienda%rowtype;
      v_json pljson:= pljson(p_obj);

   begin

      v_rec.azienda_id := seq_azienda.nextval;
      v_rec.cid := get_json_string(v_json, 'cid');
      v_rec.footer := get_json_string(v_json, 'footer');
      v_rec.shortname := get_json_string(v_json, 'shortname');
      v_rec.mail_account := get_json_string(v_json, 'mail_account');
      v_rec.logo_login := get_json_string(v_json, 'logo_login');

      insert into azienda
      values v_rec
      return azienda_id into v_rec.azienda_id;

      v_json.put('_id', v_rec.azienda_id);
      p_json := v_json.to_char();

   end create_;

   procedure update_(p_id   in number,
                     p_json in out varchar2) as

      v_rec  azienda%rowtype;
      v_json pljson:= pljson(p_json);

   begin

      select * into v_rec
        from azienda
       where azienda_id = p_id;

      v_rec.cid := get_json_string(v_json, 'cid');
      v_rec.footer := get_json_string(v_json, 'footer');
      v_rec.shortname := get_json_string(v_json, 'shortname');
      v_rec.mail_account := get_json_string(v_json, 'mail_account');
      v_rec.logo_login := get_json_string(v_json, 'logo_login');

      update azienda
         set row = v_rec
       where azienda_id = p_id;

   end update_;

   procedure delete_(p_id in number) as
   begin

      delete azienda
       where azienda_id = p_id;

   end delete_;

end no_azienda;
/