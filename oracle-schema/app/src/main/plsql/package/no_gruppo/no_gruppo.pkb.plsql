create or replace package body no_gruppo as

   type id_table is table of number;

   function get_json_string(p_j in pljson, p_name in varchar2)
     return varchar2
   as
   begin
     return p_j.get(p_name).get_string;
     exception when others then 
       begin
         return replace(p_j.get(p_name).to_char, '"', '');
       exception when others then
         return null;
       end;
   end;

   function lazy_read(p_id in number)
     return pljson
   as

      v_json pljson;

   begin

      for c_cur in (select *
                      from gruppo
                     where gruppo_id = p_id) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.gruppo_id);
         v_json.put('nome', c_cur.nome);
         v_json.put('tipo', c_cur.tipo);
         v_json.put('visibile', c_cur.visibile);
         v_json.put('cdate', to_char(c_cur.cdate, 'dd/mm/yyyy'));

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2, p_out out clob)
   as

      v_json_list pljson_list:= pljson_list();
      l_pars      pljson:= pljson(p_pars);
      v_ids       id_table := id_table();

   begin

      select gruppo_id
        bulk collect into v_ids
        from gruppo
       where azienda_id = ng.g_azienda_id;

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_json_list.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_json_list.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id  in number,
                  p_out out clob) as

      v_json pljson:= lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

   procedure create_(p_obj  in clob,
                     p_json out varchar2) as

      v_rec gruppo%rowtype;
      v_json pljson:= pljson(p_obj);
      v_utenti pljson_list;

   begin

      v_rec.gruppo_id := seq_gruppo.nextval;
      v_rec.nome := get_json_string(v_json, 'nome');
      v_rec.tipo := get_json_string(v_json, 'tipo');
      v_rec.azienda_id := ng.g_azienda_id;
      v_rec.cdate := sysdate;

      begin
         v_rec.visibile := nvl(v_json.get('visibile').get_number, 0);
         exception
            when others then
               if nvl(get_json_string(v_json, 'visibile'), 'false') = 'true' then
                  v_rec.visibile := 1;
               else
                  v_rec.visibile := 0;
               end if;
      end;

      insert into gruppo
      values v_rec
      return gruppo_id into v_rec.gruppo_id;

      insert into gruppi_canali
             (gruppo_id,       canale_id)
      values (v_rec.gruppo_id, v_json.get('canale_id').get_number);

      if v_rec.tipo = 'UTENTE' then --se è un gruppo di utenti va registrata la relazione col canale e quella con gli utenti

         v_utenti := pljson_list(v_json.get('utenti'));

         for i in 1..v_utenti.count loop

            insert into gruppi_utenti
                   (gruppo_id,       utente_id)
            values (v_rec.gruppo_id, v_utenti.get(i).get_number);

         end loop;

      end if;

      v_json.put('_id', v_rec.gruppo_id);
      p_json := v_json.to_char();

   end create_;

   procedure update_(p_id   in number,
                     p_json in out varchar2) as

      v_rec    gruppo%rowtype;
      v_json   pljson:= pljson(p_json);
      v_utenti pljson_list;

   begin

      select * into v_rec
        from gruppo
       where gruppo_id  = p_id
         and azienda_id = ng.g_azienda_id;

      v_rec.nome := get_json_string(v_json, 'nome');
      v_rec.tipo := get_json_string(v_json, 'tipo');
      v_rec.visibile := nvl(v_json.get('visibile').get_number, 0);
      v_rec.cdate := sysdate;

      update gruppo
         set row = v_rec
       where gruppo_id = p_id;

      if v_rec.tipo = 'UTENTE' then --se è un gruppo di utenti va registrata la relazione col canale e quella con gli utenti
      
         delete gruppi_utenti
          where gruppo_id = v_rec.gruppo_id;

         v_utenti := pljson_list(v_json.get('utenti'));

         for i in 1..v_utenti.count loop

            insert into gruppi_utenti
                   (gruppo_id,       utente_id)
            values (v_rec.gruppo_id, v_utenti.get(i).get_number);

         end loop;

      end if;

   end update_;

   procedure delete_(p_id in number) as
   begin

      delete gruppo
       where gruppo_id  = p_id
         and azienda_id = ng.g_azienda_id;

   end delete_;

   procedure read_gruppi_canale(p_params in varchar2,
                                p_out    out clob) as

      v_json_list pljson_list:= pljson_list();
      v_ids       id_table := id_table();
      v_json      pljson;
      v_jsonu     pljson;
      v_params    pljson := pljson(p_params);
      v_utenti    pljson_list;

   begin

      for c_cur in (select gc.*, g.nome, g.tipo, g.visibile
                      from gruppi_canali gc, gruppo g
                     where gc.canale_id = v_params.get('canale_id').get_number
                       and gc.gruppo_id = g.gruppo_id
                       and g.azienda_id = ng.g_azienda_id
                     order by lower(g.nome)) loop

         v_json:= pljson();
         v_json.put('gruppo_id', c_cur.gruppo_id);
         v_json.put('canale_id', c_cur.canale_id);
         v_json.put('nome', c_cur.nome);
         v_json.put('tipo', c_cur.tipo);
         v_json.put('visibile', c_cur.visibile);

         v_utenti := pljson_list();
         v_jsonu  := pljson();
         for d_cur in (select u.utente_id, u.nome
                         from gruppi_utenti gu, utente u
                        where gu.gruppo_id = c_cur.gruppo_id
                          and gu.utente_id = u.utente_id
                          and u.azienda_id = ng.g_azienda_id
                          and u.blocked    = 0) loop

            v_jsonu.put('utente_id', d_cur.utente_id);
            v_jsonu.put('nome', d_cur.nome);
            v_utenti.append(v_jsonu.to_json_value);

         end loop;

         v_json.put('utenti', v_utenti);
         v_json_list.append(v_json.to_json_value);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_json_list.to_json_value().to_clob(p_out);

   end read_gruppi_canale;

end no_gruppo;
/