create or replace PACKAGE BODY no_report_utenti as

   type id_table is table of number;

   function lazy_read(p_id in number)
     return pljson as

      v_json pljson;

   begin

      for c_cur in (select u.utente_id, u.mail, u.nome, nvl(u.downloaded_size, 0) downloaded_size, last_login,
                           (select sum(d.doc_size)
                              from documento d, logs l
                             where d.documento_id = l.documento_id
                               and l.utente_id    = u.utente_id
                               and l.azienda_id   = ng.g_azienda_id
                               and l.tipo         = 'UPLOAD') total_size
                      from utente u
                     where u.utente_id  = p_id
                       and u.azienda_id = ng.g_azienda_id
                    union
                    select null, 'anonimo@anonimo.it', 'Anonimo',
                           (select sum(d.doc_size)
                              from documento d, logs l
                             where d.documento_id = l.documento_id
                               and l.azienda_id   = ng.g_azienda_id
                               and l.tipo         = 'DOWNLOAD'
                               and l.utente_id is null), null, 0
                      from dual) loop

         v_json:= pljson();
         v_json.put('utente_id', c_cur.utente_id);
         v_json.put('nome', c_cur.nome);
         v_json.put('mail', c_cur.mail);
         v_json.put('downloaded_size', c_cur.downloaded_size);
         v_json.put('last_login', to_char(c_cur.last_login, 'dd/mm/yyyy hh:mi'));
         v_json.put('total_size', c_cur.total_size);

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2,
                  p_out  out clob) as

      v_json_list pljson_list:= pljson_list();
      l_pars      pljson:= pljson(p_pars);
      v_ids       id_table := id_table();

   begin

      select utente_id
        bulk collect into v_ids
        from utente
       where blocked    = 0
         and azienda_id = ng.g_azienda_id
       order by lower(nome);

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_json_list.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_json_list.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id  in number,
                  p_out out clob) as

      v_json pljson:= lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

end no_report_utenti;
/