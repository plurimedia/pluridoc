create or replace package no_report_utenti as

    function lazy_read(p_id in number)
      return pljson;

    procedure read(p_pars in varchar2, p_out out clob);

    procedure read(p_id in number, p_out out clob);

end no_report_utenti;
/