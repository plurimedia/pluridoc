create or replace package no_logs as

   function lazy_read(p_id in number)
     return pljson;

   procedure read(p_pars in varchar2, p_out out clob);

   procedure read(p_id in number, p_out out clob);

   procedure create_(p_obj in clob, p_json out varchar2);

   procedure update_(p_id   in number,
                     p_json in out varchar2);

   procedure delete_(p_id in number);

   procedure logout(p_params in varchar2, p_out out clob);

   procedure public_(p_params in varchar2, p_out out clob);

end no_logs;
/