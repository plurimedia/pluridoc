create or replace package body no_logs as

   type id_table is table of number;

   function get_json_string(p_j in pljson, p_name in varchar2)
     return varchar2 as
   begin
     return p_j.get(p_name).get_string;
     exception when others then return null;
   end;

   function lazy_read(p_id in number)
     return pljson as

      v_json pljson;

   begin

      for c_cur in (select *
                      from logs
                     where log_id     = p_id
                       and azienda_id = ng.g_azienda_id) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.log_id);
         v_json.put('cdate', to_char(c_cur.cdate, 'dd/mm/yyyy hh24:mi'));
         v_json.put('utente_id', c_cur.utente_id);
         v_json.put('documento_id', c_cur.documento_id);
         v_json.put('dati', c_cur.dati);

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2, p_out out clob) as

      v_jsonl pljson_list := pljson_list();
      l_pars  pljson := pljson(p_pars);
      v_ids   id_table := id_table();

   begin

      select l.log_id
        bulk collect into v_ids
        from logs l, utente u
       where l.tipo       = nvl(l_pars.get('tipo').get_string, l.tipo)
         and l.utente_id  = l_pars.get('utente_id').get_number
         and u.utente_id  = l.utente_id
         and u.azienda_id = ng.g_azienda_id
         and l.azienda_id = ng.g_azienda_id
       order by l.cdate desc;

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_jsonl.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id in number, p_out out clob) as

      v_json pljson := lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

   procedure create_(p_obj in clob, p_json out varchar2) as

      v_rec    logs%rowtype;
      v_json   pljson;
      v_jsonl  pljson_list;
      is_multi boolean default false;

   begin

      v_json := pljson(p_obj);
      
      --in caso di tipo download controllo se è un download multiplo
      if (get_json_string(v_json, 'tipo') = 'DOWNLOAD' and v_json.get('documenti') is not null) then
         begin
            v_jsonl := pljson_list(v_json.get('documenti'));
            is_multi := true;
            exception
               when others then
                  is_multi := false;
         end;
      end if;

      if not is_multi then

         v_rec.log_id := seq_logs.nextval;
         v_rec.cdate := sysdate;
         v_rec.utente_id := ng.g_utente_id;
         v_rec.azienda_id := ng.g_azienda_id;
         v_rec.documento_id := get_json_string(v_json, 'documento_id');
         v_rec.tipo := get_json_string(v_json, 'tipo');
         v_rec.dati := get_json_string(v_json, 'dati');
  
         insert into logs
         values v_rec
         returning log_id into v_rec.log_id;

      else

         for i in 1..v_jsonl.count loop
            v_json := pljson(v_jsonl.get(i));

            v_rec.log_id := seq_logs.nextval;
            v_rec.cdate := sysdate;
            v_rec.utente_id := ng.g_utente_id;
            v_rec.azienda_id := ng.g_azienda_id;
            v_rec.documento_id := get_json_string(v_json, '_id');
            v_rec.tipo := 'DOWNLOAD';
    
            insert into logs
            values v_rec
            returning log_id into v_rec.log_id;
         end loop;

      end if;

      --commit;

      v_json.put('_id', v_rec.log_id);
      p_json := v_json.to_char();

   end create_;

   procedure update_(p_id   in number,
                     p_json in out varchar2) as

      v_rec logs%rowtype;
      v_json pljson:= pljson(p_json);

   begin

      select * into v_rec
        from logs
       where log_id     = p_id
         and azienda_id = ng.g_azienda_id;

      v_rec.cdate := sysdate;
      v_rec.utente_id := get_json_string(v_json, 'utente_id');
      v_rec.documento_id := get_json_string(v_json, 'documento_id');
      v_rec.dati := get_json_string(v_json, 'dati');

      update logs
         set row = v_rec
       where log_id     = p_id
         and azienda_id = ng.g_azienda_id;

   end update_;

   procedure delete_(p_id in number) as
   begin

      delete logs
       where log_id     = p_id
         and azienda_id = ng.g_azienda_id;

   end delete_;

   procedure logout(p_params in varchar2,
                    p_out    out clob) as

      v_rec logs%rowtype;

   begin

      v_rec.log_id := seq_logs.nextval;
      v_rec.cdate := sysdate;
      v_rec.utente_id := ng.g_utente_id;
      v_rec.azienda_id := ng.g_azienda_id;
      v_rec.tipo := 'LOGOUT';

      insert into logs values v_rec;

      p_out := '{"messaggio":"ok"}';

   end logout;

   procedure public_(p_params in varchar2,
                     p_out    out clob) as

      v_jsonl pljson_list:= pljson_list();
      v_json  pljson := pljson(p_params);
      v_da    date;
      v_a     date;

   begin

      v_da := to_date(substr(get_json_string(v_json, 'da'), 1, 10), 'yyyy-mm-dd');
      v_a := to_date(nvl(substr(get_json_string(v_json, 'a'), 1, 10), '2100-01-01'), 'yyyy-mm-dd');

      for item in (select l.*, u.mail
                     from utente u, logs l
                    where u.utente_id(+)  = l.utente_id
                      and u.azienda_id(+) = ng.g_azienda_id
                      and l.azienda_id    = ng.g_azienda_id
                      and l.tipo in ('LOGIN', 'LOGIN_FAIL', 'LOGOUT')
                      and l.cdate between v_da and v_a
                      and nvl(u.mail, l.dati) is not null
                 order by l.cdate) loop

         v_json:= pljson();

         v_json.put('operationId', to_char(item.log_id));
         v_json.put('operationMoment', to_char(item.cdate, 'dd/mm/yyyy hh24:mi:ss'));
         v_json.put('userName', nvl(item.mail, item.dati));
         v_json.put('ipAddress', '');
         if item.tipo = 'LOGIN_FAIL' then
            v_json.put('operation', 'LOGINTRY');
         else
            v_json.put('operation', item.tipo);
         end if;

         v_jsonl.append(v_json.to_json_value);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end public_;

end no_logs;
/