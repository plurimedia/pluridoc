create or replace package body no_utente as

   type id_table is table of number;

   function get_json_string(p_j in pljson, p_name in varchar2)
     return varchar2 as
   begin
     return p_j.get(p_name).get_string;
     exception when others then return null;
   end get_json_string;

   function lazy_read(p_id in number)
     return pljson as

      v_json pljson;

   begin

      for c_cur in (select utente.*,
                           (select sum(doc_size) from documento where utente_id = utente.utente_id) uploaded_size
                      from utente
                     where utente_id = p_id) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.utente_id);
         v_json.put('nome', c_cur.nome);
         v_json.put('blocked', c_cur.blocked);
         v_json.put('mail', c_cur.mail);
         v_json.put('fl_temp', c_cur.fl_temp);
         v_json.put('admin', c_cur.admin);
         v_json.put('azienda', c_cur.azienda);
         v_json.put('azienda_id', c_cur.azienda_id);
         v_json.put('last_login', to_char(c_cur.last_login, 'dd/mm/yyyy hh24:mi'));
         v_json.put('downloaded_size', nvl(c_cur.downloaded_size, 0));
         v_json.put('uploaded_size', nvl(c_cur.uploaded_size, 0));
         v_json.put('cdate', to_char(c_cur.cdate, 'dd/mm/yyyy'));
         v_json.put('scadenza', to_char(c_cur.scadenza, 'yyyy-mm-dd'));
         v_json.put('decorrenza', to_char(c_cur.decorrenza, 'yyyy-mm-dd'));

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2, p_out out clob) as

      v_jsonl pljson_list:= pljson_list();
      l_pars  pljson:= pljson(p_pars);
      v_ids   id_table := id_table();
      v_rid   varchar2(50);

   begin

      if get_json_string(l_pars, 'temp') is not null and
         get_json_string(l_pars, 'temp') = '1' then

        select utente_id
          bulk collect into v_ids
          from utente
         where nvl(fl_temp, 0) = 1
           and utente_id > 0
           and blocked = 0
           and azienda_id = ng.g_azienda_id
         order by lower(nome);

      elsif get_json_string(l_pars, 'rid') is not null then

        v_rid := get_json_string(l_pars, 'rid');
        
        select utente_id
          bulk collect into v_ids
          from utente
         where rowid = v_rid;

      elsif get_json_string(l_pars, 'search') is not null then

        select utente_id
          bulk collect into v_ids
          from utente
         where lower(nome) like '%'||lower(l_pars.get('search').get_string)||'%'
           and azienda_id = ng.g_azienda_id
           and utente_id > 0;

      else

        select utente_id
          bulk collect into v_ids
          from utente
         where nvl(fl_temp, 0) = 0
           and utente_id > 0
           and azienda_id = ng.g_azienda_id
         order by lower(nome);

      end if;

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_jsonl.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id in number, p_out out clob) as

      v_json pljson:= lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

   procedure create_(p_obj in clob, p_json out varchar2) as

      v_rec utente%rowtype;
      v_json pljson:= pljson(p_obj);
      v_rid  varchar2(50);

   begin

      ng.check_admin;

      v_rec.utente_id := seq_utente.nextval;
      v_rec.nome := get_json_string(v_json, 'nome');
      v_rec.azienda_id := ng.g_azienda_id;
      v_rec.password := sha1(get_json_string(v_json, 'password'));
      v_rec.mail := get_json_string(v_json, 'mail');
      v_rec.azienda := get_json_string(v_json, 'azienda');
      v_rec.fl_temp := get_json_string(v_json, 'fl_temp');
      v_rec.scadenza := to_date(get_json_string(v_json, 'scadenza'), 'yyyy-mm-dd');
      v_rec.decorrenza := to_date(get_json_string(v_json, 'decorrenza'), 'yyyy-mm-dd');
      v_rec.cdate := sysdate;
      v_rec.blocked := 0;

      insert into utente
      values v_rec
      returning utente_id, rowid into v_rec.utente_id, v_rid;

      v_json.put('_id', v_rec.utente_id);
      v_json.put('rid', v_rid);
      p_json := v_json.to_char();

   end create_;

   procedure update_(p_id   in number,
                     p_json in out varchar2) as

      v_rec utente%rowtype;
      v_json pljson:= pljson(p_json);

   begin

      ng.check_admin;

      select * into v_rec
        from utente
       where utente_id = p_id;

      v_rec.nome := get_json_string(v_json, 'nome');
      v_rec.mail := get_json_string(v_json, 'mail');
      v_rec.azienda := get_json_string(v_json, 'azienda');
      v_rec.fl_temp := get_json_string(v_json, 'fl_temp');
      v_rec.scadenza := to_date(get_json_string(v_json, 'scadenza'), 'yyyy-mm-dd');
      v_rec.decorrenza := to_date(get_json_string(v_json, 'decorrenza'), 'yyyy-mm-dd');

      update utente
         set row = v_rec
       where utente_id = p_id;

   end update_;

   procedure delete_(p_id in number) as
   begin

      ng.check_admin;

      if p_id > 0 then
         update utente
            set blocked = 1
          where utente_id = p_id;
      else
         update utente
            set blocked = 0
          where utente_id = -p_id;
      end if;

   end delete_;

   function update_download(p_params in varchar2)
     return varchar2 as

      v_json pljson := pljson(p_params);
      v_size utente.downloaded_size%type;

   begin

      v_size := to_number(nvl(get_json_string(v_json, 'file_size'), '0'));

      if v_size > 0 then

         update utente
            set downloaded_size = nvl(downloaded_size, 0) + v_size
          where utente_id = ng.g_utente_id;

      end if;

      return '{"update":"ok"}';

   end update_download;

   procedure change_password(p_params in varchar2,
                             p_out    out clob) as

      v_json pljson := pljson(p_params);
      v_pwd  utente.password%type;
      v_old  utente.password%type;

   begin

      v_pwd := sha1(get_json_string(v_json, 'password'));
      v_old := sha1(get_json_string(v_json, 'old_password'));

      update utente
         set password = v_pwd
       where utente_id = ng.g_utente_id
         and password = v_old;

      if sql%rowcount = 0 then
         raise_application_error(-20002, 'Password errata');
      end if;

      dbms_lob.createtemporary(p_out, true);
      p_out := '{ "update": "ok" }';

   end change_password;

   procedure create_password(p_params in varchar2,
                             p_out    out clob) as

      v_json pljson := pljson(p_params);
      v_pwd  utente.password%type;
      v_id   utente.utente_id%type;

   begin

      v_pwd := sha1(get_json_string(v_json, 'password'));
      v_id := get_json_string(v_json, 'utente_id');

      update utente
         set password = v_pwd, fl_temp = null
       where utente_id = v_id;

      if sql%rowcount = 0 then
         raise_application_error(-20002, 'Qualcosa si è rotto '||sqlerrm);
      end if;

      dbms_lob.createtemporary(p_out, true);
      p_out := '{ "update": "ok" }';

      --commit; -- Aggiunto il 6-MAG-2021 ma non so perché è necessario :(

   end create_password;

   procedure mail_request(p_params in varchar2,
                          p_out    out clob) as

      v_json pljson := pljson(p_params);
      v_mail utente.mail%type := get_json_string(v_json, 'mail');
      v_rid  rowid;

   begin

      update utente
         set password = null
       where mail       = v_mail
         and azienda_id = ng.g_azienda_id
      return rowid
        into v_rid;

      if sql%rowcount = 0 then
         raise_application_error(-20002, 'Utente non trovato con mail ' || v_mail);
      end if;

      p_out := '{ "rowid": "' || v_rid || '", "mail": "' || v_mail || '", "azienda_id": ' || ng.g_azienda_id || ' }';

   end mail_request;

   procedure total_upload(p_params in varchar2,
                          p_out    out clob) as

      l_tot  number;
      v_json pljson := pljson(p_params);
      v_id   number := get_json_string(v_json, 'utente_id');

   begin

     select sum(doc_size)
       into l_tot
       from documento
      where utente_id = v_id;

      dbms_lob.createtemporary(p_out, true);
      p_out := '{ "dimensione": ' || nvl(l_tot, 0) || ' }';

   end total_upload;

   procedure total_download(p_params in varchar2,
                            p_out    out clob) as

      l_tot  number;
      v_json pljson := pljson(p_params);
      v_id   number := get_json_string(v_json, 'utente_id');

   begin

     select sum(a.doc_size)
       into l_tot
       from documento a, logs d
      where d.utente_id    = v_id
        and a.documento_id = d.documento_id
        and d.tipo         = 'DOWNLOAD'
        and d.documento_id is not null;

      dbms_lob.createtemporary(p_out, true);
      p_out := '{ "dimensione": ' || nvl(l_tot, 0) || ' }';

   end total_download;

   procedure report_upload(p_params in varchar2,
                           p_out    out clob) as

      v_jsonl pljson_list:= pljson_list();
      v_json  pljson := pljson(p_params);
      v_id    number;
      v_da    date;
      v_a     date;

   begin

      begin
         v_da := to_date(substr(get_json_string(v_json, 'da'), 1, 10), 'yyyy-mm-dd');
         v_a := to_date(substr(get_json_string(v_json, 'a'), 1, 10), 'yyyy-mm-dd');
         exception when others then
            ng.log(sqlerrm || ' '|| p_params);
      end;

      v_id := get_json_string(v_json, '_id');

      for item in (select a.*, b.percorso, c.nome nome_canale
                     from documento a, v_cartella_path b, canale c
                    where a.utente_id   = v_id
                      and a.cartella_id = b.cartella_id
                      and a.canale_id   = c.canale_id
                      and a.mdate between v_da and v_a
                 order by a.mdate desc) loop

         v_json:= pljson();
         begin
            v_json.put('_id', item.documento_id);
            v_json.put('mdate', to_char(item.mdate, 'dd/mm/yyyy hh24:mi'));
            v_json.put('file', item.name_orig);
            v_json.put('bytes', item.doc_size);
            v_json.put('percorso', item.percorso);
            v_json.put('nome_canale', item.nome_canale);
            v_json.put('mimetype', item.mimetype);
            v_json.put('deleted_flg', nvl(item.deleted_flg, 'N'));
            exception
               when others then
                  ng.log(sqlerrm || ' '|| item.documento_id);
         end;

        v_jsonl.append(v_json.to_json_value);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end report_upload;

   procedure report_download(p_params in varchar2,
                             p_out    out clob) as

      v_jsonl pljson_list:= pljson_list();
      v_json  pljson := pljson(p_params);
      v_id    number;
      v_da    date;
      v_a     date;
      v_rec   documento%rowtype;

   begin

      begin
         v_da := to_date(substr(get_json_string(v_json, 'da'), 1, 10), 'yyyy-mm-dd');
         v_a := to_date(substr(get_json_string(v_json, 'a'), 1, 10), 'yyyy-mm-dd');
         exception when others then
            ng.log(sqlerrm || ' '|| p_params);
      end;

      v_id := get_json_string(v_json, '_id');

      for item in (select distinct a.*, b.percorso, c.nome nome_canale, d.cdate
                     from documento a,
                          v_cartella_path b,
                          canale c,
                          logs d
                    where d.utente_id = v_id
                      and a.cartella_id = b.cartella_id
                      and c.canale_id = a.canale_id
                      and a.documento_id = d.documento_id
                      and d.cdate between v_da and v_a
                      and d.tipo = 'DOWNLOAD'
                      and d.documento_id is not null
                    order by d.cdate desc) loop

         v_json:= pljson();
         begin
            v_json.put('_id', item.documento_id);
            v_json.put('mdate', to_char(item.cdate, 'dd/mm/yyyy hh24:mi'));
            v_json.put('file', item.name_orig);
            v_json.put('bytes', item.doc_size);
            v_json.put('percorso', item.percorso);
            v_json.put('nome_canale', item.nome_canale);
            v_json.put('deleted_flg', nvl(item.deleted_flg, 'N'));
            exception
               when others then
                  ng.log(sqlerrm || ' '|| item.documento_id);
        end;

        v_jsonl.append(v_json.to_json_value);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end report_download;

   procedure get_mails_by_ids(p_params in varchar2,
                              p_out    out clob) as

      v_mails pljson_list := pljson_list();
      v_ids   pljson_list := pljson_list(p_params);
      l_mail  varchar2(255);

   begin

      for i in 1..v_ids.count loop

         select mail into l_mail from utente where utente_id = v_ids.get(i).get_number;
         v_mails.append(l_mail);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_mails.to_json_value().to_clob(p_out);

   end get_mails_by_ids;

   procedure attiva(p_params in varchar2,
                    p_out    out clob) as

      v_json pljson := pljson(p_params);
      v_mail utente.mail%type := get_json_string(v_json, 'mail');
      v_id   utente.utente_id%type;
      v_rid  varchar2(50);

   begin

      update utente
         set password = null, fl_temp = 1
       where mail       = v_mail
         and azienda_id = ng.g_azienda_id
      return rowid, utente_id
        into v_rid, v_id;

      if sql%rowcount = 0 then
         raise_application_error(-20002, 'Utente non trovato con mail ' || v_mail);
      end if;

      v_json.put('_id', v_id);
      v_json.put('rid', v_rid);
      p_out := v_json.to_char();

   end attiva;

   procedure associa_docs_utente(p_params in varchar2,
                                 p_out    out clob) as

      v_json  pljson := pljson(p_params);
      v_from  number := get_json_string(v_json, 'utente_id_from');
      v_to    number := get_json_string(v_json, 'utente_id_to');
      v_dummy number;

   begin

      ng.check_admin;

      select null
        into v_dummy
        from utente
       where utente_id  = v_from
         and azienda_id = ng.g_azienda_id;

      select null
        into v_dummy
        from utente
       where utente_id  = v_to
         and azienda_id = ng.g_azienda_id;

      /*insert into utenti_cartelle (cartella_id, utente_id)
      select cartella_id, v_to
        from utenti_cartelle
       where utente_id = v_from
         and cartella_id not in (select cartella_id from utenti_cartelle where utente_id = v_to);*/

      insert into utenti_documenti (documento_id, utente_id)
      select documento_id, v_to
        from utenti_documenti
       where utente_id = v_from
         and documento_id not in (select documento_id from utenti_documenti where utente_id = v_to);

      p_out := '{"messaggio":"ok"}';

   end associa_docs_utente;

   procedure public_(p_params in varchar2,
                     p_out    out clob) as

      v_jsonl pljson_list:= pljson_list();
      v_json  pljson := pljson(p_params);

   begin

      for item in (select *
                     from utente
                    where azienda_id = ng.g_azienda_id
                 order by mail) loop

         v_json:= pljson();
         v_json.put('Account', to_char(item.mail));
         v_json.put('Email', to_char(item.mail));
         v_json.put('DtUltimoLogin', to_char(item.last_login, 'dd/mm/yyyy hh24:mi:ss'));
         v_json.put('Nominativo', item.nome);
         v_json.put('Modulo', '');

         if nvl(item.fl_temp, 0) = 1 then
            v_json.put('Stato', 2);
         elsif item.blocked = 1 then
            v_json.put('Stato', 0);
         else
            v_json.put('Stato', '');
         end if;

         if item.admin = 1 then
            v_json.put('Profilo', 'admin');
         else
            v_json.put('Profilo', '');
         end if;

         v_jsonl.append(v_json.to_json_value);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end public_;

end no_utente;
/