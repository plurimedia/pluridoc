create or replace package no_utente as

    function lazy_read(p_id in number)
      return pljson;

    --function read(p_pars in varchar2)
    --  return varchar2;

    --function read(p_id in number)
    --  return varchar2;

    procedure read(p_pars in varchar2, p_out out clob);

    procedure read(p_id in number, p_out out clob);

    procedure create_(p_obj in clob, p_json out varchar2);

    procedure update_(p_id   in number,
                      p_json in out varchar2);

    procedure delete_(p_id in number);

    function update_download(p_params in varchar2)
      return varchar2;

    procedure change_password(p_params in varchar2, p_out out clob);

    procedure create_password(p_params in varchar2, p_out out clob);

    procedure report_upload(p_params in varchar2, p_out out clob);

    procedure report_download(p_params in varchar2, p_out out clob);

    procedure get_mails_by_ids(p_params in varchar2, p_out out clob);

    procedure mail_request(p_params in varchar2, p_out out clob);
    
    procedure total_download(p_params in varchar2, p_out out clob);
    
    procedure total_upload(p_params in varchar2, p_out out clob);

    procedure attiva(p_params in varchar2, p_out out clob);

    procedure associa_docs_utente(p_params in varchar2, p_out out clob);

   procedure public_(p_params in varchar2, p_out out clob);

end no_utente;
/