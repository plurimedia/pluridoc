create or replace package no_report_sintesi as

    function lazy_read(p_id in number)
      return pljson;

    procedure read(p_pars in varchar2, p_out out clob);

    procedure read(p_id in number, p_out out clob);

    procedure create_;

    procedure get_job_status(p_params in varchar2, p_out out clob);
    
    procedure read_anno(p_params in varchar2, p_out out clob);
    
    procedure get_total_bytes(p_params in varchar2, p_out out clob);

end no_report_sintesi;
/