create or replace package body no_report_sintesi as

   type id_table is table of number;

   function get_json_string(p_j in pljson, p_name in varchar2)
     return varchar2 as
   begin

     return p_j.get(p_name).get_string;

     exception when others then
       begin
         return replace(p_j.get(p_name).to_char, '"', '');
       exception when others then
         return null;
       end;

   end get_json_string;

   function lazy_read(p_id in number)
     return pljson as

      v_json pljson;

   begin

      for c_cur in (select *
                      from report_sintesi
                     where report_sintesi_id = p_id
                       and azienda_id        = ng.g_azienda_id) loop

         v_json:= pljson();
         v_json.put('canali', c_cur.canali);
         v_json.put('documenti', c_cur.documenti);
         v_json.put('_id', 'rpt_sintesi_'||to_char(to_date(c_cur.report_sintesi_id, 'yyyymmdd'), 'yyyy_mm_dd'));
         v_json.put('utenti', c_cur.utenti);
         v_json.put('cdate', to_char(c_cur.cdate, ng.g_date_format));

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2, p_out out clob) as

      v_jsonl pljson_list:= pljson_list();
      l_pars  pljson:= pljson(p_pars);
      v_ids   id_table := id_table();
      l_da    varchar2(20);
      l_a     varchar2(20);

   begin

      l_da := get_json_string(l_pars, 'da');
      l_a := get_json_string(l_pars, 'a');

      select report_sintesi_id
        bulk collect into v_ids
        from report_sintesi
       where cdate between to_date(l_da, 'yyyy-mm-dd') and to_date(l_a, 'yyyy-mm-dd')
         and azienda_id = ng.g_azienda_id;

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_jsonl.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id in number, p_out out clob) as

      v_json pljson:= lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

   procedure create_ as

      v_rec report_sintesi%rowtype;

   begin

      for a in (select azienda_id from azienda) loop

         select count(*)
           into v_rec.canali
           from canale
          where azienda_id = a.azienda_id;

         select sum(nvl(d.doc_size, 0))
           into v_rec.documenti
           from documento d, canale c
          where c.azienda_id = a.azienda_id
            and d.canale_id  = c.canale_id;
         
         v_rec.documenti := nvl(v_rec.documenti, 0);

         select count(*)
           into v_rec.utenti
           from utente
          where azienda_id = a.azienda_id;

         v_rec.report_sintesi_id := to_char(sysdate, 'yyyymmdd');
         v_rec.azienda_id := a.azienda_id;
         v_rec.cdate := sysdate;

         begin
            insert into report_sintesi values v_rec;

            exception
               when dup_val_on_index then
                  update report_sintesi
                     set row = v_rec
                   where report_sintesi_id = v_rec.report_sintesi_id
                     and azienda_id = a.azienda_id;
         end;

      end loop;

      update documento
         set deleted_flg = 'Y'
       where scadenza is not null
         and scadenza <= sysdate
         and deleted_flg <> 'Y';

   end create_;

   procedure get_job_status(p_params in varchar2, p_out out clob) as

      v_json pljson;
      l_id   report_sintesi.report_sintesi_id%type;

   begin

      v_json:= pljson();

      for j in (select last_date, last_sec, next_date, next_sec, decode(broken, 'N', 'Attivo', 'Y', 'In errore') broken from user_jobs) loop

         v_json.put('broken', j.broken);
         v_json.put('last_date', to_char(j.last_date, 'dd/mm/yyyy'));
         v_json.put('last_sec', j.last_sec);
         v_json.put('next_date', to_char(j.next_date, 'dd/mm/yyyy'));
         v_json.put('next_sec', j.next_sec);

         if j.last_date < sysdate -1 then
            v_json.put('warning', 1);
         else
            v_json.put('warning', 0);
         end if;

      end loop;

      select max(report_sintesi_id)
        into l_id
        from report_sintesi
       where azienda_id = ng.g_azienda_id;

      v_json.put('last_id', to_char(to_date(l_id, 'yyyymmdd'), 'dd/mm/yyyy'));

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end get_job_status;

   procedure read_anno(p_params in varchar2, p_out out clob) as

      v_json  pljson:= pljson(p_params);
      l_anno  number := get_json_string(v_json, 'anno');
      v_jsonl pljson_list := pljson_list();

   begin

      for c_cur in (select *
                      from v_report_sintesi_mese
                     where anno       = l_anno
                       and azienda_id = ng.g_azienda_id
                     order by mese) loop

        v_json:= pljson();
        v_json.put('canali', c_cur.canali);
        v_json.put('documenti', c_cur.documenti);
        v_json.put('_id', 'rpt_sintesi_'||to_char(to_date(c_cur.report_sintesi_id, 'yyyymmdd'), 'yyyy_mm_dd'));
        v_json.put('utenti', c_cur.utenti);
        v_json.put('anno', c_cur.anno);
        v_json.put('mese', c_cur.mese - 1);
        v_json.put('cdate', to_char(c_cur.cdate, ng.g_date_format));

        v_jsonl.append(v_json.to_json_value);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end read_anno;

   procedure get_total_bytes(p_params in varchar2, p_out out clob) as

      v_bytes number;

   begin
      select sum(d.doc_size)
        into v_bytes
        from documento d, canale c
       where c.azienda_id = ng.g_azienda_id
         and d.canale_id  = c.canale_id;

      dbms_lob.createtemporary(p_out, true);
      p_out := '{ "tot": ' || nvl(v_bytes, 0) || ' }';

   end get_total_bytes;

end no_report_sintesi;
/