create or replace package body no_canale as

   type id_table is table of number;

   function get_json_string(p_j in pljson, p_name in varchar2)
     return varchar2 as
   begin
      return p_j.get(p_name).get_string;
      exception when others then return null;
   end get_json_string;

   function lazy_read(p_id in number)
     return pljson as

      v_json pljson;

   begin

      for c_cur in (select *
                      from canale
                     where canale_id  = p_id
                       and azienda_id = ng.g_azienda_id) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.canale_id);
         v_json.put('canale_id', c_cur.canale_id);
         v_json.put('nome', c_cur.nome);
         v_json.put('avviso', c_cur.avviso);
         v_json.put('blocked', c_cur.blocked);
         v_json.put('pubblico', c_cur.pubblico);
         v_json.put('mail_from_name', c_cur.mail_from_name);
         v_json.put('mail_nuovi_docs', c_cur.mail_nuovi_docs);
         v_json.put('cdate', to_char(c_cur.cdate, 'dd/mm/yyyy'));

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2, p_out out clob) as

      v_jsonl pljson_list:= pljson_list();
      l_pars  pljson:= pljson(p_pars);
      v_ids   id_table := id_table();

   begin

      select canale_id
        bulk collect into v_ids
        from canale
       where azienda_id = ng.g_azienda_id
       order by pubblico, lower(nome);

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_jsonl.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id in number, p_out out clob) as

      v_json pljson:= lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

   procedure create_(p_obj in clob, p_json out varchar2) as

      v_rec canale%rowtype;
      v_json pljson := pljson(p_obj);
      c_json varchar2(1000);
      l_count number;

   begin

      ng.check_admin;
      
      v_rec.nome := get_json_string(v_json, 'nome');
      v_rec.avviso := get_json_string(v_json, 'avviso');
      v_rec.blocked := get_json_string(v_json, 'blocked');
      v_rec.mail_from_name := get_json_string(v_json, 'mail_from_name');
      v_rec.mail_nuovi_docs := get_json_string(v_json, 'mail_nuovi_docs');
      v_rec.pubblico := nvl(get_json_string(v_json, 'pubblico'), 0);
      v_rec.azienda_id := ng.g_azienda_id;
      v_rec.cdate := sysdate;

      v_rec.canale_id := seq_canale.nextval;

      insert into canale
      values v_rec
      return canale_id into v_rec.canale_id;

      v_json.put('_id', v_rec.canale_id);
      p_json := v_json.to_char();

      insert into cartella
             (cartella_id,          canale_id,       nome)
      values (seq_cartella.nextval, v_rec.canale_id, 'pluridoc');

   end create_;

   procedure update_(p_id   in number,
                     p_json in out varchar2) as

      v_rec canale%rowtype;
      v_json pljson:= pljson(p_json);
      l_count number;

   begin

      ng.check_admin;

      select * into v_rec
        from canale
       where canale_id  = p_id
         and azienda_id = ng.g_azienda_id;

      v_rec.nome := get_json_string(v_json, 'nome');
      v_rec.avviso := get_json_string(v_json, 'avviso');
      v_rec.blocked := get_json_string(v_json, 'blocked');
      v_rec.mail_from_name := get_json_string(v_json, 'mail_from_name');
      v_rec.mail_nuovi_docs := get_json_string(v_json, 'mail_nuovi_docs');
      v_rec.pubblico := nvl(get_json_string(v_json, 'pubblico'), 0);

      update canale
         set row = v_rec
       where canale_id  = p_id
         and azienda_id = ng.g_azienda_id;

   end update_;

   procedure delete_(p_id in number) as
   begin

      ng.check_admin;

      delete canale
       where canale_id  = p_id
         and azienda_id = ng.g_azienda_id;

   end delete_;

   procedure report_upload(p_params in varchar2, p_out out clob) as

      v_jsonl pljson_list:= pljson_list();
      v_json  pljson := pljson(p_params);
      v_ruolo utenti_canali.ruolo%type;
      v_id    number;
      v_da    date;
      v_a     date;

    begin
      begin
         v_da := to_date(substr(get_json_string(v_json, 'da'), 1, 10), 'yyyy-mm-dd');
         v_a := to_date(substr(get_json_string(v_json, 'a'), 1, 10), 'yyyy-mm-dd');

         exception
            when others then
               ng.log(sqlerrm || ' '|| p_params);
      end;

      v_id := get_json_string(v_json, 'cid');

      if nvl(ng.g_admin, 0) = 1 or is_public(v_id) then

         v_ruolo := 3; -- Uploader

      else

         select ruolo
           into v_ruolo
           from utenti_canali
          where utente_id = ng.g_utente_id
            and canale_id = v_id;

      end if;

      --{ cid: ch._id, mdate: doc.mdate, file: doc.name_orig, bytes: doc.size, percorso: doc.folder.percorso }
      --report upload utente {"_id":"74","da":"2016-10-01","a":"2016-10-31"}
      for item in (select d.*, p.percorso, c.nome nome_canale, u.nome nome_uploader
                     from documento d, v_cartella_path p, canale c, utente u
                    where d.canale_id   = v_id
                      and d.cartella_id = p.cartella_id
                      and c.canale_id   = d.canale_id
                      and d.utente_id   = u.utente_id
                      and c.azienda_id  = ng.g_azienda_id
                      and u.azienda_id  = ng.g_azienda_id
                      and p.azienda_id  = ng.g_azienda_id
                      and d.mdate between v_da and v_a
                      and (v_ruolo > 1 or d.deleted_flg is null)
                      and (v_ruolo > 1
                       or  d.documento_id in (select ud.documento_id
                                                from utenti_documenti ud
                                               where ud.utente_id = ng.g_utente_id))
                 order by d.mdate desc) loop

         v_json:= pljson();

         v_json.put('_id', item.documento_id);
         v_json.put('mdate', to_char(item.mdate, 'dd/mm/yyyy hh24:mi'));
         v_json.put('file', item.name_orig);
         v_json.put('bytes', item.doc_size);
         v_json.put('percorso', item.percorso);
         v_json.put('canale_id', item.canale_id);
         v_json.put('nome_canale', item.nome_canale);
         v_json.put('nome_uploader', item.nome_uploader);
         v_json.put('mimetype', item.mimetype);
         v_json.put('deleted_flg', nvl(item.deleted_flg, 'N'));

         v_jsonl.append(v_json.to_json_value);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end report_upload;

   procedure report_download(p_params in varchar2, p_out out clob) as

      v_jsonl pljson_list:= pljson_list();
      v_json  pljson := pljson(p_params);
      v_id    number;
      v_da    date;
      v_a     date;

   begin

      begin
         v_da := to_date(substr(get_json_string(v_json, 'da'), 1, 10), 'yyyy-mm-dd');
         v_a := to_date(substr(get_json_string(v_json, 'a'), 1, 10), 'yyyy-mm-dd');
         exception when others then
            ng.log(sqlerrm || ' '|| p_params);
      end;

      v_id := get_json_string(v_json, 'cid');

      for item in (select distinct a.documento_id, a.name_orig, a.doc_size, a.deleted_flg,
                                   b.percorso, d.cdate mdate, nvl(u.nome, 'Anonimo') utente
                     from documento a, v_cartella_path b, utente u, logs d
                    where a.canale_id     = v_id
                      and a.cartella_id   = b.cartella_id
                      and u.utente_id(+)  = d.utente_id
                      and d.documento_id  = a.documento_id
                      and d.tipo          = 'DOWNLOAD'
                      and b.azienda_id    = ng.g_azienda_id
                      and u.azienda_id(+) = ng.g_azienda_id
                      and d.azienda_id    = ng.g_azienda_id
                      and d.cdate between v_da and v_a
                 order by d.cdate desc) loop

         v_json:= pljson();

         v_json.put('_id', item.documento_id);
         v_json.put('mdate', to_char(item.mdate, 'dd/mm/yyyy hh24:mi'));
         v_json.put('file', item.name_orig);
         v_json.put('bytes', item.doc_size);
         v_json.put('percorso', item.percorso);
         v_json.put('user', item.utente);
         v_json.put('deleted_flg', nvl(item.deleted_flg, 'N'));

         v_jsonl.append(v_json.to_json_value);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end report_download;

   procedure associa_docs(p_params in varchar2,
                          p_out out clob) as

      v_json      pljson := pljson(p_params);
      l_utente_id number := get_json_string(v_json, 'utente_id');
      l_canale_id number := get_json_string(v_json, 'canale_id');
      v_dummy     number;

   begin

      ng.check_admin;

      select null
        into v_dummy
        from utente
       where utente_id  = l_utente_id
         and azienda_id = ng.g_azienda_id;

      select null
        into v_dummy
        from canale
       where canale_id  = l_canale_id
         and azienda_id = ng.g_azienda_id;

      insert into utenti_documenti (documento_id, utente_id)
      select distinct documento_id, l_utente_id
        from documento
       where canale_id = l_canale_id
         and documento_id not in (select documento_id from utenti_documenti where utente_id = l_utente_id);

      p_out := '{"messaggio":"ok"}';

   end associa_docs;

   function is_public(p_id in number)
     return boolean is

      v_pub canale.pubblico%type;

   begin

      select pubblico
        into v_pub
        from canale
       where canale_id  = p_id
         and azienda_id = ng.g_azienda_id;

      return v_pub = 1;

   end is_public;

   function is_public2(p_id in number)
     return boolean is

      v_pub canale.pubblico%type;

   begin

      select pubblico
        into v_pub
        from canale
       where canale_id  = p_id
         and azienda_id = ng.g_azienda_id;

      return v_pub > 0;

   end is_public2;

end no_canale;
/