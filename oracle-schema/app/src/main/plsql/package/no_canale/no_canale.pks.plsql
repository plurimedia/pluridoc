create or replace package no_canale as

    function lazy_read(p_id in number)
      return pljson;

    procedure read(p_pars in varchar2, p_out out clob);

    procedure read(p_id in number, p_out out clob);

    procedure create_(p_obj in clob, p_json out varchar2);

    procedure update_(p_id   in number,
                      p_json in out varchar2);

    procedure delete_(p_id in number);

    procedure report_upload(p_params in varchar2, p_out out clob);

    procedure report_download(p_params in varchar2, p_out out clob);

    procedure associa_docs(p_params in varchar2, p_out out clob);

    function is_public(p_id in number)
      return boolean;

    function is_public2(p_id in number)
      return boolean;

end no_canale;
/