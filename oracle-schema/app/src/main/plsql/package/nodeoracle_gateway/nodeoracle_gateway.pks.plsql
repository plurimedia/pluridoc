create or replace package nodeoracle_gateway as

   g_utente      pljson;
   g_azienda_id  azienda.azienda_id%type;
   g_utente_id   utente.utente_id%type;
   g_admin       utente.admin%type default 0;
   g_date_format varchar2(50) := 'yyyy-mm-dd"T"hh24:mi:ss".000Z"';
   g_display_date_format varchar2(50) := 'dd/mm/yyyy hh24:mi';

--vecchie versioni, controllarne l'utilizzo
   function read(p_clob    in varchar2,
                 p_model   in varchar2,
                 p_context in varchar2)
     return varchar2;

   function read(p_id      in number,
                 p_model   in varchar2,
                 p_context in varchar2)
     return varchar2;

   procedure read(p_clob    in varchar2,
                  p_model   in varchar2,
                  p_context in varchar2,
                  p_out     out clob);
--/vecchie versioni
   procedure read(p_clob    in out clob,
                  p_model   in varchar2,
                  p_context in varchar2);

   procedure read(p_id      in number,
                  p_model   in varchar2,
                  p_context in varchar2,
                  p_out     out clob);

   procedure create_(p_clob    in clob,
                     p_model   in varchar2,
                     p_context in varchar2,
                     p_result  out clob);

   procedure update_(p_id      in number,
                     p_clob    in varchar2,
                     p_model   in varchar2,
                     p_context in varchar2,
                     p_result  out varchar2);

   procedure delete_(p_id      in number,
                     p_model   in varchar2,
                     p_context in varchar2);

--vecchia versione controllarne l'utilizzo
   function method(p_clob    in varchar2,
                   p_method  in varchar2,
                   p_context in varchar2)
     return varchar2;
--/vecchia versione

   procedure method(p_clob    in out clob,
                    p_method  in varchar2,
                    p_context in varchar2);

   procedure method(p_clob    in varchar2,
                    p_method  in varchar2,
                    p_context in varchar2,
                    p_out out clob);

   procedure log(p_text in clob);

   procedure check_ruolo(p_cid   in number,
                         p_ruolo in number);

   procedure check_ruolo_doc(p_did   in number,
                             p_ruolo in number);

   procedure check_ruolo_cartella(p_cid   in number,
                                  p_ruolo in number);

   procedure check_admin;

   procedure check_ruolo2(p_cid   in number,
                          p_ruolo in number);

   procedure check_ruolo_doc2(p_did   in number,
                              p_ruolo in number);

end nodeoracle_gateway;
/