create or replace package body nodeoracle_gateway as

   v_debug number default 0;

   procedure log(p_text in clob) is
      pragma autonomous_transaction;

   begin

      insert into nodeoracle_logs
             (testo,  id_utente)
      values (p_text, g_utente_id);

      commit;

   end log;

   procedure log_err(p_proc  in varchar2,
                     p_model in varchar2,
                     p_stack in varchar2,
                     p_clob  in clob default null) is
      v_clob clob;
   begin
      if p_clob is not null then
         log(p_clob);
      end if;
      dbms_lob.createtemporary(v_clob, true);
      v_clob := 'proc ' || p_proc || ' model ' || p_model || ' stack ' || p_stack;
      log(v_clob);
   end log_err;

   procedure read_context(p_context in varchar2) is
   begin

      if v_debug = 1 then
         log('[GATEWAY read_context] '||p_context);
      end if;

      g_utente := pljson(p_context);

      if g_utente.get('mail').get_string = 'anonimo@anonimo.it' then

         g_utente_id := null;
         g_admin     := 0;

         select azienda_id
           into g_azienda_id
           from azienda
          where cid = g_utente.get('cid').get_string;

      else

         select utente_id,   azienda_id,   admin
           into g_utente_id, g_azienda_id, g_admin
           from utente
          where mail       = g_utente.get('mail').get_string
            and azienda_id = g_utente.get('azienda_id').get_number
            and blocked = 0;

      end if;

   end read_context;

   function read(p_clob    in varchar2,
                 p_model   in varchar2,
                 p_context in varchar2)
     return varchar2 is

      v_jsonl varchar2(32000);
      v_ret   varchar2(32000);

   begin

      if v_debug = 1 then
         log('[GATEWAY read 1] '||p_clob||' '||p_model||' '||p_context);
      end if;

      if (upper(p_model) = 'NO_UTENTE' and (p_context = 'undefined' or p_context is null)) then
         null;
      else
         read_context(p_context);
      end if;

      execute immediate 'begin :json_list := ' || p_model || '.read(:x); end;'
        using out v_jsonl, in p_clob;

      return v_jsonl;

   end read;

   function read(p_id      in number,
                 p_model   in varchar2,
                 p_context in varchar2)
     return varchar2 is

      v_json varchar2(32000);
      v_ret  varchar2(32000);

   begin

      if v_debug = 1 then log('[GATEWAY read 2] '||p_id||' '||p_model||' '||p_context); end if;
      read_context(p_context);

      execute immediate 'begin :json := ' || p_model || '.read(:x); end;'
        using out v_json, p_id;

      return v_json;

   end read;

   procedure read(p_clob    in varchar2,
                  p_model   in varchar2,
                  p_context in varchar2,
                  p_out     out clob)
   is

      v_jsonl varchar2(32000);
      v_ret   clob;

   begin

      if v_debug = 1 then
         log('[GATEWAY read CLOB 1] '||p_clob||' '||p_model||' '||p_context);
      end if;

      if (upper(p_model) = 'NO_UTENTE' and (p_context = 'undefined' or p_context is null)) then
         null;
      else
         read_context(p_context);
      end if;

      execute immediate 'begin ' || p_model || '.read(:x, :ret); end;'
        using in p_clob, out v_ret;

      p_out := v_ret;

   end read;

   procedure read(p_id      in number,
                  p_model   in varchar2,
                  p_context in varchar2,
                  p_out     out clob) is

      v_json varchar2(32000);
      v_ret  clob;

   begin

      if v_debug = 1 then
         log('[GATEWAY read CLOB 2] '||p_id||' '||p_model||' '||p_context);
      end if;

      if (upper(p_model) = 'NO_UTENTE' and p_context = 'undefined') then
         null;
      else
         read_context(p_context);
      end if;

      execute immediate 'begin ' || p_model || '.read(:x, :ret); end;'
        using in p_id, out v_ret;

      p_out := v_ret;

   end read;

   procedure read(p_clob    in out clob,
                  p_model   in varchar2,
                  p_context in varchar2) is

      v_ret pljson_list;

   begin

      if v_debug = 1 then
         log('[GATEWAY read VARCHAR 1] '||p_model||' '||p_context);
      end if;

      if (upper(p_model) = 'NO_UTENTE' and p_context = 'undefined') then
         null;
      else
         read_context(p_context);
      end if;

      execute immediate 'begin ' || p_model || '.read(:x, :ret); end;'
        using in pljson(p_clob), out v_ret;

      if v_ret is not null then
         v_ret.to_clob(p_clob);
      end if;

   end read;

   procedure create_(p_clob    in clob,
                     p_model   in varchar2,
                     p_context in varchar2,
                     p_result  out clob) is

      v_ret_json varchar2(32000);

   begin

      if v_debug = 1 then
         log('[GATEWAY create] '||p_clob||' '||p_model||' '||p_context);
      end if;

      read_context(p_context);

      if g_utente_id is null and lower(p_model) not in ('no_mail_log', 'no_logs') then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      execute immediate 'begin ' || p_model || '.create_(:x, :y); end;'
        using in p_clob, out v_ret_json;

      p_result:=v_ret_json;

      exception
         when others then
            log_err(p_proc  => 'create_',
                    p_model => p_model,
                    p_stack => dbms_utility.format_error_stack || dbms_utility.format_error_backtrace,
                    p_clob  => p_clob);
            raise;
   end create_;

   procedure update_(p_id      in number,
                     p_clob    in varchar2,
                     p_model   in varchar2,
                     p_context in varchar2,
                     p_result  out varchar2) is

      v_json pljson := pljson_Parser.parser(p_clob);
      v_ret varchar2(32000);

   begin

      if v_debug = 1 then
         log('[GATEWAY update] '||p_id||' '||p_model||' '||p_context);
      end if;

      read_context(p_context);

      if g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_ret := v_json.to_char();

      execute immediate 'begin ' || p_model || '.update_(:x, :y); end;'
        using p_id, in out v_ret;

      p_result:= v_ret;

      exception
         when others then
            log_err(p_proc  => 'update_',
                    p_model => p_model,
                    p_stack => dbms_utility.format_error_stack || dbms_utility.format_error_backtrace,
                    p_clob  => p_clob);
            raise;
   end update_;

   procedure delete_(p_id      in number,
                     p_model   in varchar2,
                     p_context in varchar2) is
   begin

      if v_debug = 1 then
         log('[GATEWAY delete] '||p_id||' '||p_model||' '||p_context);
      end if;

      read_context(p_context);

      if g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      execute immediate 'begin ' || p_model || '.delete_(:x); end;'
        using p_id;

      exception
         when others then
            log_err(p_proc  => 'delete_',
                    p_model => p_model,
                    p_stack => dbms_utility.format_error_stack || dbms_utility.format_error_backtrace);
            raise;
   end delete_;

   function method(p_clob    in varchar2,
                   p_method  in varchar2,
                   p_context in varchar2)
     return varchar2 is

      v_ret varchar2(32000);

   begin

      if v_debug = 1 then
         log('[GATEWAY method] '||p_clob||' '||p_method||' '||p_context);
      end if;

      read_context(p_context);

      execute immediate 'begin :ret := ' || p_method || '(:x); end;'
        using out v_ret, in p_clob;

      return v_ret;

      exception
         when others then
            log_err(p_proc  => 'method0',
                    p_model => p_method,
                    p_stack => dbms_utility.format_error_stack || dbms_utility.format_error_backtrace,
                    p_clob  => p_clob);
            raise;
   end method;

   procedure method(p_clob    in varchar2,
                    p_method  in varchar2,
                    p_context in varchar2,
                    p_out     out clob) is

      v_ret clob;

   begin

      if v_debug = 1 then
         log('[GATEWAY method] '||p_clob||' '||p_method||' '||p_context);
      end if;

      if not lower(p_method) in ('no_utente.create_password') then
         read_context(p_context);
      end if;
      
      execute immediate 'begin ' || p_method || '(:x, :ret); end;'
        using in p_clob, out v_ret;

      p_out := v_ret;

      exception
         when others then
            log_err(p_proc  => 'method1',
                    p_model => p_method,
                    p_stack => dbms_utility.format_error_stack || dbms_utility.format_error_backtrace,
                    p_clob  => p_clob);
            raise;
   end method;

   procedure method(p_clob    in out clob,
                    p_method  in varchar2,
                    p_context in varchar2) is

      v_ret clob;

   begin

      if v_debug = 1 then
         log('[GATEWAY method] '||p_clob||' '||p_method||' '||p_context);
      end if;

      read_context(p_context);

      execute immediate 'begin ' || p_method || '(:x, :ret); end;'
        using in p_clob, out v_ret;

      exception
         when others then
            log_err(p_proc  => 'method2',
                    p_model => p_method,
                    p_stack => dbms_utility.format_error_stack || dbms_utility.format_error_backtrace,
                    p_clob  => p_clob);
            raise;
   end method;

   procedure check_ruolo(p_cid   in number,
                         p_ruolo in number) is

      v_ruolo utenti_canali.ruolo%type := 0;

   begin

      begin
         select ruolo
           into v_ruolo
           from utenti_canali
          where canale_id = p_cid
            and utente_id = g_utente_id;

         exception
            when no_data_found then
               if no_canale.is_public(p_cid) then
                  v_ruolo := 1;
               end if;
      end;

      if v_ruolo < p_ruolo then
         raise_application_error(-20111, 'Privilegi insufficienti');
      end if;

   end check_ruolo;

   procedure check_ruolo_doc(p_did   in number,
                             p_ruolo in number) is

      v_cid number;

   begin

      select canale_id
        into v_cid
        from documento
       where documento_id = p_did;

      check_ruolo(v_cid, p_ruolo);

   end check_ruolo_doc;

   procedure check_ruolo_cartella(p_cid   in number,
                                  p_ruolo in number) is

      v_cid number;

   begin

      select canale_id
        into v_cid
        from cartella
       where cartella_id = p_cid;

      check_ruolo(v_cid, p_ruolo);

   end check_ruolo_cartella;

   procedure check_admin is
   begin

      if g_admin <> 1 then
         raise_application_error(-20111, 'Privilegi insufficienti');
      end if;

   end check_admin;

   procedure check_ruolo2(p_cid   in number,
                          p_ruolo in number) is

      v_ruolo utenti_canali.ruolo%type := 0;

   begin

      begin
         select ruolo
           into v_ruolo
           from utenti_canali
          where canale_id = p_cid
            and utente_id = g_utente_id;

         exception
            when no_data_found then
               if no_canale.is_public2(p_cid) then
                  v_ruolo := 1;
               end if;
      end;

      if v_ruolo < p_ruolo then
         raise_application_error(-20111, 'Privilegi insufficienti');
      end if;

   end check_ruolo2;

   procedure check_ruolo_doc2(p_did   in number,
                              p_ruolo in number) is

      v_cid number;

   begin

      select canale_id
        into v_cid
        from documento
       where documento_id = p_did;

      check_ruolo2(v_cid, p_ruolo);

   end check_ruolo_doc2;

end nodeoracle_gateway;
/