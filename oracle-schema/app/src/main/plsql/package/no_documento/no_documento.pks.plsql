create or replace package no_documento as

    type ids_rec is record
    (
       nome varchar2(30)
    );

    type ids_tab is table of ids_rec;

    function json_list_to_table(p_l in pljson_list)
      return ids_tab pipelined;

    function lazy_read(p_id in number)
      return pljson;

    procedure read(p_pars in varchar2, p_out out clob);

    procedure read(p_id in number, p_out out clob);

    procedure create_(p_obj in clob, p_out out clob);

    procedure update_(p_id   in number,
                      p_json in out varchar2);

    procedure delete_(p_id in number);

    procedure assegna(p_params in varchar2, p_out out clob);

    procedure rinomina(p_params in varchar2, p_out out clob);

    procedure name_compressed(p_params in varchar2, p_out out clob);

    procedure delete_multiple(p_params in varchar2, p_out out clob);

    procedure restore_multiple(p_params in varchar2, p_out out clob);

    procedure assegna_multiple(p_params in varchar2, p_out out clob);

    procedure can_delete(p_params in varchar2, p_out out clob);

    procedure elimina(p_params in varchar2, p_out out clob);

    procedure scadenza(p_params in varchar2, p_out out clob);

    procedure find_tag(p_params in varchar2, p_out out clob);

    procedure save_meta(p_params in varchar2, p_out out clob);

    procedure find_by_tags(p_params in varchar2, p_out out clob);

    procedure public_(p_params in varchar2, p_out out clob);

    procedure all_docs_s3(p_params in varchar2, p_out out clob);

end no_documento;
/