create or replace package body no_documento as

   type id_table is table of number;

   function get_json_string(p_j in pljson, p_name in varchar2)
   return varchar2 as
   begin
     return p_j.get(p_name).get_string;
     exception when others then return null;
   end get_json_string;

   function json_list_to_table(p_l in pljson_list)
     return ids_tab pipelined is

      v_rec ids_rec;

   begin

      for i in 1..p_l.count loop
         v_rec.nome := p_l.get(i).get_string;
         pipe row (v_rec);
      end loop;

   end json_list_to_table;

   function lazy_read(p_id in number)
     return pljson as

      v_json    pljson;
      v_viewers pljson_list := pljson_list();
      v_txt     varchar2(4000);

   begin

      for c_cur in (select d.*, u.nome uploader
                      from documento d, utente u
                     where d.documento_id = p_id
                       and d.utente_id    = u.utente_id) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.documento_id);
         v_json.put('cartella_id', c_cur.cartella_id);
         v_json.put('name', c_cur.name);
         v_json.put('name_orig', c_cur.name_orig);
         v_json.put('name_compressed', c_cur.name_compressed);
         v_json.put('size', c_cur.doc_size);
         v_json.put('mdate', to_char(c_cur.mdate, ng.g_date_format));
         v_json.put('scadenza', to_char(c_cur.scadenza, ng.g_date_format));
         v_json.put('scadenza_d', to_char(c_cur.scadenza, 'dd/mm/yyyy'));
         v_json.put('canale_id', c_cur.canale_id);
         v_json.put('utente_id', c_cur.utente_id);
         v_json.put('mimetype', c_cur.mimetype);
         v_json.put('deleted_flg', c_cur.deleted_flg);
         v_json.put('descrizione', c_cur.descrizione);
         v_json.put('uploader', c_cur.uploader);

         for d_cur in (select utente_id
                         from utenti_documenti
                        where documento_id = p_id) loop

            v_viewers.append(d_cur.utente_id);

         end loop;

         v_json.put('viewers', v_viewers);

         select listagg('"' || t.nome || '"', ',') within group (order by t.nome)
           into v_txt
           from tags t, tags_documenti d
          where d.documento_id = p_id
            and t.tag_id       = d.tag_id;

         v_json.put('tags', pljson_list('[' || v_txt || ']'));

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2, p_out out clob) as

      v_json_list   pljson_list:= pljson_list();
      l_pars        pljson:= pljson(p_pars);
      v_ids         id_table := id_table();
      l_canale_id   number;
      l_utente_id   number;
      l_cartella_id number;
      l_ruolo       number;
      l_order       number;
      v_pub         boolean := false;

   begin

      l_canale_id := get_json_string(l_pars, 'canale_id');
      l_utente_id := get_json_string(l_pars, 'utente_id');
      l_cartella_id := get_json_string(l_pars, 'cartella_id');
      l_order := nvl(get_json_string(l_pars, 'orderbydate'), 0);

      if l_canale_id is not null then

         v_pub := no_canale.is_public(l_canale_id);

         begin
            select ruolo
              into l_ruolo
              from utenti_canali
             where utente_id = ng.g_utente_id
               and canale_id = l_canale_id;

            exception
               when no_data_found then l_ruolo := null;
         end;

      end if;

      if v_pub or l_canale_id is null or (l_ruolo is not null and l_ruolo > 1) or nvl(ng.g_admin, 0) = 1 then

         select documento_id
           bulk collect into v_ids
           from documento
          where (utente_id = l_utente_id or l_utente_id is null)
            and (cartella_id = l_cartella_id or l_cartella_id is null)
            and (canale_id = l_canale_id or l_canale_id is null)
          order by case when l_order = 1 then mdate end desc,
                   case when l_order <> 1 then lower(name_orig) end asc;

      else

         select a.documento_id
           bulk collect into v_ids
           from documento a, utenti_documenti b
          where a.documento_id = b.documento_id
            and b.utente_id    = ng.g_utente_id
            and canale_id      = l_canale_id
            and (a.utente_id = l_utente_id or l_utente_id is null)
            and (cartella_id = l_cartella_id or l_cartella_id is null)
            and a.deleted_flg is null
            and (a.scadenza is null or a.scadenza > sysdate)
          order by case when l_order = 1 then a.mdate end desc,
                   case when l_order <> 1 then lower(a.name_orig) end asc;

      end if;

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_json_list.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_json_list.to_json_value().to_clob(p_out);

   end read;
     
   procedure read(p_id in number, p_out out clob) as

      v_json pljson:= lazy_read(p_id);

   begin

      ng.check_ruolo_doc2(p_id, 1);

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

   procedure save_tags(p_tags in pljson_list,
                       p_id   in number) as

      v_id number;

   begin

      delete tags_documenti where documento_id = p_id;

      for y in 1 .. p_tags.count loop

         begin
            select tag_id
              into v_id
              from tags
             where nome       = p_tags.get(y).get_string
               and azienda_id = ng.g_azienda_id;

            exception
               when no_data_found then
                  insert into tags
                         (azienda_id,      nome)
                  values (ng.g_azienda_id, p_tags.get(y).get_string)
                  return tag_id into v_id;
         end;

         insert into tags_documenti
                (tag_id, documento_id)
         values (v_id,   p_id);

      end loop;

   end save_tags;

   procedure create_(p_obj in clob, p_out out clob) as

      v_rec  documento%rowtype;
      v_json pljson:= pljson(p_obj);

      v_utenti_list    pljson_list;
      v_tags_list      pljson_list;
      v_documenti_list pljson_list;
      v_ret_list       pljson_list := pljson_list();
      v_doc            pljson;
      v_uid            utenti_documenti.utente_id%type;
      v_overwrite      pls_integer;

   begin

      v_utenti_list := pljson_list(v_json.get('utenti'));
      v_documenti_list := pljson_list(v_json.get('documenti'));
      v_rec.canale_id := get_json_string(v_json, 'cid');
      v_rec.scadenza := to_date(get_json_string(v_json, 'scadenza'), 'yyyy-mm-dd');
      ng.check_ruolo(v_rec.canale_id, 2);

      for i in 1..v_documenti_list.count loop

         --registrazione documento
         v_doc := pljson(v_documenti_list.get(i));

         v_rec.documento_id := seq_documento.nextval;
         v_rec.cartella_id := pljson(v_doc.get('folder')).get('_id').get_string;
         v_rec.name := get_json_string(v_doc, 'name2');
         v_rec.descrizione := get_json_string(v_doc, 'descrizione');
         v_rec.name_orig := get_json_string(v_doc, 'name_orig');
         v_rec.doc_size := get_json_string(v_doc, 'size2');
         v_rec.mdate := sysdate;
         v_rec.utente_id := ng.g_utente_id;
         v_rec.mimetype := get_json_string(v_doc, 'mimetype');
         v_overwrite := nvl(get_json_string(v_doc, 'overwrite'), 0);

         if v_overwrite = 1 then
            update documento
               set deleted_flg = 'Y'
             where name_orig   = v_rec.name_orig
               and cartella_id = v_rec.cartella_id
               and deleted_flg is null;
         end if;

         insert into documento
         values v_rec
         return documento_id into v_rec.documento_id;
         
         v_doc.put('documento_id', v_rec.documento_id);
         v_ret_list.append(v_doc.to_json_value);

         --associazioni agli utenti
         for y in 1 .. v_utenti_list.count loop

            v_uid := pljson(v_utenti_list.get(y)).get('utente_id').get_number;
            insert into utenti_documenti
                   (utente_id, documento_id)
            values (v_uid,     v_rec.documento_id);

         end loop;

         save_tags(pljson_list(v_doc.get('tags')), v_rec.documento_id);

         --log upload
         ng.log('tipo=UPLOAD, documento_id='||v_rec.documento_id);

      end loop;

      --controllare che possa fare upload 
      --se !cu.canale || !cu.ruolo || cu.ruolo == 0 non autorizzato
      --recuperare l'utente (ma a me serve solo l'id che potrei gia avere nella sessione
      --ciclare sull'oggetto documenti che mi arriva e fare una insert per ogni documento
      --invia mail (devo fare una route apposta)

      -- p_json := '{ "esito": "ok" }';
      dbms_lob.createtemporary(p_out, true);
      v_ret_list.to_json_value().to_clob(p_out);

   end create_;

   procedure update_(p_id   in number,
                     p_json in out varchar2) as

      v_rec  documento%rowtype;
      v_json pljson:= pljson(p_json);

   begin

      ng.check_ruolo_doc(p_id, 2);

      select * into v_rec
        from documento
       where documento_id = p_id;

      if v_rec.canale_id != get_json_string(v_json, 'canale_id') then

         v_rec.canale_id := get_json_string(v_json, 'canale_id');
         ng.check_ruolo(v_rec.canale_id, 3);

      end if;

      v_rec.cartella_id := get_json_string(v_json, 'cartella_id');
      v_rec.name := get_json_string(v_json, 'name2');
      v_rec.descrizione := get_json_string(v_json, 'descrizione');
      v_rec.name_orig := get_json_string(v_json, 'name_orig');
      v_rec.doc_size := get_json_string(v_json, 'size2');
      v_rec.mdate := sysdate;
      v_rec.utente_id := get_json_string(v_json, 'utente_id');
      v_rec.mimetype := get_json_string(v_json, 'mimetype');

      update documento
         set row = v_rec
       where documento_id = p_id;

   end update_;

   procedure delete_(p_id in number) as

      v_cid documento.canale_id%type;

   begin

      if p_id < 1 then

         update documento
            set deleted_flg = null
          where documento_id = -p_id
         return canale_id into v_cid;

      else

         update documento
            set deleted_flg = 'Y'
          where documento_id = p_id
         return canale_id into v_cid;

      end if;

      ng.check_ruolo(v_cid, 3);

   end delete_;

   procedure assegna(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_rec    utenti_documenti%rowtype;
      v_utenti pljson_list := pljson_list();
      v_cid    number;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_rec.documento_id := get_json_string(v_params, 'doc');

      ng.check_ruolo_doc(v_rec.documento_id, 3);

      v_utenti := pljson_list(v_params.get('utenti'));

      delete utenti_documenti
       where documento_id = v_rec.documento_id;

      for y in 1..v_utenti.count loop

         v_rec.utente_id := pljson_value.get_number(v_utenti.get(y));

         insert into utenti_documenti
         values v_rec;

      end loop;

      p_out := '{ "esito": "ok" }';

   end assegna;

   procedure rinomina(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_doc    number;
      v_ch     number;
      v_nome   cartella.nome%type;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_doc  := get_json_string(v_params, 'id');
      v_nome := get_json_string(v_params, 'nome');

      update documento
         set name_orig = v_nome, mdate = sysdate
       where documento_id = v_doc
      return canale_id
        into v_ch;

      ng.check_ruolo(v_ch, 2);

      p_out := '{ "esito": "ok" }';

   end rinomina;

   procedure name_compressed(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_doc    number;
      v_ch     number;
      v_nome   documento.name_compressed%type;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_doc  := get_json_string(v_params, 'id');
      v_nome := get_json_string(v_params, 'name_compressed');

      update documento
         set name_compressed = v_nome, mdate = sysdate
       where documento_id = v_doc
      return canale_id
        into v_ch;

      ng.check_ruolo(v_ch, 2);

      p_out := '{ "esito": "ok" }';

   end name_compressed;

   procedure delete_multiple(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_docs   pljson_list := pljson_list();
      v_id     number;
      v_ch     number := 0;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_docs := pljson_list(v_params.get('docs'));

      for i in 1 .. v_docs.count loop

         v_id := pljson_value.get_number(v_docs.get(i));

         update documento
            set deleted_flg = 'Y'
          where documento_id = v_id
         return canale_id into v_id;

         if v_ch <> v_id then

            v_ch := v_id;
            ng.check_ruolo(v_ch, 3);

         end if;

      end loop;

      p_out := '{ "esito": "ok" }';

   end delete_multiple;

   procedure restore_multiple(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_docs   pljson_list := pljson_list();
      v_id     number;
      v_ch     number := 0;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_docs := pljson_list(v_params.get('docs'));

      for i in 1 .. v_docs.count loop

         v_id := pljson_value.get_number(v_docs.get(i));

         update documento
            set deleted_flg = null
          where documento_id = v_id
         return canale_id into v_id;

         if v_ch <> v_id then

            v_ch := v_id;
            ng.check_ruolo(v_ch, 3);

         end if;

      end loop;

      p_out := '{ "esito": "ok" }';

   end restore_multiple;

   procedure assegna_multiple(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_ruolo  utenti_canali.ruolo%type;
      v_rec    utenti_documenti%rowtype;
      v_utenti pljson_list := pljson_list();
      v_docs   pljson_list := pljson_list();
      v_cid    number;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_docs := pljson_list(v_params.get('docs'));
      v_utenti := pljson_list(v_params.get('utenti'));

      for i in 1 .. v_docs.count loop

         v_rec.documento_id := pljson_value.get_number(v_docs.get(i));

         ng.check_ruolo_doc(v_rec.documento_id, 3);

         delete utenti_documenti
          where documento_id = v_rec.documento_id;

         for y in 1 .. v_utenti.count loop

            v_rec.utente_id := pljson_value.get_number(v_utenti.get(y));

            insert into utenti_documenti
            values v_rec;

         end loop;

      end loop;

      p_out := '{ "esito": "ok" }';

   end assegna_multiple;

   procedure can_delete(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_id     number;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_id := get_json_string(v_params, 'doc');

      ng.check_ruolo_doc(v_id, 3);

      v_params := lazy_read(v_id);
      dbms_lob.createtemporary(p_out, true);
      v_params.to_json_value().to_clob(p_out);

   end can_delete;

   procedure elimina(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_id     number;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_id := get_json_string(v_params, 'doc');

      ng.check_ruolo_doc(v_id, 3);

      delete documento where documento_id = v_id;

      p_out := '{ "esito": "ok" }';

   end elimina;

   procedure scadenza(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_doc    number;
      v_ch     number;
      v_data   documento.scadenza%type;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_doc  := get_json_string(v_params, 'id');
      v_data := to_date(get_json_string(v_params, 'scadenza'), 'yyyy-mm-dd');

      update documento
         set scadenza = v_data, mdate = sysdate
       where documento_id = v_doc
      return canale_id
        into v_ch;

      ng.check_ruolo(v_ch, 2);

      p_out := '{ "esito": "ok" }';

   end scadenza;
   
   procedure find_tag(p_params in varchar2,
                      p_out    out clob) as

      v_json pljson := pljson(p_params);
      v_nome varchar2(30) := lower(trim(get_json_string(v_json, 'nome')));
      v_txt  varchar2(4000);

   begin

      select listagg('"' || nome || '"', ',') within group (order by nome)
        into v_txt
        from tags
       where azienda_id = ng.g_azienda_id
         and nome like '%' || v_nome || '%';

      dbms_lob.createtemporary(p_out, true);
      p_out := '[' || v_txt || ']';

   end find_tag;

   procedure find_by_tags(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_docs   pljson_list := pljson_list();
      v_tags   pljson_list;
      v_json   pljson;
      v_ch     number := get_json_string(v_params, 'canale_id');
      v_txt    varchar2(4000);

   begin

      v_tags := pljson_list(v_params.get('tags'));

      if v_tags.count > 0 then

         for c_cur in (select a.*, p.percorso, c.nome canale
                         from documento a, canale c, v_cartella_path p, tags t, tags_documenti td
                        where a.documento_id = td.documento_id
                          and a.canale_id    = c.canale_id
                          and c.azienda_id   = ng.g_azienda_id
                          and a.cartella_id  = p.cartella_id
                          and t.tag_id       = td.tag_id
                          and a.deleted_flg is null
                          and (a.scadenza is null or a.scadenza > sysdate)
                          and (c.pubblico = 1
                           or  nvl(ng.g_admin, 0) = 1
                           or  ng.g_utente_id in (select b.utente_id
                                                    from utenti_documenti b
                                                   where b.documento_id = a.documento_id))
                          and (v_ch is null or a.canale_id = v_ch)
                          and t.nome in (select nome from table(no_documento.json_list_to_table(v_tags)))
                        order by p.percorso, lower(a.name_orig)) loop

            v_json:= pljson();
            v_json.put('_id', c_cur.documento_id);
            v_json.put('canale_id', c_cur.canale_id);
            v_json.put('name_orig', c_cur.name_orig);
            v_json.put('size', c_cur.doc_size);
            v_json.put('mdate', to_char(c_cur.mdate, ng.g_date_format));
            v_json.put('canale', c_cur.canale);
            v_json.put('percorso', regexp_replace(c_cur.percorso, 'pluridoc/', 'pluridoc/' || c_cur.canale || '/', 1, 1));
            v_json.put('descrizione', c_cur.descrizione);

            select listagg('"' || t.nome || '"', ',') within group (order by t.nome)
              into v_txt
              from tags t, tags_documenti d
             where d.documento_id = c_cur.documento_id
               and t.tag_id       = d.tag_id;

            v_json.put('tags', pljson_list('[' || v_txt || ']'));

            v_docs.append(v_json.to_json_value);

         end loop;

      end if;

      dbms_lob.createtemporary(p_out, true);
      v_docs.to_json_value().to_clob(p_out);

   end find_by_tags;

   procedure save_meta(p_params in varchar2,
                       p_out    out clob) as

      v_json pljson := pljson(p_params);
      v_desc documento.descrizione%type := get_json_string(v_json, 'descrizione');
      v_id   number := get_json_string(v_json, '_id');

   begin

      ng.check_ruolo_doc(v_id, 2);

      update documento
         set descrizione = v_desc
       where documento_id = v_id;

      save_tags(pljson_list(v_json.get('tags')), v_id);

      p_out := '{ "esito": "ok" }';

   end save_meta;

   procedure public_(p_params in varchar2,
                     p_out    out clob) as

      v_json pljson := pljson(p_params);
      v_desc v_documento_path.percorso%type := get_json_string(v_json, 'nome');

   begin
   
      select null
        into v_desc
        from v_documento_path
       where percorso = v_desc;

      p_out := '{ "esito": "ok" }';

      exception
         when others then
            p_out := '{ "esito": "ko" }';

   end public_;

   procedure all_docs_s3(p_params in varchar2,
                         p_out    out clob) as

      v_params pljson := pljson(p_params);
      v_docs   pljson_list := pljson_list();
      v_json   pljson;

   begin

      for c_cur in (select * from v_documento_path where azienda_id = ng.g_azienda_id) loop

         v_json:= pljson();
         v_json.put('percorso', c_cur.percorso);
         v_docs.append(v_json.to_json_value);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_docs.to_json_value().to_clob(p_out);

   end all_docs_s3;

end no_documento;
/