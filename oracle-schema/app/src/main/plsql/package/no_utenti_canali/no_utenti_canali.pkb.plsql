create or replace package body no_utenti_canali as

   type id_table is table of number;

   function get_json_string(p_j in pljson, p_name in varchar2)
     return varchar2 as
   begin
     return p_j.get(p_name).get_string;
     exception when others then return null;
   end get_json_string;

   procedure check_access(p_cid in number) is
   begin
      ng.check_admin;
      exception
         when others then
            ng.check_ruolo(p_cid, 3);
   end check_access;

   function lazy_read(p_id in number)
     return pljson as

      v_json pljson;

   begin

      for c_cur in (select *
                      from utenti_canali
                     where utenti_canali_id = p_id) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.utenti_canali_id);
         v_json.put('utente_id', c_cur.utente_id);
         v_json.put('canale_id', c_cur.canale_id);
         v_json.put('ruolo', c_cur.ruolo);
         v_json.put('cdate', to_char(c_cur.cdate, 'dd/mm/yyyy'));

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2,
                  p_out  out clob) as

      v_jsonl pljson_list := pljson_list();
      l_pars  pljson := pljson(p_pars);
      v_ids   id_table := id_table();

   begin

      select uc.utenti_canali_id
        bulk collect into v_ids
        from utenti_canali uc, utente u
       where uc.utente_id = u.utente_id
         and u.blocked    = 0
         and u.azienda_id = ng.g_azienda_id
       order by u.nome;

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_jsonl.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id  in number,
                  p_out out clob) as

      v_json pljson:= lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

   procedure create_(p_obj  in clob,
                     p_json out varchar2) as

      v_rec  utenti_canali%rowtype;
      v_json pljson:= pljson(p_obj);

   begin

      v_rec.canale_id := get_json_string(v_json, 'canale_id');
      check_access(v_rec.canale_id);

      v_rec.utenti_canali_id := seq_utenti_canali.nextval;
      v_rec.utente_id := get_json_string(v_json, 'utente_id');
      v_rec.ruolo := get_json_string(v_json, 'ruolo');
      v_rec.cdate := sysdate;

      insert into utenti_canali
      values v_rec;

      v_json.put('_id', v_rec.utenti_canali_id);
      p_json := v_json.to_char();

   end create_;

   procedure update_(p_id   in number,
                     p_json in out varchar2) as

      v_rec  utenti_canali%rowtype;
      v_json pljson:= pljson(p_json);

   begin

      v_rec.canale_id := get_json_string(v_json, 'canale_id');
      check_access(v_rec.canale_id);

      select * into v_rec
        from utenti_canali
       where utenti_canali_id = p_id;

      v_rec.utente_id := get_json_string(v_json, 'utente_id');
      v_rec.ruolo := get_json_string(v_json, 'ruolo');
      v_rec.cdate := sysdate;

      update utenti_canali
         set row = v_rec
       where utenti_canali_id = p_id;

   end update_;

   procedure delete_(p_id in number) as

      v_id utenti_canali.canale_id%type;

   begin

      delete utenti_canali
       where utenti_canali_id = p_id
      return canale_id into v_id;

      check_access(v_id);

   end delete_;

   procedure read_canali_utente(p_params in varchar2,
                                p_out    out clob) as

      v_jsonl  pljson_list := pljson_list();
      v_ids    id_table := id_table();
      v_json   pljson;
      v_params pljson := pljson(p_params);
      v_all    pls_integer := nvl(get_json_string(v_params, 'all'), 0);

   begin

      for c_cur in (select a.utenti_canali_id, a.utente_id, a.ruolo, a.cdate,
                           b.canale_id, b.nome, b.blocked, b.avviso, b.pubblico
                      from utenti_canali a, canale b
                     where a.utente_id(+) = nvl(get_json_string(v_params, 'utente_id'), ng.g_utente_id)
                       and a.canale_id(+) = b.canale_id
                       and b.azienda_id   = ng.g_azienda_id
                     order by lower(b.nome)) loop

         if v_all <> 1 and ((c_cur.ruolo is null and c_cur.pubblico <> 1) or c_cur.ruolo = 0) then
            continue;
         end if;

         v_json:= pljson();
         v_json.put('_id', c_cur.utenti_canali_id);
         v_json.put('utente_id', c_cur.utente_id);
         v_json.put('canale_id', c_cur.canale_id);
         if c_cur.pubblico = 2 then
            v_json.put('ruolo', nvl(c_cur.ruolo, 0));
         else
            v_json.put('ruolo', nvl(c_cur.ruolo, c_cur.pubblico));
         end if;
         v_json.put('cdate', to_char(c_cur.cdate, 'dd/mm/yyyy'));
         v_json.put('nome', c_cur.nome);
         v_json.put('avviso', c_cur.avviso);
         v_json.put('blocked', c_cur.blocked);
         v_json.put('pubblico', c_cur.pubblico);
         
         v_jsonl.append(v_json.to_json_value);

      end loop;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end read_canali_utente;

   procedure read_utenti_canale(p_params in varchar2,
                                p_out out clob) as

      v_jsonl  pljson_list := pljson_list();
      v_json   pljson;
      v_params pljson := pljson(p_params);
      --v_cid    utenti_cartelle.cartella_id%type := v_params.get('cartella_id').get_number;
      v_can    utenti_canali.canale_id%type := v_params.get('canale_id').get_number;
      v_txt    varchar2(1000) := null;

   begin

      /*if v_cid is not null then

         begin
            ng.check_ruolo(v_can, 2);

            begin
               select listagg(utente_id, ',') within group (order by utente_id)
                 into v_txt
                 from (select utente_id
                         from utenti_cartelle
                        where cartella_id = v_cid);

               v_txt := ',' || v_txt || ',';

               exception
                  when no_data_found then
                     null;
            end;

            exception
               when others then
                  null;
         end;

      end if;*/

      for c_cur in (select uc.*, u.nome, u.mail
                      from utenti_canali uc, utente u
                     where uc.canale_id = v_can
                       and uc.utente_id = u.utente_id
                       and u.blocked    = 0
                       and u.azienda_id = ng.g_azienda_id
                       and (v_params.get('ruolo').get_string is null
                        or  v_params.get('ruolo').get_string = uc.ruolo)
                     order by lower(u.nome)) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.utenti_canali_id);
         v_json.put('utente_id', c_cur.utente_id);
         --v_json.put('canale_id', c_cur.canale_id);
         v_json.put('ruolo', c_cur.ruolo);
         v_json.put('cdate', to_char(c_cur.cdate, 'dd/mm/yyyy'));
         v_json.put('nome', c_cur.nome);
         v_json.put('mail', c_cur.mail);

         if v_txt is not null and v_txt like '%,' || c_cur.utente_id || ',%' then
            v_json.put('enabled', 1);
         end if;

         v_jsonl.append(v_json.to_json_value);

      end loop;
      
      dbms_lob.createtemporary(p_out, true);

      v_jsonl.to_json_value().to_clob(p_out);

   end read_utenti_canale;

   procedure create_or_update(p_params in varchar2,
                              p_out    out clob) as

      v_params pljson := pljson(p_params);
      v_rec    utenti_canali%rowtype;
      c        number;

   begin

      v_rec.canale_id := v_params.get('canale').get_number;
      check_access(v_rec.canale_id);

      v_rec.utenti_canali_id := seq_utenti_canali.nextval;
      v_rec.utente_id := v_params.get('utente').get_number;
      v_rec.ruolo := v_params.get('ruolo').get_number;
      v_rec.cdate := sysdate;

      if (get_json_string(v_params,'ruolo') is not null) then

         insert into utenti_canali
         values v_rec;

      else

        --caso per la nuova interfaccia. dovrebbe comunque continuare a funzionare come prima
        delete utenti_canali
         where utente_id = v_rec.utente_id
           and canale_id = v_rec.canale_id;

      end if;

      p_out := '{ "message" : "insert" }';

      exception
         when dup_val_on_index then
            update utenti_canali
               set ruolo = v_rec.ruolo
             where utente_id = v_rec.utente_id
               and canale_id = v_rec.canale_id;

            p_out := '{ "message" : "update" }';

   end create_or_update;

   function get_ruolo(p_cid in number)
     return utenti_canali.ruolo%type is

      v_ruolo number;

   begin

      select ruolo
        into v_ruolo
        from utenti_canali
       where canale_id = p_cid
         and utente_id = ng.g_utente_id;

      return v_ruolo;

      exception
         when no_data_found then
            select pubblico
              into v_ruolo
              from canale
             where canale_id  = p_cid
               and azienda_id = ng.g_azienda_id;
            return v_ruolo;
   end get_ruolo;

   procedure read_ruolo(p_params in varchar2,
                        p_out    out clob) as

      v_params pljson := pljson(p_params);

   begin

      dbms_lob.createtemporary(p_out, true);
      pljson('{ "ruolo": ' || get_ruolo(v_params.get('canale_id').get_number) || ' }').to_json_value().to_clob(p_out);

   end read_ruolo;

end no_utenti_canali;
/