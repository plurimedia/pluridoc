create or replace package no_utenti_canali as

    function lazy_read(p_id in number)
      return pljson;

    procedure read(p_pars in varchar2, p_out out clob);

    procedure read(p_id in number, p_out out clob);

    procedure create_(p_obj in clob, p_json out varchar2);

    procedure update_(p_id   in number,
                      p_json in out varchar2);

    procedure delete_(p_id in number);
    
    procedure read_canali_utente (p_params in varchar2, p_out out clob);

    procedure read_utenti_canale (p_params in varchar2, p_out out clob);

    procedure create_or_update (p_params in varchar2, p_out out clob);

    function get_json_string(p_j in pljson, p_name in varchar2)
      return varchar2;

    function get_ruolo(p_cid in number)
      return utenti_canali.ruolo%type;

    procedure read_ruolo(p_params in varchar2, p_out out clob);

end no_utenti_canali;
/