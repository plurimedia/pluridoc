create or replace package auth as

   function authenticate(p_params in varchar2)
     return varchar2;

   function authenticate_spid(p_params in varchar2)
     return varchar2;

end auth;
/