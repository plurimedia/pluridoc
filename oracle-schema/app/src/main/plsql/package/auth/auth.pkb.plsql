create or replace package body auth as

   function authenticate(p_params in varchar2)
     return varchar2 as

      l_mail varchar2(100);
      l_pwd  varchar2(100);
      l_cid  number;
      c_cur  utente%rowtype;
      v_json pljson := pljson(p_params);
      v_rec  logs%rowtype;

   begin

      l_mail := lower(trim(v_json.get('email').get_string));
      l_pwd  := sha1(v_json.get('password').get_string);
      l_cid  := v_json.get('cid').get_number;

      begin
         select utente_id,       last_login
           into c_cur.utente_id, v_rec.cdate
           from utente
          where mail       = l_mail
            and azienda_id = l_cid;

         exception
            when no_data_found then
               v_rec.log_id := seq_logs.nextval;
               v_rec.cdate := sysdate;
               v_rec.azienda_id := l_cid;
               v_rec.dati := l_mail;
               v_rec.tipo := 'LOGIN_FAIL';

               insert into logs values v_rec;
               commit;

               raise_application_error(-20999, 'Utente non riconosciuto o Password errata');
      end;

      update utente
         set last_login = sysdate
       where utente_id = c_cur.utente_id
         and password  = l_pwd
         and blocked   = 0
         and (decorrenza is null or decorrenza < sysdate)
         and (scadenza is null or scadenza > sysdate)
         and nvl(fl_temp, 0) = 0
         and password is not null
      return utente_id, nome, password, mail, last_login, downloaded_size, fl_temp, admin, cdate, blocked, azienda_id, azienda, scadenza, decorrenza
        into c_cur;

      if sql%rowcount = 0 then

         v_rec.log_id := seq_logs.nextval;
         v_rec.cdate := sysdate;
         v_rec.utente_id := c_cur.utente_id;
         v_rec.azienda_id := l_cid;
         v_rec.tipo := 'LOGIN_FAIL';

         insert into logs values v_rec;
         commit;

         raise_application_error(-20999, 'Utente non riconosciuto o Password errata');

      end if;

      v_json := pljson();
      v_json.put('_id', c_cur.utente_id);
      v_json.put('nome', c_cur.nome);
      v_json.put('mail', c_cur.mail);
      v_json.put('admin', c_cur.admin);
      v_json.put('azienda_id', l_cid);
      v_json.put('penultimo_login', to_char(v_rec.cdate, ng.g_date_format));

      v_rec.log_id := seq_logs.nextval;
      v_rec.cdate := sysdate;
      v_rec.utente_id := c_cur.utente_id;
      v_rec.azienda_id := l_cid;
      v_rec.tipo := 'LOGIN';

      insert into logs values v_rec;

      return v_json.to_char();

   end authenticate;

   function authenticate_spid(p_params in varchar2)
     return varchar2 as

      l_mail varchar2(100);
      l_pwd  varchar2(100);
      l_cid  number;
      c_cur  utente%rowtype;
      v_json pljson := pljson(p_params);
      v_rec  logs%rowtype;

   begin

      l_mail := lower(trim(v_json.get('email').get_string));
      l_cid  := v_json.get('cid').get_number;

      begin
         select utente_id
           into c_cur.utente_id
           from utente
          where mail       = l_mail
            and azienda_id = l_cid;

         exception
            when no_data_found then
               c_cur.utente_id := seq_utente.nextval;
               c_cur.nome := 'Utente SPID';
               c_cur.azienda_id := l_cid;
               c_cur.mail := l_mail;
               c_cur.cdate := sysdate;
               c_cur.blocked := 0;
         
               insert into utente values c_cur;
               commit;
      end;

      update utente
         set last_login = sysdate
       where utente_id = c_cur.utente_id
         and blocked   = 0
         and (decorrenza is null or decorrenza < sysdate)
         and (scadenza is null or scadenza > sysdate)
         and nvl(fl_temp, 0) = 0
      return utente_id, nome, password, mail, last_login, downloaded_size, fl_temp, admin, cdate, blocked, azienda_id, azienda, scadenza, decorrenza
        into c_cur;

      if sql%rowcount = 0 then

         v_rec.log_id := seq_logs.nextval;
         v_rec.cdate := sysdate;
         v_rec.utente_id := c_cur.utente_id;
         v_rec.azienda_id := l_cid;
         v_rec.tipo := 'LOGIN_FAIL';

         insert into logs values v_rec;
         commit;

         raise_application_error(-20999, 'Utente non abilitato');

      end if;

      v_json := pljson();
      v_json.put('_id', c_cur.utente_id);
      v_json.put('nome', c_cur.nome);
      v_json.put('mail', c_cur.mail);
      v_json.put('admin', c_cur.admin);
      v_json.put('azienda_id', l_cid);

      v_rec.log_id := seq_logs.nextval;
      v_rec.cdate := sysdate;
      v_rec.utente_id := c_cur.utente_id;
      v_rec.azienda_id := l_cid;
      v_rec.tipo := 'LOGIN';

      insert into logs values v_rec;

      return v_json.to_char();

   end authenticate_spid;

end auth;
/