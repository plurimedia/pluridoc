create or replace package body no_mail_log as

   type id_table is table of varchar2(100);

   function get_json_string(p_j in pljson, p_name in varchar2)
   return varchar2
   as
   begin
     return p_j.get(p_name).get_string;
     exception when others then 
       begin
         return replace(p_j.get(p_name).to_char, '"', '');
       exception when others then
         return null;
       end;
   end;

   function lazy_read_invio(p_id in varchar2)
     return pljson as

      v_json       pljson;
      v_json_invio pljson;
      v_jl         pljson_list;
      v_canale_id  number;
      v_utente_rid rowid; 
      v_display    varchar2(250);

   begin

      for c_cur in (select m.*, u.nome, (select count(1) from mail_log where invio_id=p_id) numero_mail
                      from mail_invio m, utente u
                     where m.invio_id   = p_id
                       and m.utente_id  = u.utente_id
                       and u.azienda_id = ng.g_azienda_id) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.invio_id);
         v_json.put('numero_mail', c_cur.numero_mail);
         v_json_invio := pljson(c_cur.json_invio);
         v_json.put('cdate', to_char(c_cur.cdate, ng.g_display_date_format));

         if c_cur.tipo_invio = 'caricamento' then

            begin
              v_canale_id := to_number(pljson(pljson(v_json_invio.get('contenuto')).get('channel')).get('canale_id').get_string);
              select nome
                into v_display
                from canale
               where canale_id = v_canale_id;
               
               /*v_jl := pljson_list(json(v_json_invio.get('documenti')).get('documenti'));

               begin
                  v_canale_id := to_number(json(v_jl.get(1)).get('canale_id').get_string);
                  select nome
                    into v_display
                    from canale
                   where canale_id = v_canale_id;

                  exception
                     when others then
                        --gli invii del caricamento normale e della "riassociazione" hanno pljson leggermente diversi anche se hanno lo stesso tipo
                        v_canale_id := to_number(json(json(v_jl.get(1)).get('folder')).get('_id').get_string);
                        select nome
                          into v_display
                          from canale
                          where canale_id = (select canale_id
                                               from cartella
                                              where cartella_id = v_canale_id);
               end;*/

               if c_cur.nome is null then
                  v_json.put('tipo_invio', c_cur.tipo_invio || ' (canale ' || v_display || ')');
               else
                  v_json.put('tipo_invio', c_cur.tipo_invio || ' (canale ' || v_display || ' - utente ' || c_cur.nome || ')');
               end if;
               v_json.put('tipo', c_cur.tipo_invio);
               v_json.put('canale', v_display);
               v_json.put('utente', c_cur.nome);

               exception
                  when others then
                     v_json.put('tipo_invio', c_cur.tipo_invio);
            end;

         elsif c_cur.tipo_invio  = 'forget-password' then

            begin
               v_utente_rid := utl_url.unescape(pljson(v_json_invio.get('utente')).get('rowid').get_string);

               select nome
                 into v_display
                 from utente
                where rowid = v_utente_rid;

               v_json.put('tipo_invio', c_cur.tipo_invio || ' (utente ' || v_display || ')');
               v_json.put('tipo', c_cur.tipo_invio);

               exception
                  when others then
                     v_json.put('tipo_invio', c_cur.tipo_invio);-- || pljson(v_json_invio.get('utente')).to_char || sqlerrm);
            end;

         elsif c_cur.tipo_invio  = 'account' then

            --questo campo pare non sia piu utile e inoltre da errore
            --v_display := pljson(v_json_invio.get('utente')).get('nome').get_string;
            v_json.put('tipo_invio', c_cur.tipo_invio || ' (utente ' || v_display || ')');
            v_json.put('tipo', c_cur.tipo_invio);

         else

            v_json.put('tipo_invio', c_cur.tipo_invio);
            v_json.put('tipo', c_cur.tipo_invio);
            v_json.put('utente', c_cur.nome);

         end if;

      end loop;

      return v_json;

   end lazy_read_invio;

   procedure read_invio(p_pars in varchar2, p_out out clob) as

      v_json_list pljson_list:= pljson_list();
      l_pars      pljson:= pljson(p_pars);
      v_ids       id_table := id_table();
      v_id        number := get_json_string(l_pars, 'cid');
      v_page      number;
      v_rows4page number:= 15;
      v_start_row number;
      v_end_row   number;

   begin

      v_page := nvl(get_json_string(l_pars, 'page'), 1);

      if get_json_string(l_pars, 'page') is not null then

         v_end_row   := v_page * v_rows4page;
         v_start_row := v_end_row - v_rows4page + 1;

         select invio_id
           bulk collect into v_ids
           from (select rownum rwn, tab.*
                   from (select m.invio_id
                           from mail_invio m, utente u
                          where m.utente_id  = u.utente_id
                            and u.azienda_id = ng.g_azienda_id
                            and (nvl(ng.g_admin, 0) = 1
                             or  m.utente_id        = ng.g_utente_id)
                          order by m.cdate desc) tab
                  where rownum <= v_end_row)
          where rwn >= v_start_row;

      else

         select invio_id
           bulk collect into v_ids
           from (select invio_id
                   from mail_invio m, utente u
                  where m.utente_id  = u.utente_id
                    and u.azienda_id = ng.g_azienda_id
                    and (v_id is null
                     or  (m.canale_id is not null
                    and   m.canale_id = v_id))
                    and (nvl(ng.g_admin, 0) = 1
                     or  m.utente_id        = ng.g_utente_id)
                  order by m.cdate desc)
          where rownum < 500; -- Se no va in timeout

      end if;

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_json_list.append(lazy_read_invio(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_json_list.to_json_value().to_clob(p_out);

   end read_invio;

   function lazy_read(p_id in varchar2)
     return pljson as

      v_json pljson;

   begin

      for c_cur in (select a.*, u.nome
                      from mail_log a, utente u
                     where a.mail_id       = p_id
                       and u.mail(+)       = a.email
                       and u.azienda_id(+) = ng.g_azienda_id) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.mail_id);
         v_json.put('email', c_cur.email);
         v_json.put('nome', c_cur.nome);
         v_json.put('status', c_cur.status);
         v_json.put('reject_reason', c_cur.reject_reason);

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2, p_out out clob) as

      v_json_list pljson_list:= pljson_list();
      l_pars      pljson:= pljson(p_pars);
      v_ids       id_table := id_table();
      v_invio_id  number;

   begin

      v_invio_id := l_pars.get('invio_id').get_string;

      select mail_id
        bulk collect into v_ids
        from mail_log
       where invio_id = v_invio_id;

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_json_list.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_json_list.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id in number, p_out out clob)
   as

      v_json pljson:= lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

   procedure create_(p_obj in clob, p_json out varchar2) as

      v_rec       mail_log%rowtype;
      v_mail_list pljson_list;--:= pljson_list(p_obj);
      v_invio     pljson;
      v_doc       pljson;
      v_tipo      varchar2(100);
      l_clob      clob;
      v_id        number;

   begin

      v_mail_list := pljson_list(pljson(p_obj).get('mail'));
      v_invio := pljson(pljson(p_obj).get('json_invio'));
      v_tipo := get_json_string(v_invio, 'tipo');

      begin
         dbms_lob.createtemporary(l_clob, true);
         v_invio.to_clob(l_clob);
         begin
            v_id := to_number(pljson(pljson(v_invio.get('contenuto')).get('channel')).get('canale_id').get_number);
            exception
               when others then
                  v_id := null;
         end;
         insert into mail_invio
                (invio_id,               json_invio, tipo_invio, cdate,   utente_id,      canale_id)
         values (seq_mail_invio.nextval, l_clob,     v_tipo,     sysdate, ng.g_utente_id, v_id)
         return invio_id into v_rec.invio_id;

         for i in 1..v_mail_list.count loop

            v_doc := pljson(v_mail_list.get(i));

            v_rec.mail_id := get_json_string(v_doc, '_id');
            v_rec.email := get_json_string(v_doc, 'email');
            v_rec.status := get_json_string(v_doc, 'status');
            v_rec.reject_reason := get_json_string(v_doc, 'reject_reason');

            insert into mail_log
            values v_rec;

         end loop;

         exception
            when others then
               ng.log(v_invio.to_char());
               ng.log(v_tipo);
      end;

      p_json := '{ "message": "ok" }';

   end create_;
   
   /*procedure report_totale(p_params in varchar2, p_out out clob)
   as

      v_params pljson := pljson(p_params);
      v_json pljson := pljson();
      v_json_canali pljson_list := pljson_list();
      v_json_tipi pljson_list := pljson_list();

   begin

      for item in (select canale.nome,
                          (select)
                     from canale) loop
        v_json:= pljson();
        v_json.put('_id', c_cur.mail_id);
        v_json.put('email', c_cur.email);
      end loop;
      
      dbms_lob.createtemporary(p_out, true);
      pljson('{ "ruolo": "' || get_ruolo(v_params.get('canale_id').get_number) || '" }').to_json_value().to_clob(p_out);

   end read_ruolo;*/

end no_mail_log;
/