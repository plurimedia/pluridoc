create or replace package no_mail_log as

    function lazy_read(p_id in varchar2)
      return pljson;

    procedure read (p_pars in varchar2, p_out out clob);

    procedure read (p_id in number, p_out out clob);

    function lazy_read_invio(p_id in varchar2)
      return pljson;

    procedure read_invio (p_pars in varchar2, p_out out clob);

    --procedure read_invio (p_id in number, p_out out clob);

    procedure create_(p_obj in clob, p_json out varchar2);

    --procedure report_totale(p_params in varchar2, p_out out clob);

end no_mail_log;
/