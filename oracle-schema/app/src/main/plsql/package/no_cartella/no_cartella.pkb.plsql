create or replace package body no_cartella as

   type id_table is table of number;

   function get_json_string(p_j in pljson, p_name in varchar2)
   return varchar2 as
   begin
     return p_j.get(p_name).get_string;
     exception when others then return null;
   end get_json_string;

   function lazy_read(p_id in number)
     return pljson as

      v_json pljson;

   begin

      for c_cur in (select *
                      from v_cartella_path
                     where cartella_id = p_id) loop

         v_json:= pljson();
         v_json.put('_id', c_cur.cartella_id);
         v_json.put('parent_id', c_cur.parent_id);
         v_json.put('canale_id', c_cur.canale_id);
         v_json.put('nome', c_cur.nome);
         v_json.put('percorso', c_cur.percorso);

      end loop;

      return v_json;

   end lazy_read;

   procedure read(p_pars in varchar2, p_out out clob) as

      v_jsonl pljson_list:= pljson_list();
      l_pars  pljson:= pljson(p_pars);
      v_ids   id_table := id_table();

   begin

      select cartella_id
        bulk collect into v_ids
        from cartella
       where canale_id = l_pars.get('cid').get_string
       order by nome;

      if v_ids.count > 0 then
         for/*all*/ i in v_ids.first .. v_ids.last loop
            v_jsonl.append(lazy_read(v_ids(i)).to_json_value);
         end loop;
      end if;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end read;

   procedure read(p_id in number, p_out out clob) as

      v_json pljson:= lazy_read(p_id);

   begin

      dbms_lob.createtemporary(p_out, true);
      v_json.to_json_value().to_clob(p_out);

   end read;

   procedure create_(p_obj in clob, p_json out varchar2) as

      v_rec cartella%rowtype;
      v_json pljson:= pljson(p_obj);
      l_count number;

   begin

      v_rec.canale_id := get_json_string(v_json, 'canale_id');
      ng.check_ruolo(v_rec.canale_id, 3);

      v_rec.parent_id := get_json_string(v_json, 'parent_id');
      v_rec.nome := get_json_string(v_json, 'nome');

      select count(1)
        into l_count
        from cartella
       where nome      = v_rec.nome
         and parent_id = v_rec.parent_id;
       
      if l_count > 0 then
        raise_application_error(-20007, 'Nome cartella esistente nel path selezionato');
      end if;

      v_rec.cartella_id := seq_cartella.nextval;

      insert into cartella
      values v_rec
      returning cartella_id into v_rec.cartella_id;

      v_json.put('_id', v_rec.cartella_id);
      p_json := v_json.to_char();

      if get_json_string(v_json, 'evento') = 1 then

         v_rec.parent_id := v_rec.cartella_id;

         v_rec.cartella_id := seq_cartella.nextval;
         v_rec.nome := 'costing';
         insert into cartella values v_rec;

         v_rec.cartella_id := seq_cartella.nextval;
         v_rec.nome := 'invoice';
         insert into cartella values v_rec;

         v_rec.cartella_id := seq_cartella.nextval;
         v_rec.nome := 'presentation';
         insert into cartella values v_rec;

         v_rec.cartella_id := seq_cartella.nextval;
         v_rec.nome := 'working';
         insert into cartella values v_rec;

      end if;

   end create_;

   procedure update_(p_id   in number,
                     p_json in out varchar2) as

      v_rec cartella%rowtype;
      v_json pljson:= pljson(p_json);
      l_count number;

   begin

      ng.check_ruolo_cartella(p_id, 3);

      select * into v_rec
        from cartella
       where cartella_id = p_id;

      v_rec.parent_id := get_json_string(v_json, 'parent_id');
      v_rec.canale_id := get_json_string(v_json, 'canale_id');
      v_rec.nome := get_json_string(v_json, 'nome');

      select count(1)
        into l_count
        from cartella
       where nome = v_rec.nome
         and parent_id = v_rec.parent_id;
       
      if l_count > 0 then
        raise_application_error(-20007, 'Nome cartella esistente nel path selezionato');
      end if;

      update cartella
         set row = v_rec
       where cartella_id = p_id;

   end update_;

   procedure delete_(p_id in number) as
   begin

      ng.check_ruolo_cartella(p_id, 3);

      delete cartella
       where cartella_id = p_id;

   end delete_;

   procedure read_cartelle_path(p_params in varchar2, p_out out clob) as

      v_jsonl  pljson_list := pljson_list();
      v_users  pljson_list;
      v_ids    id_table := id_table();
      v_json   pljson;
      v_params pljson := pljson(p_params);
      v_cid    number;
      v_ruolo  utenti_canali.ruolo%type;
      v_keep   pls_integer default 1;
      v_pub    boolean;
      v_txt    varchar2(1000);
      v_data   date;

   begin

      v_cid   := get_json_string(v_params, 'cid');
      v_data  := to_date(get_json_string(v_params, 'data'), ng.g_date_format);
      v_ruolo := no_utenti_canali.get_ruolo(v_cid);
      v_pub   := no_canale.is_public(v_cid);

      if v_ruolo > 0 or nvl(ng.g_admin, 0) = 1 then

         for c_cur in (select * 
                         from v_cartella_path
                        where canale_id = v_cid) loop

            if v_pub then

               v_keep := 1;

            else

               if v_ruolo = 1 then

                  if c_cur.lv > 1 then

                     -- Controllo se ci sono docs in questo ramo che l'utente può vedere
                     begin
                        select 1
                          into v_keep
                          from utenti_documenti ud, documento d
                         where rownum          = 1
                           and ud.utente_id    = ng.g_utente_id
                           and ud.documento_id = d.documento_id
                           and d.deleted_flg is null
                           and (d.scadenza is null or d.scadenza > sysdate)
                           and d.cartella_id in (select cartella_id
                                                   from cartella
                                                  where canale_id = v_cid
                                                  start with cartella_id = c_cur.cartella_id
                                                connect by prior cartella_id = parent_id);
                        exception
                           when no_data_found then
                              v_keep := 0;
                     end;

                  end if;

               end if;

            end if;

            if v_keep = 1 or c_cur.lv = 1 then -- La root è sempre inclusa

               v_json:= pljson();
               v_json.put('_id', c_cur.cartella_id);
               v_json.put('level', c_cur.lv);
               v_json.put('padre', c_cur.parent_id);
               v_json.put('nome', c_cur.nome);
               v_json.put('percorso', c_cur.percorso);
               v_json.put('utenti', pljson_list());
               v_users := pljson_list();

               /*if c_cur.lv > 1 and (v_ruolo > 1 or nvl(ng.g_admin, 0) = 1) then

                  begin
                     select listagg(utente_id, ',') within group (order by utente_id)
                       into v_txt
                       from (select utente_id
                               from utenti_cartelle
                              where cartella_id = c_cur.cartella_id);
                     v_json.put('utenti', pljson_list('[' || v_txt || ']'));
            
                     exception
                        when no_data_found then
                           v_json.put('utenti', pljson_list());
                  end;

               end if;*/

               if v_data is not null then

                  -- Controllo se ci sono docs in questo ramo caricati dopo il penultimo login dell'utente
                  if v_ruolo > 1 or nvl(ng.g_admin, 0) = 1 then -- Admin o uploader

                     begin
                        select 1
                          into v_keep
                          from documento
                         where rownum = 1
                           and deleted_flg is null
                           and (scadenza is null or scadenza > sysdate)
                           and mdate > v_data
                           and cartella_id in (select cartella_id
                                                 from cartella
                                                where canale_id = v_cid
                                                start with cartella_id = c_cur.cartella_id
                                              connect by prior cartella_id = parent_id);

                        v_json.put('new_docs', 1);

                        exception
                           when no_data_found then
                              null;
                     end;

                  else -- Viewer

                     begin
                        select 1
                          into v_keep
                          from utenti_documenti ud, documento d
                         where rownum          = 1
                           and ud.utente_id    = ng.g_utente_id
                           and ud.documento_id = d.documento_id
                           and d.deleted_flg is null
                           and (d.scadenza is null or d.scadenza > sysdate)
                           and d.mdate > v_data
                           and d.cartella_id in (select cartella_id
                                                   from cartella
                                                  where canale_id = v_cid
                                                  start with cartella_id = c_cur.cartella_id
                                                connect by prior cartella_id = parent_id);

                        v_json.put('new_docs', 1);

                        exception
                           when no_data_found then
                              null;
                     end;

                  end if;

               end if;

               v_jsonl.append(v_json.to_json_value);

            end if;

         end loop;

      end if;

      dbms_lob.createtemporary(p_out, true);
      v_jsonl.to_json_value().to_clob(p_out);

   end read_cartelle_path;

   procedure cartella_cancellabile(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      l_n      number := 1;
      v_ca     number;
      v_ch     number;

   begin

      v_ca := get_json_string(v_params, 'cartella_id');
      v_ch := get_json_string(v_params, 'cid');

      ng.check_ruolo(v_ch, 3);

      /* Una cartella NON è cancellabile se contiene docs o
         se almeno una delle cartelle figlie contiene docs */
      begin
         select 0
           into l_n
           from documento
          where rownum = 1
            and cartella_id in (select cartella_id
                                  from cartella
                                 where canale_id = v_ch
                                 start with cartella_id = v_ca
                               connect by prior cartella_id = parent_id);
         exception
            when no_data_found then
               null;
      end;

      p_out := '{ "cancellabile": ' || l_n || '}';

      /*select count(1) 
        into l_n
        from cartella
       where parent_id = v_ca
         and canale_id = v_ch;

      if l_n > 0 then
         return '{ "cancellabile": 0}';
      else
         select count(*)
           into l_n
           from documento
          where cartella_id = v_ca
            and canale_id = v_ch;

         if l_n > 0 then
            return '{ "cancellabile": 0}';
         else
            return '{ "cancellabile": 1}';
         end if;

     end if;*/

   end cartella_cancellabile;

   procedure rinomina(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_ca     number;
      v_nome   cartella.nome%type;
      l_count  number;
      l_parent number;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_ca   := get_json_string(v_params, 'id');
      v_nome := get_json_string(v_params, 'nome');

      ng.check_ruolo_cartella(v_ca, 3);
      
      select parent_id
        into l_parent
        from cartella
       where cartella_id = v_ca;

      select count(1)
        into l_count
        from cartella
       where nome = v_nome
         and parent_id = l_parent;
       
      if l_count > 0 then
        raise_application_error(-20007, 'Nome cartella esistente nel path selezionato');
      end if;

      update cartella
         set nome = v_nome
       where cartella_id = v_ca;

      p_out := '{ "esito": "ok" }';

   end rinomina;

   procedure sposta(p_params in varchar2, p_out out clob) as

      v_params     pljson := pljson(p_params);
      v_ch         number;
      v_ca         number;
      v_dest       number;
      v_padre      number;
      v_dummy      number;
      v_ch_dest    number;
      v_nome       varchar2(1000);
      l_count      number;

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      dbms_lob.createtemporary(p_out, true);

      v_ca   := get_json_string(v_params, 'id');
      v_dest := get_json_string(v_params, 'destinazione');

      if v_ca = v_dest then
         pljson('{ "esito": "Nothing to do"}').to_json_value().to_clob(p_out);
         return;
      end if;

      ng.check_ruolo_cartella(v_ca, 3);
      ng.check_ruolo_cartella(v_dest, 3);

      select parent_id, canale_id, nome
        into v_padre,   v_ch,      v_nome
        from cartella
       where cartella_id = v_ca;

      if v_padre = v_dest then
         pljson('{ "esito": "Nothing to do"}').to_json_value().to_clob(p_out);
         return;
      end if;

      select canale_id
        into v_ch_dest
        from cartella
       where cartella_id = v_dest;

      if v_ch <> v_ch_dest then
         raise_application_error(-20005, 'Non è possibile spostare una cartella in un altro canale');
      end if;

      --una cartella con lo stesso nome non deve esistere nello stesso path
      select count(1)
        into l_count
        from cartella
       where nome = v_nome
         and parent_id = v_dest;
       
      if l_count > 0 then
        raise_application_error(-20007, 'Nome cartella esistente nel path selezionato');
      end if;

      /* Una cartella NON è spostabile in una delle cartelle figlie */
      /*begin
         select 0
           into v_dummy
           from cartella
          where v_padre_dest in (select cartella_id
                                   from cartella
                                  where canale_id = v_ch
                                  start with cartella_id = v_ca
                                connect by prior cartella_id = parent_id);

         raise_application_error(-20006, 'Non è possibile spostare una cartella in una delle sue discendenti');

         exception
            when no_data_found then
               null;
      end;*/

      update cartella
         set parent_id = v_dest
       where cartella_id = v_ca;

      -- Una cartella NON è spostabile se rompe la gerarchia
      select count(*)
        into v_dummy
        from cartella
       where connect_by_iscycle = 1
     connect by nocycle prior cartella_id = parent_id;

      if v_dummy > 0 then
         raise_application_error(-20006, 'Non è possibile spostare una cartella in una delle sue discendenti');
      end if;

      ng.log('Spostata cartella ' || v_ca || ' - padre precedente ' || v_padre || ' - nuovo padre ' || v_dest);

      p_out := '{ "esito": "ok" }';

   end sposta;

   procedure get_ruolo_canale_by_cartella(p_params in varchar2, p_out out clob) as

      v_params   pljson := pljson(p_params);
      v_cid      number;
      v_cartella pljson;
      v_canale   pljson;
      v_ruolo    pljson;
      v_out      pljson := pljson();

   begin

      v_cid := get_json_string(v_params, 'id');

      read(v_cid, p_out);
      v_cartella := pljson(p_out);
      no_canale.read(to_number(get_json_string(v_cartella, 'canale_id')), p_out);
      v_canale := pljson(p_out);
      no_utenti_canali.read_ruolo('{ canale_id: '||get_json_string(v_cartella, 'canale_id')||'}', p_out);
      v_ruolo := pljson(p_out);

      if v_ruolo.get('ruolo').get_string = 0 then

         raise_application_error(-20111, 'Privilegi insufficienti');

      end if;

      v_out.put('cartella', v_cartella);
      v_out.put('canale', v_canale);
      v_out.put('ruolo', v_ruolo);

      v_out.to_json_value().to_clob(p_out);

   end get_ruolo_canale_by_cartella;

   procedure associa(p_params in varchar2, p_out out clob) as

      v_params pljson := pljson(p_params);
      v_cid    cartella.cartella_id%type;
      v_uid    utente.utente_id%type;
      v_utenti pljson_list := pljson_list();

   begin

      if ng.g_utente_id is null then
         raise_application_error(-20403, 'Unauthorized');
      end if;

      v_cid := get_json_string(v_params, 'id');

      ng.check_ruolo_cartella(v_cid, 3);

      v_utenti := pljson_list(v_params.get('utenti'));

      for y in 1..v_utenti.count loop

         v_uid := pljson_value.get_number(v_utenti.get(y));

         insert into utenti_documenti (documento_id, utente_id)
         select documento_id, v_uid
           from documento
          where cartella_id in (select car.cartella_id
                                  from cartella car, canale can, azienda azi
                                 where car.canale_id  = can.canale_id
                                   and can.azienda_id = azi.azienda_id
                                 start with car.parent_id = v_cid
                               connect by prior car.cartella_id = car.parent_id)
            and documento_id not in (select documento_id
                                       from utenti_documenti
                                      where utente_id = v_uid);

      end loop;

      p_out := '{ "esito": "ok" }';

   end associa;

end no_cartella;
/