create or replace package no_cartella as

   function lazy_read(p_id in number)
     return pljson;

   procedure read(p_pars in varchar2, p_out out clob);

   procedure read(p_id in number, p_out out clob);

   procedure create_(p_obj in clob, p_json out varchar2);

   procedure update_(p_id   in number,
                     p_json in out varchar2);

   procedure delete_(p_id in number);

   procedure read_cartelle_path (p_params in varchar2, p_out out clob);

   procedure rinomina(p_params in varchar2, p_out out clob);

   procedure sposta(p_params in varchar2, p_out out clob);

   procedure get_ruolo_canale_by_cartella(p_params in varchar2, p_out out clob);

   procedure associa(p_params in varchar2, p_out out clob);

end no_cartella;
/