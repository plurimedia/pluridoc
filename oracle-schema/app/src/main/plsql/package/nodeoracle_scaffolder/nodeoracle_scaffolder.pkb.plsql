create or replace package body nodeoracle_scaffolder
as

    type vc_tab is table of varchar2(100); 
    flds vc_tab;
    flds_type vc_tab;

    procedure create_no_package(p_name varchar2)
    as
      p_sql varchar2(32000);
    begin

      execute immediate 'create or replace package NO_'||p_name
             ||chr(10)||'as'
             ||chr(10)||'    function lazy_read(p_id in number)'
             ||chr(10)||'      return pljson;'
             ||chr(10)||''
             ||chr(10)||'    procedure read (p_pars in varchar2, p_out out clob);'
             ||chr(10)||''
             ||chr(10)||'    function read (p_id in number, p_out out clob);'
             ||chr(10)||''
             ||chr(10)||'    procedure create_(p_obj in clob, p_json out varchar2);'
             ||chr(10)||''
             ||chr(10)||'    procedure update_(p_id   in number,'
             ||chr(10)||'                      p_json in out varchar2);'
             ||chr(10)||''
             ||chr(10)||'    procedure delete_(p_id in number);'
             ||chr(10)||''
             ||chr(10)||'    --procedure custom (p_params in varchar2, p_out out clob);'
             ||chr(10)||''
             ||chr(10)||'end NO_'||p_name||';';

      p_sql :=          'create or replace package body NO_'||p_name
             ||chr(10)||'as'
             ||chr(10)||''
             ||chr(10)||'   type id_table is table of number;'
             ||chr(10)||''
             ||chr(10)||'   function get_json_string(p_j in pljson, p_name in varchar2)'
             ||chr(10)||'   return varchar2'
             ||chr(10)||'   as'
             ||chr(10)||'   begin'
             ||chr(10)||'     return p_j.get(p_name).get_string;'
             ||chr(10)||'     exception when others then '
             ||chr(10)||'       begin'
             ||chr(10)||'         return replace(p_j.get(p_name).to_char, ''"'', '''');'
             ||chr(10)||'       exception when others then'
             ||chr(10)||'         return null;'
             ||chr(10)||'       end;'
             ||chr(10)||'   end;'
             ||chr(10)||''
             ||chr(10)||'   function lazy_read(p_id in number)'
             ||chr(10)||'     return pljson'
             ||chr(10)||'   as'
             ||chr(10)||''
             ||chr(10)||'      v_json pljson;'
             ||chr(10)||''
             ||chr(10)||'   begin'
             ||chr(10)||''
             ||chr(10)||'      for c_cur in (select *'
             ||chr(10)||'                      from '||p_name
             ||chr(10)||'                     where '||flds(1)||' = p_id) loop'
             ||chr(10)||''
             ||chr(10)||'         v_json:= pljson();'
             ||chr(10)||'         v_json.put(''_id'', c_cur.'||flds(1)||');';
             
      for i in 2 .. flds.count
        loop
       
        if flds_type(i) = 'date' then
          p_sql := p_sql||chr(10)||'         v_json.put('''||flds(i)||''', to_char(c_cur.'||flds(i)||', nodeoracle_gateway.g_date_format));';
        else
          p_sql := p_sql||chr(10)||'         v_json.put('''||flds(i)||''', c_cur.'||flds(i)||');';
        end if;
      end loop;

      p_sql := p_sql||chr(10)||''
             ||chr(10)||'      end loop;'
             ||chr(10)||''
             ||chr(10)||'      return v_json;'
             ||chr(10)||''
             ||chr(10)||'   end lazy_read;'
             ||chr(10)||''
             ||chr(10)||'   procedure read(p_pars in varchar2, p_out out clob)'
             ||chr(10)||'   as'
             ||chr(10)||''
             ||chr(10)||'      v_json_list pljson_list:= pljson_list();'
             ||chr(10)||'      l_pars      pljson:= pljson(p_pars);'
             ||chr(10)||'      v_ids       id_table := id_table();'
             ||chr(10)||''
             ||chr(10)||'   begin'
             ||chr(10)||''
             ||chr(10)||'      select '||flds(1)
             ||chr(10)||'        bulk collect into v_ids'
             ||chr(10)||'        from '||p_name
             ||chr(10)||'       --order by nome;'
             ||chr(10)||'       ;'
             ||chr(10)||''
             ||chr(10)||'      if v_ids.count > 0 then'
             ||chr(10)||'         for/*all*/ i in v_ids.first .. v_ids.last loop'
             ||chr(10)||'            v_json_list.append(lazy_read(v_ids(i)).to_json_value);'
             ||chr(10)||'         end loop;'
             ||chr(10)||'      end if;'
             ||chr(10)||''
             ||chr(10)||'      dbms_lob.createtemporary(p_out, true);'
             ||chr(10)||'      v_json_list.to_json_value().to_clob(p_out);'
             ||chr(10)||''
             ||chr(10)||'   end read;'
             ||chr(10)||''
             ||chr(10)||'   function read(p_id in number)'
             ||chr(10)||'     return varchar2'
             ||chr(10)||'   as'
             ||chr(10)||''
             ||chr(10)||'      v_json pljson:= lazy_read(p_id);'
             ||chr(10)||''
             ||chr(10)||'   begin'
             ||chr(10)||''
             ||chr(10)||'      dbms_lob.createtemporary(p_out, true);'
             ||chr(10)||'      v_json.to_json_value().to_clob(p_out);'
             ||chr(10)||''
             ||chr(10)||'   end read;'
             ||chr(10)||''
             ||chr(10)||'   procedure create_(p_obj in clob, p_json out varchar2)'
             ||chr(10)||'   as'
             ||chr(10)||''
             ||chr(10)||'      v_rec '||p_name||'%rowtype;'
             ||chr(10)||'      v_json pljson:= pljson(p_obj);'
             ||chr(10)||''
             ||chr(10)||'   begin'
             ||chr(10)||''
             ||chr(10)||'      v_rec.'||flds(1)||' := seq_'||p_name||'.nextval;';
      
      for i in 2 .. flds.count
        loop
dbms_output.put_line(flds(i)||' '||flds_type(i));
        
        if flds_type(i) = 'date' then
          p_sql := p_sql||chr(10)||'      v_rec.'||flds(i)||' := sysdate;';
        else
          p_sql := p_sql||chr(10)||'      v_rec.'||flds(i)||' := get_json_string(v_json, '''||flds(i)||''');';
        end if;
      end loop;
/*TODO : aggiungere un pezzo che in caso di number 1,0 faccia questo 
      if nvl(get_json_string(v_json, 'visibile'), 'false') = 'true' then
        v_rec.visibile := 1;
      else
        v_rec.visibile := 0;
      end if;
*/
      
      p_sql:=p_sql||chr(10)||''
             ||chr(10)||'      insert into '||p_name
             ||chr(10)||'      values v_rec'
             ||chr(10)||'      returning '||flds(1)||' into v_rec.'||flds(1)||';'
             ||chr(10)||'      commit;'
             ||chr(10)||''
             ||chr(10)||'      v_json.put(''_id'', v_rec.'||flds(1)||');'
             ||chr(10)||'      p_json := v_json.to_char();'
             ||chr(10)||''
             ||chr(10)||'   end create_;'
             ||chr(10)||''
             ||chr(10)||'   procedure update_(p_id   in number,'
             ||chr(10)||'                     p_json in out varchar2)'
             ||chr(10)||'   as'
             ||chr(10)||''
             ||chr(10)||'      v_rec '||p_name||'%rowtype;'
             ||chr(10)||'      v_json pljson:= pljson(p_json);'
             ||chr(10)||''
             ||chr(10)||'   begin'
             ||chr(10)||''
             ||chr(10)||'      select * into v_rec'
             ||chr(10)||'        from '||p_name
             ||chr(10)||'       where '||flds(1)||' = p_id;'
             ||chr(10)||'';

      for i in 2 .. flds.count
        loop
        
        if flds_type(i) = 'date' then
          p_sql := p_sql||chr(10)||'      v_rec.'||flds(i)||' := sysdate;';
        else
          p_sql := p_sql||chr(10)||'      v_rec.'||flds(i)||' := get_json_string(v_json, '''||flds(i)||''');';
        end if;
      end loop;

      p_sql := p_sql||chr(10)||''
             ||chr(10)||'      update '||p_name
             ||chr(10)||'         set row = v_rec'
             ||chr(10)||'       where '||flds(1)||' = p_id;'
             ||chr(10)||''
             ||chr(10)||'   end update_;'
             ||chr(10)||''
             ||chr(10)||'   procedure delete_(p_id in number)'
             ||chr(10)||'   as'
             ||chr(10)||'   begin'
             ||chr(10)||''
             ||chr(10)||'      delete '||p_name
             ||chr(10)||'       where '||flds(1)||' = p_id;'
             ||chr(10)||''
             ||chr(10)||'   end delete_;'
             ||chr(10)||''
             ||chr(10)||'    --procedure custom (p_params in varchar2, p_out out clob)'
             ||chr(10)||'    --as'
             ||chr(10)||'    --  begin'
             ||chr(10)||'    --    null;'
             ||chr(10)||'    --end;'
             ||chr(10)||''
             ||chr(10)||'end NO_'||p_name||';';
             
      execute immediate p_sql;
    
    end;


    procedure create_package(p_name varchar2)
    as
    begin
    
      execute immediate 'create or replace package '||p_name
             ||chr(10)||'as'
             ||chr(10)||''
             ||chr(10)||'    procedure dummy;'
             ||chr(10)||''
             ||chr(10)||'end '||p_name||';';

      execute immediate 'create or replace package body '||p_name
             ||chr(10)||'as'
             ||chr(10)||''
             ||chr(10)||'    procedure dummy'
             ||chr(10)||'    as'
             ||chr(10)||'    begin'
             ||chr(10)||'      null;'
             ||chr(10)||'    end dummy;'
             ||chr(10)||''
             ||chr(10)||'end '||p_name||';';
    
    end;

    procedure check_name(p_name varchar2)
    as
      v_name  varchar2(29):= upper(p_name);
    begin
    
       for c_cur in (select object_name from user_objects where object_name in (v_name||'_M',v_name||'_V',v_name||'_C')) loop
        raise_application_error(-20001,'An object with name "'||c_cur.object_name||'" already exists.');
       end loop;
    
       --for c_cur in (select any_path from resource_view where any_path = plsqlmvc.BASE_XDB_PATH||p_name) loop
       -- raise_application_error(-20001,'An XDB directory "'||c_cur.any_path||'" already exists.');
       --end loop;

    end;
    
    procedure set_fields(p_name varchar2)
    as
      tb vc_tab;
      tb2 vc_tab;
    begin
    
      for c_cur in (select * from user_tab_columns where table_name=upper(p_name) order by column_id asc) loop
        begin
          tb.extend;
          tb2.extend;
          tb(tb.count) := lower(c_cur.column_name);
          tb2(tb2.count) := lower(c_cur.data_type);
        exception when others then
          tb := vc_tab(lower(c_cur.column_name));
          tb2 := vc_tab(lower(c_cur.data_type));
        end;
      end loop;

      flds := tb;
      flds_type := tb2;
      
    end;
    
    procedure create_service(p_name varchar2)
    as
      v_name  varchar2(29):= lower(p_name);
    begin
    
         check_name(v_name);
         set_fields(v_name);
         create_no_package(v_name);
         commit;

    end;
    
    procedure get_examples
    as
    begin
      dbms_output.put_line(q'{

var dbc= plsql($scope, 'no_cartella');

dbc.create({  })
    .success(function(cartella)
    {
    })
    .error(function(e)
    {
       growl.addErrorMessage(e.errore || 'Ops, problema tecnico riprova!'+JSON.stringify(e));
    });
      
dbc.update({  })
   .success(function(cartella)
   {
   })
   .error(function(e)
   {
      growl.addErrorMessage(e.errore || 'Ops, problema tecnico riprova!'+JSON.stringify(e));
   });

dbc.readAll({})
   .success(function(cartelle)
   {
   })
   .error(function()
   {
      growl.addErrorMessage('Ops, problema tecnico riprova!'+JSON.stringify(e));
   });

dbu.delete_(_id)
   .success(function() {
   })
   .error(function()
   {
      growl.addErrorMessage('Ops, problema tecnico riprova!');
   });

      }');
    end;

end nodeoracle_scaffolder;
/