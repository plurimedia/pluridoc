# CREAZIONE USER ORACLE
CREATE USER "EXTRANET_PLURI" IDENTIFIED BY PASS;
ALTER USER "EXTRANET_PLURI" DEFAULT ROLE ALL;
GRANT CREATE PROCEDURE TO "EXTRANET_PLURI";
GRANT CREATE VIEW TO "EXTRANET_PLURI";
GRANT CREATE SYNONYM TO "EXTRANET_PLURI";
GRANT EXECUTE ON "SYS"."UTL_FILE" TO "EXTRANET_PLURI";
GRANT "CONNECT" TO "EXTRANET_PLURI";
GRANT "RESOURCE" TO "EXTRANET_PLURI";
GRANT EXECUTE ON "SYS"."DBMS_CRYPTO" TO "EXTRANET_PLURI";


declare id number;
begin
DBMS_JOB.SUBMIT(what => 'begin no_report_sintesi.create_; end;', interval => 'SYSDATE+1/24', job => id);
end;

# USO:
clonare

```
npm install

bower install

export NODE_ENV=development

node start.js

git push heroku HEAD:master
```

# UPLOAD DEI FILES

si svolge in due parti : prima viene invocato routes/transfer.js che ci restituisce il signed url di amazon, poi viene chiamato lib/file.js procedura write

ATTENZIONE : se dovessimo mai creare un nuovo bucket per l'upload dei files (ad esempio uno criptato) ricordarsi di mettere i permessi CORS

```
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
<CORSRule>
    <AllowedOrigin>*</AllowedOrigin>
    <AllowedMethod>GET</AllowedMethod>
    <AllowedMethod>POST</AllowedMethod>
    <AllowedMethod>PUT</AllowedMethod>
    <AllowedHeader>*</AllowedHeader>
</CORSRule>
</CORSConfiguration>
```

# trasformazione in pluridoc
crearsi in locale un file .env con il seguente contenuto

AWS_ACCESS_KEY_ID=AKIAJRQ7BS65L6FWDPRQ
AWS_SECRET_KEY=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
AWS_BUCKET=pluridoc
PORT=8080
DB_USER=pluridocmt_svil
DB_PWD==NvzwJ7Mtd=P4sEN
DB_CNTSTRING=plurimedia.c6zt09twmyxj.eu-central-1.rds.amazonaws.com/ORCL
LANGUAGE=it
MANDRIL_API_KEY=e__P8Da15NkrcnL5XBRH_g

ho configurato una pipeline di deploy come descritto qui
http://plurimedia.wikidot.com/wiki:heroku-bitbucket

per far si che Mandril mandi mail con sender diverso da info@plurimedia.it, bisogna aggiungere dalla sua interfaccia il dominio da cui mandiamo la mail, e poi configurare il DNS come solo Roberta sa fare :)

# appunti su passport
ho dovuto estrarre la logica di autenticazione dalla route e importare le route dell'utente in modo un po diverso da come era prima.
Dopodichè ho installato passport e passport-local per implementare la nostra autenticazione.
IMPORTANTE in app.js questa riga
app.use(passport.initialize());

Tutto il resto è nel file routes/user.js

Implemento anche passport-http per la basic auth delle api

# swagger

abbiamo creato due api di esempio documentate con swagger, si raggiungono all'url
/rest/api-docs

# api per app esterne

abbiamo creato come su BA backend delle api protette da token + basic auth.
alla rotta /ext-auth/secure/tokens con basic auth si ottiene un token valido per le api
/ext-auth/secure/loginEntries/:daData
/ext-auth/secure/loginEntries/:daData/:aData
/ext-auth/secure/utenti

# appunti sulle classi di storage di amazon

con una query simile alla sguente possiamo individuare i file modificati-scaricati negli ultimi tot giorni, e spostarli in classi di storage più economiche con la funzione in file.js changeStorageClass
```
select d.documento_id, d.name, d.mdate, d.canale_id, a.cid, sysdate - d.mdate ggmod,
(select sysdate - nvl(max(l.cdate), sysdate) from logs l where tipo='DOWNLOAD' and l.documento_id = d.documento_id)--, sysdate - l.cdate ggdown
from documento d, azienda a, canale c
where d.mdate < sysdate - 30
and d.canale_id = c.canale_id
and c.azienda_id = a.azienda_id
--and d.classe_s3 = ???
--and d.doc_size > 100000
and d.documento_id not in (select distinct l.documento_id
from logs l
where l.tipo = 'DOWNLOAD'
and l.cdate > sysdate - 30)
```

sarebbe anche il caso di aggiungere il campo classe_s3 per segnarsi la classe di storage dei file nel db e velocizzare un eventuale cron