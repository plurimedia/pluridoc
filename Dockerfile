FROM centos:7
USER root

# environment pre-requirements (nodejs 14 repo)
RUN curl -sL https://rpm.nodesource.com/setup_16.x | bash -

# install yum needed packages
RUN yum groupinstall -y "Development tools"
RUN yum -y install nodejs git wget && yum clean all -y

# pull from bitbucket repository
RUN echo "pulling from $REPOSITORY branch"
RUN git clone --branch $REPOSITORY https://bitbucket.org/plurimedia/pluridoc.git /opt/pluridoc

# install oracle client
RUN wget https://cloudev.s3-eu-west-1.amazonaws.com/oracle-instantclient19.6-{basic,devel}-19.6.0.0.0-1.x86_64.rpm && \
    yum -y install local oracle-instantclient19.6-{basic,devel}-19.6.0.0.0-1.x86_64.rpm && \
    rm -vf *.rpm && \
    mkdir /opt/oracle && \
    ln -s /usr/lib/oracle/19.6/client64 /opt/oracle/instantclient

# creating non-root user and setting its permissions
RUN useradd nodejs && \
    chown -R nodejs:nodejs /opt && \
    chown -R nodejs:nodejs /usr/lib/oracle/ && \
    chmod g+rwX -R /opt && \
    chmod g+rX -R /usr/lib/oracle/

# expose port
EXPOSE 8080

# switch to non-root user
USER nodejs

# set prefix to nodejs directory, no need to use sudo on -g flag
RUN mkdir /home/nodejs/.npm-global &&\
    npm config set prefix '/home/nodejs/.npm-global'

# npm install dependencies
RUN cd /opt/pluridoc && \
    npm install -g bower && \
    bower install && \
    npm install

WORKDIR /opt/pluridoc

RUN node create-keys.js

CMD node start.js
