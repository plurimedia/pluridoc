var express= require('express'),
    bodyParser = require('body-parser'),
    path= require('path'),
    sessions = require('client-sessions'),
    helmet = require('helmet'),
    //morgan = require('morgan'),

    company= require('./server/middleware/company'),
    user= require('./server/middleware/user'),

    css = require('./server/routes/css'),
    users = require('./server/routes/users'),
    docs = require('./server/routes/docs'),
    logsApis = require('./server/routes/api-logs'),
//    spid = require('./server/routes/spid'),
    transfer = require('./server/routes/transfer'),
    Email = require('./server/routes/email'),
    restApis = require('./server/routes/api-rest'),
    plsql= require('oracledb-plsql').plsql,
    conf = require('./server/config'),
    passport = require('passport'),
    swaggJsDoc = require('swagger-jsdoc'),
    swaggUi = require('swagger-ui-express'),

    app= express()

require('better-logging')(console, {
  format: ctx => `${ctx.time} ${ctx.date} ${ctx.type} ${ctx.unix} ${ctx.msg}`
})

// force https in production
app.use((req, res, next) => {
   if (process.env.NODE_ENV === 'production')
   {
      if (req.headers['x-forwarded-proto'] !== 'https') // || req.hostname.indexOf('heroku') !== -1
      {
         //console.log('redirect https', req.headers['x-forwarded-proto'], req.hostname.indexOf('heroku'))
         try {
            res.redirect('https://'+req.hostname+req.url)
         } catch (e) {
            console.error('errore redirect https', e, req.headers)
            next()
         }
      }
      else
         next()
   }
   else
      next()
})

app.use(function (req, res, next)
{
   if (req.is('application/octet-stream'))
      res.connection.setTimeout(0)

   next()
})

app.use(helmet({ contentSecurityPolicy: false }))

// API logs
// app.get('/loginEntries/:daData/:aData', logsApis.loginEntries);
// app.get('/loginEntries/:daData', logsApis.loginEntries);
app.use('/ext-auth', require('./server/routes/ext-auth'))

app.use(sessions
({
   cookieName: 'session', // cookie name dictates the key name added to the request object
   secret: 'uweNDsb34VCFJkFVx1EsMcdF',
   duration: 8 * 60 * 60 * 1000 // 8h
}))

app.use(bodyParser.urlencoded({
   extended: true
}))
app.use(bodyParser.json())

app.use(passport.initialize());

app.use(company.load)
app.use(user.load)

// app.use('/spid', spid)

app.get('/rest/css/variables.css', css.get)

app.get('/rest/email/:id', Email.stato) // controlla lo stato di una mail
app.post('/rest/email', Email.send) // invia una mail

app.get('/rest/docs/download/:id/:cid/:attach/:compress', docs.download)
app.get('/rest/docs/delete/:id/:cid', docs.delete)
app.post('/rest/docs/compress', docs.compressPdf)

app.use(users)
app.get('/rest/user', user.get)

app.get('/rest/company', company.get)

app.post('/secure/transfer', transfer.upload)

app.use(plsql(app, {
   oracle_cn: conf.oracle_cn,
   pkgGateway: 'NODEORACLE_GATEWAY',
   formatUserFunction: user.formatUser
}))

app.post('/rest/server/log', function (req, res) { console.log(req.body) })

app.use('/rest/api', restApis)

const swaggOpt ={
   swaggerDefinition:{
      info: {
         version: "1.0.0",
         title:'Elenco API PluriDoc',
      }
   },
   apis: ["app.js","./server/routes/api-rest.js"]
}

const opt= {
   customSiteTitle: "PluriDoc",
   customfavIcon: "./client/img/pluridoc/favicon.ico"
}
const swaggDocs = swaggJsDoc(swaggOpt);
app.use("/rest/api-docs",swaggUi.serve,swaggUi.setup(swaggDocs, opt));

app.use('/', express.static(path.join(__dirname, 'client')))

module.exports = app

RegExp.fromString = function(str) {
    return new RegExp((str+'').replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1"))
}

const Zoom = require('./server/routes/zoom')

app.get('/zoom/get', Zoom.getMeeting)
