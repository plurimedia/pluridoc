const ILovePDFApi = require('@ilovepdf/ilovepdf-nodejs');
const ILovePDFFile = require('@ilovepdf/ilovepdf-nodejs/ILovePDFFile');
const ilovepdf = new ILovePDFApi("project_public_f3efa95f75834df8ec27e7553d2d6e66_YtrzTea934bff535a3ea5ba1de8fbfbe74401", "secret_key_f8ac180265c488a4352d37bf0bb5e475_9Fd7od0925058e905c864e48347e8bb89f219");
const fs = require('fs')

    // Choose your processing tool and create a new task
let myTaskCompress = ilovepdf.newTask('compress');

    // For this example, await notation will be used instead of
    // promises.
const fun=async()=>{
  let a = await myTaskCompress.start()

    console.log("step1")
    const file = new ILovePDFFile('scripts/BrianzAcque.pdf');

    await myTaskCompress.addFile(file);
    console.log("step2")

    // Execute the task
    await myTaskCompress.process();

    // Download the packaged files
    const data = await myTaskCompress.download();
    fs.writeFileSync('scripts/BrianzAcqueMini.pdf', data);

}
    // Add files to task for upload
fun()