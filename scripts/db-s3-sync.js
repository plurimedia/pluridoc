
/* script per controllare se i files sul nostro db sono su s3 e viceversa

launch per vscode : 
{
  "type": "node",
  "request": "launch",
  "name": "pluridoc sync db-s3",
  "program": "${workspaceFolder}/scripts/db-s3-sync.js",
  "skipFiles": [
    "<node_internals>/**"
  ],
  "env": {
      "AMBIENTE": "dev",
      "PATH": "/Users/damon/.nvm/versions/node/v18.8.0/bin"
  }
}
*/
let cid=process.argv[2]
const plsql= require('oracledb-plsql'),
  conf = require('../server/config'),
  { S3Client, HeadObjectCommand, DeleteObjectCommand, PutObjectCommand, CopyObjectCommand,ListObjectsCommand } = require('@aws-sdk/client-s3')

const s3 = new S3Client({
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_KEY
  },
  signatureVersion: 'v4',
  region: 'eu-west-1'
})


const get_db_docs = async (cit) => {
  plsql.init({
    oracle_cn: conf.oracle_cn,
    pkgGateway: 'NODEORACLE_GATEWAY'})

  const results = await plsql.method('all_docs_s3', 'no_documento', JSON.stringify({ nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: cit }),{})
  return results
}

let dbDocs=[]

const init = async () => {
  const res=[]
  let j=0
  dbDocs = await get_db_docs(cid)


  for(let i=0;i<dbDocs.length;i++){
    const command = new HeadObjectCommand({
      Bucket: process.env.AWS_BUCKET,
      Key: dbDocs[i].percorso
    })

    try {
      const item = await s3.send(command)
      
    } catch (e) {
      
      res[j]=dbDocs[i].percorso
      console.log(res[j])
      j++
    }
  }
}

const keys=2
const bucketParams = { Bucket: process.env.AWS_BUCKET,MaxKeys: keys,Marker:"",Prefix: cid}

const run = async () => {
  const arr=[]
  let i2=0
  try {
    while(true){
      const data = await s3.send(new ListObjectsCommand(bucketParams))
     
      for(let i=0;i<keys;i++){
          const local=data.Contents[i].Key
          if(local!=undefined){
            arr[i2]=local
          }
          if(i===keys-1){
            bucketParams.Marker=data.Contents[i].Key
          }
          i2++
      }
    }
   
  } catch (err) {
    if(arr==undefined) console.log(err)
    else return arr
  }
  
}
const checkFile = async (nome,cid) =>
{
  plsql.init({
    oracle_cn: conf.oracle_cn,
    pkgGateway: 'NODEORACLE_GATEWAY'})

  const clob_string = await plsql.method(
    'public_',
    'no_documento',
    JSON.stringify({ nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: cid }),
    { nome: nome }
    
  )

  return clob_string
}

const read=async()=>{
  const finale=[]
  const content=[]
  const arr= await run();
  let j=0,z=0
  for(let i=0;i<arr.length;i++){
    if(arr[i].slice(-1)!="/"){
      content[j]=await checkFile(arr[i],cid)//
      if(content[j].esito=="ko"){
        finale[z]=arr[i];
        console.log(finale[z])
        z++
      }
      j++
      
    }
  } 
}

const end=async()=>{
  console.log("file non esistenti sul db oracle: ")
  await read()
  console.log("\nfile non esistenti su S3: ")
  await init()
}


end()