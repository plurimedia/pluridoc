var intercettaTutto = false;
app.config(['$qProvider', function($qProvider)
   {
      $qProvider.errorOnUnhandledRejections(false);
   }])
   .config([ '$httpProvider', function($httpProvider)
   {
      console.log('[DEBUG SETUP] add interceptor');
      $httpProvider.interceptors.push(function($q)
      {
         return {
            'request': function(config)
            {
               if (intercettaTutto)
                  console.log('[INTERCEPTOR] request intercepted ' + config.url);

               if (config.url.indexOf('/rest/') == 0)
               {
                  console.log('[INTERCEPTOR] request intercepted url match ' + config.url);
                  try {
                     if (interceptorResponse)
                     {
                        console.log('[INTERCEPTOR] request intercepted url match cancel request ' + config.url);
                        // promise that should abort the request when resolved.
                        config.timeout = canceller.promise;
                        return config;
                     }
                  } catch(e) {
                     console.log('[INTERCEPTOR] request intercepted url match interceptorResponse not defined ' + config.url);
                     return config;
                  }
               }

               return config;
            },
            'response': function(response)
            {
               // do something on success
               console.log('[INTERCEPTOR] response', response);
               if (intercettaTutto)
                  console.log('[INTERCEPTOR] response intercepted ' + response.config.url, response);

               if (response.config.url.indexOf('/rest/') == 0)
               {
                  console.log('[INTERCEPTOR] response intercepted url match ' + response.config.url, response);
                  try {
                     if (interceptorResponse)
                     {
                        if (interceptorResponse.status != 200)
                        {
                           console.log('[INTERCEPTOR] response intercepted url match sending error ' + response.config.url);
                           response.status = interceptorResponse.status;
                           response.data.status = response.status;
                           response.statusText = "Internal Server Error";
                           response.data = interceptorResponse.data;
                           interceptorResponse = undefined;
                           return $q.reject(response);
                        }
                        else
                        {
                           console.log('[INTERCEPTOR] response intercepted url match sending confirm ' + response.config.url);
                           interceptorResponse = undefined;
                           return $q.resolve(response);
                        }
                     }

                     return response || $q.when(response);
                  } catch (e) {
                     console.log('[INTERCEPTOR] interceptorResponse not defined ' + response.config.url);
                     return response || $q.when(response);
                  }
               }

               return response || $q.when(response);
            },

            // optional method
            'responseError': function(rejection)
            {
               console.log('[INTERCEPTOR] response error', rejection.config.url, rejection);
               return $q.reject(rejection);
            }
         };
      });
   }]);
