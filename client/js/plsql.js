app
    .factory('plsql', [ '$http', '$cacheFactory', '$q', function($http, $cacheFactory, $q) {
        var urlPrefix = '/rest/plsql/';
        var mycache = $cacheFactory('dataCache');
        return function(name, $scope) {
            var _notSaving = function() {
                if ($scope)
                    $scope.saving = false;
            }, _saving = function(req) {
                req.then(_notSaving);
                return req;
            }, _readAll = function(params, cache) {
                cache = typeof cache !== 'undefined' ? cache : false;
                return $http.get(urlPrefix + name, {
                    params : params, cache: cache
                });/*.success(function(data, status, headers, config) {
                 var contentType = headers('Content-Type');
                 //console.log("config:",config);
                 //console.log("data:",data);
                 //console.log("contentType:",contentType);
                 // this callback will be called asynchronously
                 // when the response is available
                 }).error(function(data, status, headers, config) {
                 // called asynchronously if an error occurs
                 // or server returns response with an error status.
                 });*/

            }, _read = function(id) {
                return $http.get(urlPrefix + name + '/' + id);
            }, _create = function(obj) {
                return _saving($http({
                    url : urlPrefix + name,
                    method : 'POST',
                    data : obj
                }));
            }, _update = function(obj) {
                return _saving($http({
                    url : urlPrefix + name + '/' + obj._id,
                    method : 'PUT',
                    data : obj
                }));
            }, _delete = function(id) {
                return _saving($http({
                    url : urlPrefix + name + '/' + id,
                    method : 'DELETE'
                }));
            }, _save = function(obj) {
                return obj._id ? _update(obj) : _create(obj);
            }, _method = function(method, obj, cache) {
                cache = typeof cache !== 'undefined' ? cache : false;
                var url= urlPrefix + 'pls/' + name + '/' + method;

                if (cache)
                {
                    // URL + dati di POST sono la chiave della mia cache
                    var cacheId = url + '*' + JSON.stringify(obj);
                    var cachedData = mycache.get(cacheId);
                    var deferred = $q.defer();

                    if (cachedData) // La chiave è già in cache: torno i dati che sono in cache
                        deferred.resolve(cachedData);
                    else // La chiave non è in cache: faccio la chiamata Ajax
                        $http({
                            url : url,
                            method : 'POST',
                            data : obj
                        })
                            .then(function(response)
                            {
                                mycache.put(cacheId, response.data); // Scrivo i dati in cache
                                deferred.resolve(response.data); // Torno i dati restituiti da $http
                            },
                            function(err)
                            {
                                deferred.reject(err.data);
                            });

                    return deferred.promise;
                }
                else // No cache: come funzionava prima
                    return $http({
                        url : url,
                        method : 'POST',
                        data : obj
                    });
            };

            return {
                create : _create,
                read : _read,
                readAll : _readAll,
                update : _update,
                delete_ : _delete,
                save : _save,
                method : _method
            };
        };
    } ]);
