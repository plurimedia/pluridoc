app
    .filter('nvl', function()
    {
        return function(value, replacer) {
            return value ? value : (replacer ? replacer : '...');
        };
    })
    .filter('bytes', function()
    {
        return function(bytes, precision) {
            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '0 Mb';
            if (bytes===0) return '0';
            if (typeof precision === 'undefined') precision = 1;
            var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
                number = Math.floor(Math.log(bytes) / Math.log(1024));
            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
        }
    })
    .filter('cleanup', function()
    {
        var _cleanup= function (obj)
        {
            return cclone(obj,function (key,value)
            {
                if (key.indexOf&&key.indexOf('_')==0&&key!='_id')
                    return undefined;
                else
                    return value;
            });
        };

        return function(value) {
            return typeof value=='object' ? _cleanup(value) : value;
        };
    })
    .filter('unsafe', function($sce) {
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    })
    .filter('data', function() {
        return function(val) {
            return moment(val).format('DD/MM/YY');
        };
    })
    .filter('ora', function() {
        return function(val) {
            return moment(val).format('HH:mm');
        };
    })
    .filter('dataora', function() {
        return function(val) {
            return moment(new Date(val)).format('DD/MM/YY HH:mm');
        };
    })
    .filter('ruolo', function() {
        return function(val) {
            if (val == 1) return 'Viewer';
            if (val == 2) return 'Uploader light';
            if (val == 3) return 'Uploader';
            return 'Nessuno';
        }
    });
