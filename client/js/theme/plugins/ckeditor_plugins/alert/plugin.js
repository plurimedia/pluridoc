CKEDITOR.plugins.add( 'alert', {
    icons: 'alert',
    init: function( editor ) {
        editor.addCommand( 'insertAlert', {
            exec: function( editor ) {
                editor.insertHtml( '<div class="alert alert-warning">Inserisci qui il testo</div>');
            }
        });
        editor.ui.addButton( 'Alert', {
            label: 'Inserisci un messaggio di avviso',
            command: 'insertAlert',
            toolbar: "insert"
        });
    }
});