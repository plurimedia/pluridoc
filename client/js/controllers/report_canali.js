angular.module('intranetpluri')
    .controller('report_canali',
    ['$rootScope', '$scope', 'ngNotify', 'CONFIG', 'REPORT', 'CANALE',
function ($rootScope, $scope, ngNotify, CONFIG, REPORT, CANALE) {

    $scope.formatSizeUnits = function (bytes) {
        if (bytes >= 1073741824) {
            bytes = (bytes / 1073741824).toFixed(2) + ' GB';
        }
        else if (bytes >= 1048576) {
            bytes = (bytes / 1048576).toFixed(2) + ' MB';
        }
        else if (bytes >= 1024) {
            bytes = (bytes / 1024).toFixed(2) + ' KB';
        }
        else if (bytes > 1) {
            bytes = bytes + ' bytes';
        }
        else if (bytes == 1) {
            bytes = bytes + ' byte';
        }
        else {
            bytes = '0 byte';
        }
        return bytes;
    }

    REPORT.read_anno(moment().year())
        .then(function (dati) {
            $scope.ultimo = _.max(dati.data, function (el) {
                return parseInt(el.mese);
            });
        });

    REPORT.all_canali()
        .then(function (canali) {
            canali = canali.data;
            //canali = _.sortBy(canali, 'n_utenti').reverse();
            canali.forEach(function (c) {
                c.perc = ((c.total_size * 100) / c.total_channels_size).round(2);
            });
            $scope.canali = canali;

        });

    REPORT.get_job_status()
        .then(function(dati) {
            $scope.stato = dati.data;
        });

    $scope.utenti_channel = function () {
        $scope.nome_canale = this.canale.nome;
        delete $scope.utenti;
        CANALE.utenti_canale(this.canale.canale_id)
            .then(function (utcanale) {
                $scope.utenti = utcanale.data;
            }, function () {
                ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
    }

}]);
