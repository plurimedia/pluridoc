angular.module('intranetpluri').controller('docs-gruppi',
['$rootScope','$scope','$location','ngNotify','CONFIG','GRUPPO','CANALE',
function ($rootScope, $scope, $location, ngNotify, CONFIG, GRUPPO, CANALE)
{
   var $modalegroup= $('#creagruppo');
   var $modaleelimina= $('#eliminagruppo');

   function enable_users()
   {
      _.each($scope.users, function(u)
         {
            u.enabled = false;

            if ($scope.gruppo.utenti && $scope.gruppo.utenti.length)
               if (_.findWhere($scope.gruppo.utenti, { utente_id: u.utente_id })) u.enabled = true;
         });

      $modalegroup.modal('show');
   }

   var load= function()
   {
      $scope.loading= true;

      if (CANALE.selectedChannel())
      {
         $rootScope.selectedChannel = CANALE.selectedChannel();
         GRUPPO.all_canale(CANALE.selectedChannel().canale_id)
            .then(function (gruppi) {
               gruppi = gruppi.data;
               $scope.gruppi = gruppi;

               CANALE.utenti_canale(CANALE.selectedChannel().canale_id, 1)
                  .then(function (users) {
                     users = users.data;
                     $scope.users = users;
                     $scope.loading = false;
                  }, function () {
                     ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
                     $scope.loading = false;
                  });
            }, function (err) {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
               $scope.loading = false;
            })
      }
      else
      {
         ngNotify.set(CONFIG.messages.selezionare_canale, 'error');
         $location.path('canali');
         $scope.loading= false;
      }
   };

   $scope.selezionati= function()
   {
      if (!$scope.users) return 0;

      return _.filter($scope.users, function(el) { return el.enabled; }).length;
   }

   $scope.carica_gruppo= function(gr)
   {
      $scope.gruppo= gr;
   }

   $scope.modifica_gruppo= function(gr)
   {
      $scope.carica_gruppo(gr);
      //hack per le checkbox
      if (gr.visibile == 1) gr.visibile = true; else gr.visibile = false;
      enable_users();
   }

   $scope.scriviGruppo= function()
   {
      var gr= JSON.parse(JSON.stringify($scope.gruppo));

      gr.canale_id= CANALE.selectedChannel().canale_id;
      gr.tipo= 'UTENTE';

      gr.utenti= [];
      $scope.gruppo.utenti= [];
      $scope.users.forEach(function(u)
      {
         if (u.enabled)
         {
            gr.utenti.push(u.utente_id);
            $scope.gruppo.utenti.push({ utente_id: u.utente_id, nome: u.nome });
         }
      });

      GRUPPO.save(gr)
         .then(function()
         {
            ngNotify.set(CONFIG.messages.gruppo_salvato, 'success');//TODO : pensare ad un modo per differenziare messaggio di create e update
            $modalegroup.modal('hide');
            load();
         },
         function(err)
         {
            ngNotify.set(CONFIG.messages.selezionare_canale, 'error');
            $modalegroup.modal('hide');
         });
   }

   $scope.elimina_gruppo= function(gr)
   {
      GRUPPO.delete(gr.gruppo_id)
         .then(function()
         {
            ngNotify.set(CONFIG.messages.gruppo_cancellato, 'success');
            $modaleelimina.modal('hide');
            $scope.gruppi = _.filter($scope.gruppi, function(g) {
               return g.gruppo_id != gr.gruppo_id;
            });
         },
         function()
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            $modaleelimina.modal('hide');
         });
   }

   load();
}]);
