angular.module('intranetpluri').controller('report_tags',
['$rootScope', '$scope', '$http', 'ngNotify', 'CONFIG', 'REPORT', 'DOCUMENTO', 'CANALE',
function ($rootScope, $scope, $http, ngNotify, CONFIG, REPORT, DOCUMENTO, CANALE)
{
   $scope.files = [];
   $scope.canali = [];

   $scope.cercaTag = function(chiave)
   {
      return DOCUMENTO.find_tag(chiave);
   }

   $scope.resetSearch = function()
   {
      $scope.search = {};
      $scope.files = [];
   }

   $scope.recuperaDocs = function()
   {
      if ($scope.search && $scope.search.tags && $scope.search.tags.length)
      {
         $scope.loading = true;
         const obj = { tags: $scope.search.tags }
         if ($scope.search.canale) obj.canale_id = $scope.search.canale.canale_id
         DOCUMENTO.find_by_tags(obj)
         .then(function(res)
         {
            $scope.files = res.data;
            $scope.loading = false;
         }, function ()
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         });
      }
   }

   $scope.copyUrl= function(id, cid)
   {
      var dummy = document.createElement("input");
      document.body.appendChild(dummy);
      dummy.value = window.location.protocol + '//' + window.location.host + '/rest/docs/download/'+id+'/'+cid+'/Y';
      dummy.select();
      document.execCommand("copy");
      document.body.removeChild(dummy);
   }

   var get_canali = function()
   {
      $scope.loading = true;

      if ($rootScope.user.admin)
         CANALE.all()
            .then(function(canali)
            {
               $scope.canali = canali.data;
               $scope.loading = false;
            }, function()
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
      else
         CANALE.canali_utente()
            .then(function (canali)
            {
               $scope.canali = canali.data;
               $scope.loading = false;
            }, function (err)
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
   }

   get_canali();
}]);
