angular.module('intranetpluri')
    .controller('docs-impostazioni',
    ['$rootScope','$scope','$location','$timeout', 'ngNotify','CONFIG','CANALE',
function ($rootScope, $scope, $location, $timeout, ngNotify, CONFIG, CANALE)
{

    var $modaleblocca = $('#bloccacanale');
    var $modalesblocca = $('#sbloccacanale');
    $scope.nuovoNome = "";
    $scope.nuovoMittente = "";
    $scope.nuovoMessaggio = "";
    $scope.nomeEsistente = false;//per ora non è usato ma io un div nascosto lo metterei come nella creazione del canale

    $scope.block_channel= function()
    {
        var c = CANALE.selectedChannel();
        CANALE.blocca(c)
            .then(function(canale)
            {
                ngNotify.set(CONFIG.messages.canale_chiuso, 'success');
                $modaleblocca.modal('hide');
                load();
            });
    }

    $scope.unblock_channel= function()
    {
        var c = CANALE.selectedChannel();
        CANALE.sblocca(c)
            .then(function(canale)
            {
                ngNotify.set(CONFIG.messages.canale_aperto, 'success');
                $modalesblocca.modal('hide');
                load();
            });
    }

    $scope.modifica_canale= function()
    {
        var c = CANALE.selectedChannel();

        if (($scope.nuovoNome && $rootScope.user.ruolo==3) || ($scope.nuovoNome && $rootScope.user.admin==1))
        {
            $scope.nuovoNome= $scope.nuovoNome.replace(/\/|"|\'|,/g, '-');
            c.nome = $scope.nuovoNome;
            c.avviso = $scope.nuovoAvviso;
            c.mail_from_name = $scope.nuovoMittente;
            c.mail_nuovi_docs = $scope.nuovoMessaggio;

            CANALE.save(c)
                .then(function(canale)
                {
                    ngNotify.set(CONFIG.messages.canale_rinominato, 'success');
                    CANALE.setSelectedChannel(c);
                },function(err) {
                    if (err.data.message.indexOf('ORA-20998') > -1) {
                        ngNotify.set(CONFIG.messages.canale_nome_esistente, 'error');
                        $scope.nomeEsistente = true;
                    }
                    else
                        console.log(err);
                });
        }
    }

    var load= function()
    {
        if (CANALE.selectedChannel())
        {
            $scope.nuovoNome = CANALE.selectedChannel().nome;
            $scope.nuovoAvviso = CANALE.selectedChannel().avviso;
            $scope.nuovoMittente = CANALE.selectedChannel().mail_from_name;
            $scope.nuovoMessaggio = CANALE.selectedChannel().mail_nuovi_docs;
            $rootScope.selectedChannel = CANALE.selectedChannel();
        }
        else
        {
            ngNotify.set(CONFIG.messages.selezionare_canale, 'error');
            $location.path('canali');
        }
    };

    load();
}]);
