angular.module('intranetpluri')
.controller('docs',
['$rootScope', '$scope', '$http', '$location', '$window', '$timeout', 'ngNotify', 'CONFIG', 'CARTELLA', 'DOCUMENTO', 'CANALE', 'UTENTE',
function ($rootScope, $scope, $http, $location, $window, $timeout, ngNotify, CONFIG, CARTELLA, DOCUMENTO, CANALE, UTENTE)
{
   var $modalefolder = $('#nuovacartella');
   var $modaleassegna = $('#permessifile');
   var $modaleassocia = $('#associautenti');
   var $modalerinomina = $('#rinominacartella');
   var $modaleelimina = $('#eliminacartella');
   var $modaleeliminaf = $('#eliminafile');
   var $modalerinominaf = $('#rinominafile');
   var $modalerestore = $('#ripristinafile');
   var $modalesposta = $('#spostacartella');
   var $modalemeta = $('#metafile');
   var $modalescadenza = $('#scadenzafile');
   var ch_users = [];
   var penultimo_login = $rootScope.user.penultimo_login ? new Date($rootScope.user.penultimo_login) : null
   $scope.loading = true;
   $rootScope.uploader = false;
   $scope.search = {};

   $scope.recuperaDocs = function ()
   {
      if (!$scope.notAuthorized)
         DOCUMENTO.all(CARTELLA.selectedFolder()._id, CANALE.selectedChannel().canale_id, $rootScope.user.orderbydate)
            .then(function(docs)
            {
               if (penultimo_login)
               {
                  docs.data.forEach(function(d)
                  {
                     var dt = new Date(d.mdate)
                     if (dt.getTime() > penultimo_login.getTime()) d.isNew = 1
                  })
               }

               $scope.undeleted_files= docs.data.filter(d => d.deleted_flg !== 'Y');
               $scope.deleted_files= docs.data.filter(d => d.deleted_flg === 'Y');
               if (!$scope.deleted_files.length) $scope.ctrl.showdeleted = false;
               if (!$scope.undeleted_files.length && $scope.deleted_files.length)
                  $scope.ctrl.showdeleted = true;

               $scope.toggleDocs();
            },
            function()
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
   }

   $scope.toggleDocs = function ()
   {
      if ($scope.ctrl.showdeleted)
         $scope.files = $scope.deleted_files;
      else
         $scope.files = $scope.undeleted_files;
   }

   var init2= function()
   {
      breadCrumb();
      getSubfolders();
      $scope.recuperaDocs();
      $scope.selectedFolder = CARTELLA.selectedFolder();
      $scope.loading = false;
   };

   var init= function()
   {
      if (CANALE.selectedChannel())
      {
         $rootScope.user = UTENTE.user();
         $rootScope.uploader = ($rootScope.user.ruolo==3);
         $scope.selectedChannel = CANALE.selectedChannel();
         CARTELLA.cartelle_path(CANALE.selectedChannel().canale_id, $rootScope.user.penultimo_login)
            .then(function(folders)
            {
               folders = folders.data;
               $scope.allFolders= folders;

               // Rimappo i folders in una gerarchia per la direttiva angular-tree-view
               $scope.foldersTree= { folders: [] };
               if (folders.length > 2) // Solo se ci sono almeno due cartelle (oltre alla root)
               {
                  var map= {}, node;

                  for (var i=0; i < folders.length; i+=1)
                  {
                     node = folders[i];
                     node.folders = [];
                     map[node._id] = i; // use map to look-up the parents
                     if (node.padre)
                        folders[map[node.padre]].folders.push(node);
                     else
                        $scope.foldersTree.folders.push(node);
                  }
               }

               if (!CARTELLA.selectedFolder())
                  CARTELLA.setSelectedFolder(folders[0]);

               if (UTENTE.user().ruolo > 1) getUsers();

               init2();
            },function()
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
      }
      else
      {
         ngNotify.set(CONFIG.messages.selezionare_canale, 'error');
         $location.path('canali');
      }
   };

   $scope.assegno= false;
   $scope.processing= true;
   $scope.searching= false;
   $rootScope.showToolbar= true;
   $scope.alerts= [];
   $scope.ctrl= { showdeleted: false };

   $scope.closeAlert = function(index)
   {
      $scope.alerts.splice(index, 1);
   };

   $scope.newFolder = function()
   {
      $scope.nuovaCartella = null;
   };

   // Recupero la lista degli utenti abilitati a vedere uno specifico file (fid)
   // Richiamata dal bottone "occhio" della lista documenti
   $scope.recuperaUtenti = function(viewers)
   {
      $scope.doc_users= [];

      if (viewers.length)
      {
         viewers.forEach(function(v)
         {
            var ute= _.find(ch_users, function(us) { return us.utente_id == v; });

            if (ute && !_.find($scope.doc_users, function(u) { return u.nome === ute.nome; }))
               $scope.doc_users.push({ nome: ute.nome, ruolo: 'Viewer' });
         });
      }

      if ($scope.uploaders.length) $scope.doc_users = $scope.doc_users.concat($scope.uploaders);

      $modale.modal('show');
   }

   var getUsers= function()
   {
      $scope.uploaders= []; // Utenti con ruolo 2 o 3
      $scope.users= []; // Utenti con ruolo 1

      CANALE.utenti_canale(CANALE.selectedChannel().canale_id)
         .then(function(users)
         {
            users = users.data;
            ch_users= users;
            _.each(users, function(u)
            {
               if (u.ruolo == 1)
                  $scope.users.push(u);

               if (u.ruolo > 1)
                  $scope.uploaders.push({ nome: u.nome, ruolo: 'Uploader' });
            });
         }, function()
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         });
   }

   $scope.creaCartella = function()
   {
      if (!$scope.nuovaCartella) return;

      var nome= $scope.nuovaCartella.replace(/\/|"|\'|,/g, '-');

      CARTELLA.save({ parent_id: CARTELLA.selectedFolder()._id, nome: nome, canale_id: CANALE.selectedChannel().canale_id })
          .then(function(cartella)
          {
             init();
             ngNotify.set(CONFIG.messages.cartella_creata, 'success');
             $modalefolder.modal('hide');
          }, function(err)
          {
             if (err.data.message.indexOf('ORA-20007') > -1)
                ngNotify.set(CONFIG.messages.cartella_nome_esistente, 'error');
             else
                ngNotify.set(err.errore || CONFIG.messages.problema_tecnico+JSON.stringify(e), 'error');
          });
   }

   function breadCrumb()
   {
      try {
         $scope.breadcrumb = CARTELLA.breadCrumb();
      } catch (err) {
         if (err === 'notAuthorized')
            $scope.notAuthorized = 1;
         else
            throw err;
      }
   }

   function getSubfolders()
   {
      $scope.subfolders= _.filter($scope.allFolders, function(p)
      {
         return p.padre===CARTELLA.selectedFolder()._id;
      });
   }

   $scope.selectFolder= function()
   {
      CARTELLA.setSelectedFolder(this.cartella);
      init2();
   }

   $scope.selectBreadcrumb= function(p)
   {
      CARTELLA.setSelectedFolder(_.find($scope.allFolders, function(f) { return f.percorso === p; } ));
      init2();
   }

   $scope.open_rename= function(file)
   {
      $scope.doc = file;
      $scope.doc.name_new = file.name_orig;
      $timeout(function()
      {
         $modalerinominaf.modal('show');
      }, 500);
   }

   $scope.rename= function()
   {
      var file = $scope.doc;
      var re = /(?:\.([^.]+))?$/;
      var ext, ext_orig;


      if ($rootScope.user.ruolo != 3 ||
          !file ||
          !file.name_new ||
          file.name_new === file.name_orig) return;

      file.name_new= file.name_new.replace(/\/|"|\'|,/g, '-');

      ext = re.exec(file.name_new)[1];
      ext_orig = re.exec(file.name_orig)[1];
      if (!ext || ext !== ext_orig) file.name_new+= '.'+ext_orig;

      DOCUMENTO.rename(file._id, file.name_new).then(function()
      {
         file.name_orig = file.name_new;
         $modalerinominaf.modal('hide');
      }, function()
      {
         ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         $modalerinominaf.modal('hide');
      });
   }

   $scope.open_remove= function(file)
   {
      $scope.doc = file;
      $timeout(function()
      {
         $modaleeliminaf.modal('show');
      }, 500);
   }

   $scope.remove= function()
   {
      var file = $scope.doc;
      if ($rootScope.user.ruolo != 3 || !file) return;

      DOCUMENTO.delete(file._id).then(function()
      {
         ngNotify.set(CONFIG.messages.documento_eliminato, 'success');
         file.deleted_flg= 'Y';
         $modaleeliminaf.modal('hide');
      }, function()
      {
         ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         $modaleeliminaf.modal('hide');
      });
   }

   $scope.remove_s3= function(f, reload)
   {
      if ($rootScope.user.ruolo != 3 || !f) return;

      $http
      ({
         url: '/rest/docs/delete/'+f._id+'/'+CANALE.selectedChannel().canale_id,
         method: 'GET'
      }).then(function()
      {
         ngNotify.set(CONFIG.messages.documento_eliminato, 'success');
         if (reload)
         {
            $scope.recuperaDocs();
            $modaleeliminaf.modal('hide');
         }
      }, function(err)
      {
         ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
      });
   }

   $scope.open_restore= function(file)
   {
      $scope.doc = file;
      $timeout(function()
      {
         $modalerestore.modal('show');
      }, 500);
   }

   $scope.restore= function()
   {
      var file = $scope.doc;
      if ($rootScope.user.ruolo != 3 || !file) return;

      DOCUMENTO.delete(-file._id).then(function()
      {
         ngNotify.set(CONFIG.messages.documento_ripristinato, 'success');
         delete file.deleted_flg;
         $modalerestore.modal('hide');
      }, function()
      {
         ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         $modalerestore.modal('hide');
      });
   }

   $scope.open_scadenza= function(file)
   {
      $scope.doc = file;
      file.oldscad = file.scadenza
      $timeout(function()
      {
         $modalescadenza.modal('show');
      }, 500);
   }

   $scope.saveScadenza= function()
   {
      if ($rootScope.user.ruolo != 3 || !$scope.doc) return;
      delete $scope.doc.oldscad

      var scadenza = moment(new Date($scope.doc.scadenza)).format('YYYY-MM-DD');
      DOCUMENTO.scadenza($scope.doc._id, scadenza).then(function()
      {
         ngNotify.set('Data scadenza modificata', 'success');
         $modalescadenza.modal('hide');
      }, function()
      {
         ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         $modalescadenza.modal('hide');
      });
   }

   $scope.dismettiScadenza= function()
   {
      $scope.doc.scadenza = $scope.doc.oldscad
      delete $scope.doc.oldscad
   }

   $scope.cercaTag = function (chiave) 
   {
      return DOCUMENTO.find_tag(chiave);
   }

   $scope.openMeta = function(file)
   {
      file.olddesc = file.descrizione
      file.oldtags = file.tags
      $scope.doc = file;
      $timeout(function()
      {
         $modalemeta.modal('show');
      }, 500);
   }

   $scope.saveMeta= function()
   {
      if ($rootScope.user.ruolo < 2) return;

      DOCUMENTO.save_meta(_.pick($scope.doc, ['_id', 'descrizione', 'tags'])).then(function()
      {
         ngNotify.set(CONFIG.messages.update_ok, 'success');
         $modalemeta.modal('hide');
      }, function()
      {
         ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         $modalemeta.modal('hide');
      });
   }

   $scope.dismettiMeta= function()
   {
      $scope.doc.descrizione = $scope.doc.olddesc
      $scope.doc.tags = $scope.doc.oldtags
      delete $scope.doc.olddesc
      delete $scope.doc.oldtags
   }

   var getSelectedDocs= function()
   {
      var ids= [];

      $scope.files.forEach(function(f)
      {
         if (f.selected) ids.push(f._id);
      });

      return ids;
   }

   $scope.remove_selected= function()
   {
      if ($rootScope.user.ruolo != 3 || !$scope.filesSelected()) return;

      if ($scope.ctrl.showdeleted)
      {
         $scope.files.forEach(function(f)
         {
            if (f.selected && f.deleted_flg==='Y')
               $scope.remove_s3(f, 0);
         });

         $timeout(function()
         {
            $scope.recuperaDocs();
         }, 2000);
      }
      else
      {
         postdata= { docs: getSelectedDocs() };

         DOCUMENTO.delete_multiple(postdata)
            .then(function ()
            {
               ngNotify.set(CONFIG.messages.documento_eliminato, 'success');
               $scope.recuperaDocs();
            },
            function ()
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
      }
   }

   $scope.restore_selected= function()
   {
      if ($rootScope.user.ruolo != 3 || !$scope.filesSelected()) return;

      postdata= { docs: getSelectedDocs() };

      DOCUMENTO.restore_multiple(postdata)
      .then(function ()
      {
         ngNotify.set(CONFIG.messages.documento_ripristinato, 'success');
         $scope.recuperaDocs();
      }, function ()
      {
         ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
      });
   }

   $scope.carica_cartella= function()
   {
      $scope.cartella = this.cartella;
   }

   $scope.open_associa= function()
   {
      $scope.cartella = this.cartella;
      $scope.viewers = this.cartella.utenti;
      this.cartella.oldutenti = $scope.viewers;

      $scope.users.forEach(function(u)
      {
         u.enabled = $scope.checkUser(u.utente_id);
      });

      $timeout(function()
      {
         $modaleassocia.modal('show');
      }, 500);
   }

   $scope.checkUser= function(usr)
   {
      return $scope.cartella?.utenti?.indexOf(usr) > -1;
   }

   $scope.filtraUtenti= function()
   {
      return function(item)
      {
         if (!$scope.search.nome || $scope.search.nome == "") return true;

         return (item.nome.toLowerCase().indexOf($scope.search.nome.toLowerCase()) > -1 ||
                 item.mail.toLowerCase().indexOf($scope.search.nome.toLowerCase()) > -1);
      };
   };

   $scope.dismettiAssocia= function()
   {
      if ($scope.cartella)
      {
         $scope.cartella.utenti= $scope.cartella.oldutenti;
         delete $scope.cartella.oldutenti;
      }
   }

   $scope.associa= function()
   {
      var utenti= [];

      if ($rootScope.user.ruolo != 3) return;

      $scope.associo = true;

      $scope.users.forEach(function(u)
      {
         if (u.enabled) utenti.push(u.utente_id);
      });

      if ($scope.cartella)
      {
         postdata= { id: $scope.cartella._id, utenti: utenti };

         CARTELLA.associa(postdata)
            .then(function()
            {
               $modaleassocia.modal('hide');
               $scope.associo = false;
               $scope.cartella.utenti = utenti;
            }, function()
            {
               $modaleassocia.modal('hide');
               $scope.associo = false;
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
      }
      else
         ngNotify.set(CONFIG.messages.documento_non_definito, 'error');
   }

   $scope.remove_folder= function()
   {
      var cartella = this.cartella;

      if ($rootScope.user.ruolo != 3 || !cartella) return;

      CARTELLA.delete(cartella._id)
         .then(function()
         {
            init();
            $modaleelimina.modal('hide');
            ngNotify.set(CONFIG.messages.cartella_eliminata, 'success');
         }, function(err)
         {
            if (err.data.message.indexOf('ORA-02292') > -1)
               ngNotify.set(CONFIG.messages.cartella_non_vuota, 'error');
            else
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');

            $modaleelimina.modal('hide');
         });
   }

   $scope.rinomina_cartella= function()
   {
      var cartella= this.cartella;

      if (cartella.newname && $rootScope.user.ruolo==3)
      {
         cartella.newname= cartella.newname.replace(/\/|"|\'|,/g, '-');

         CARTELLA.rename(cartella._id, cartella.newname)
             .then(function(folders)
             {
                init();
                $modalerinomina.modal('hide');
                ngNotify.set(CONFIG.messages.cartella_rinominata, 'success');
             },function(err)
             {
                if (err.data.message.indexOf('ORA-20007') > -1)
                   ngNotify.set(CONFIG.messages.cartella_nome_esistente, 'error');
                else
                   ngNotify.set(CONFIG.messages.problema_tecnico, 'error');

                $modalerinomina.modal('hide');
             });
      }

      delete cartella.edit;
   }

   // Modale con tree delle cartelle e relativa funzione onSelect
   $scope.sposta_folder= function()
   {
      $scope.da_spostare= this.cartella._id;
      $scope.da_spostare_nome= this.cartella.nome;
      $scope.da_spostare_padre= this.cartella.padre;

      $timeout(function()
      {
         $modalesposta.modal('show');
      }, 500);
   }

   var treeSelect= function(node, breadcrumbs)
   {
      if ($scope.da_spostare_padre === node._id) return; // La sposto dove è già?
      if ($scope.da_spostare === node._id) return; // La sposto dentro a sè stessa?
      if ($scope.da_spostare === node.padre)
         ngNotify.set(CONFIG.messages.cartella_non_spostata_figlie, 'error');
      else
         CARTELLA.sposta($scope.da_spostare, node._id )
            .then(function()
            {
               $modalesposta.modal('hide');
               delete $scope.da_spostare_padre;
               delete $scope.da_spostare;
               ngNotify.set(CONFIG.messages.cartella_spostata, 'success');
               init();
            },function(err)
            {
               err = err.data;
               if (err.message.indexOf('ORA-20006') > -1)
                  ngNotify.set(CONFIG.messages.cartella_non_spostata_discendenti, 'error');
               else if (err.message.indexOf('ORA-20007') > -1)
                  ngNotify.set(CONFIG.messages.cartella_nome_esistente, 'error');
               else
                  ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
   }
   $scope.treeOptions = { onNodeSelect: treeSelect };

   $scope.open_assegna= function(file)
   {
      $scope.doc = file;
      $scope.viewers = file.viewers;
      file.oldviewers = $scope.viewers;

      $scope.users.forEach(function(u)
      {
         u.enabled = $scope.canView(u.utente_id);
         u.enabled_mail= false;
      });

      $timeout(function()
      {
         $modaleassegna.modal('show');
      }, 500);
   }

   $scope.open_assegna_selected= function()
   {
      $scope.viewers = '';
      delete $scope.doc;

      $scope.users.forEach(function(u) { u.enabled = false; u.enabled_mail= false; });

      $timeout(function()
      {
         $modaleassegna.modal('show');
      }, 500);
   }

   $scope.dismetti= function()
   {
      if ($scope.doc)
      {
         $scope.doc.viewers= $scope.doc.oldviewers;
         delete $scope.doc.oldviewers;
      }
   }

   $scope.assegna= function()
   {
      var utenti= [], utenti_mail= [];

      if ($rootScope.user.ruolo != 3) return;

      $scope.assegno = true;

      $scope.users.forEach(function(u)
      {
         if (u.enabled) utenti.push(u.utente_id);
         if (u.enabled && u.enabled_mail) utenti_mail.push(u.utente_id);
      });

      if ($scope.doc)
      {
         postdata= { doc: $scope.doc._id, utenti: utenti };

         DOCUMENTO.assegna(postdata)
            .then(function()
            {
               var nuovi_utenti = _.difference(utenti_mail, $scope.doc.viewers);

               if (nuovi_utenti.length)
                  UTENTE.mails_by_ids(JSON.stringify(nuovi_utenti))
                     .then(function (mails)
                     {
                        mails = mails.data;

                        $scope.email = {};
                        $scope.email.a = mails;

                        $http
                        ({
                           url: '/rest/email',
                           data: JSON.stringify({
                              email: $scope.email,
                              contenuto: {folder: CARTELLA.selectedFolder(), channel: CANALE.selectedChannel()},
                              tipo: 'caricamento',
                              documenti: {documenti:[$scope.doc]}
                           }),
                           method: 'POST'
                        }).then(function (result)
                        {
                           $modaleassegna.modal('hide');
                           $scope.assegno = false;
                           init2();
                           ngNotify.set(CONFIG.messages.documento_assegnato, 'success');
                        });
                     });
               else
               {
                  $modaleassegna.modal('hide');
                  $scope.assegno = false;
                  init2();
                  ngNotify.set(CONFIG.messages.documento_assegnato, 'success');
               }
            }, function()
            {
               $modaleassegna.modal('hide');
               $scope.assegno = false;
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
      }
      else if ($scope.filesSelected())
      {
         $scope.sendmail = 1;
         postdata= { docs: getSelectedDocs(), utenti: utenti };

         DOCUMENTO.assegna_multiple(postdata)
            .then(function()
            {
               // Ho selezionato più documenti: mando la notifica a TUTTI indistintamente
               // come concordato con Maurizio in data 11-NOV-2016

               if ($scope.sendmail && utenti.length)
                  UTENTE.mails_by_ids(JSON.stringify(utenti))
                     .then(function (mails)
                     {
                        mails = mails.data;

                        $scope.email = {};
                        $scope.email.a = mails;

                        $http
                        ({
                           url: '/rest/email',
                           data: JSON.stringify({
                              email: $scope.email,
                              contenuto: {folder: CARTELLA.selectedFolder(), channel: CANALE.selectedChannel()},
                              tipo: 'caricamento',
                              documenti: {documenti: _.filter($scope.files, function(f) { return f.selected; })}
                           }),
                           method: 'POST'
                        }).then(function (result)
                        {
                           $modaleassegna.modal('hide');
                           $scope.assegno = false;
                           init2();
                           ngNotify.set(CONFIG.messages.documento_assegnato, 'success');
                        });
                     });
               else
               {
                  $modaleassegna.modal('hide');
                  $scope.assegno = false;
                  init2();
                  ngNotify.set(CONFIG.messages.documento_assegnato, 'success');
               }
            }, function()
            {
               $modaleassegna.modal('hide');
               $scope.assegno = false;
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
      }
      else
         ngNotify.set(CONFIG.messages.documento_non_definito, 'error');
   }

   // Click su checkbox gruppo
   $scope.grpClick= function()
   {
      var grp= this.group;

      if (grp.utenti && grp.utenti.length)
         _.each(grp.utenti, function(u)
         {
            var ut= _.findWhere($scope.users, { _id: u });

            if (ut) ut.enabled= !grp.enabled;
         });
   }

   $scope.canView= function(usr)
   {
      return ($scope.viewers && $scope.viewers.indexOf(usr) > -1);
   }

   $scope.utenti_selezionati= function ()
   {
      return _.filter($scope.users, function(u){ return u.enabled; }).length;
   }

   $scope.filesSelected= function()
   {
      return _.filter($scope.files, function(f) { return f.selected; }).length;
   }

   $scope.copyUrl= function(id, cid)
   {
      var dummy = document.createElement("input");
      document.body.appendChild(dummy);
      dummy.value = window.location.protocol + '//' + window.location.host + '/rest/docs/download/' + id + '/' + cid + '/Y/Y';
      dummy.select();
      document.execCommand("copy");
      document.body.removeChild(dummy);
   }

   $scope.copyUrlFolder= function()
   {
      var dummy = document.createElement("input");
      document.body.appendChild(dummy);
      dummy.value = window.location.protocol + '//' + window.location.host + '/#!/canale-documenti?cartella='+this.cartella._id;
      dummy.select();
      document.execCommand("copy");
      document.body.removeChild(dummy);
   }

   // Se arrivo qui dal link di una mail devo selezionare la cartella
   if ($location.search().cartella)
      CARTELLA.get_ruolo_canale_by_cartella($location.search().cartella)
         .then(function (role)
         {
            role = role.data;
            CANALE.setSelectedChannel(_.extend(role.canale, { canale_id : role.canale._id }));
            CARTELLA.setSelectedFolder(role.cartella);
            $rootScope.user.ruolo = role.ruolo;
            $rootScope.uploader = (role.ruolo==3);
            init();
         }, function ()
         {
            ngNotify.set(CONFIG.messages.cartella_proibita, 'error');
            if ($rootScope.user._id)
               init();
            else
               $window.location.href = '#!/login?path=' + window.location.href
         });
   else
   {
      if (CANALE.selectedChannel())
         CANALE.get_ruolo(CANALE.selectedChannel().canale_id)
            .then(function (role)
            {
               $rootScope.user.ruolo = role.data.ruolo;
               $rootScope.uploader = (role.data.ruolo == 3);
               UTENTE.setUser(_.extend(UTENTE.user(), { ruolo : role.data.ruolo }));
               init();
            }, function (err)
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
      else
         init();
   }

   // Every time a modal is shown, if it has an autofocus element, focus on it.
   $('.modal').on('shown.bs.modal', function()
   {
      $(this).find('[autofocus]').focus();
   });
}]);
