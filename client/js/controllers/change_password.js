angular.module('intranetpluri')
       .controller('change_password',
       ['$rootScope', '$scope', '$location', 'ngNotify', 'CONFIG','UTENTE',
function($rootScope, $scope, $location, ngNotify, CONFIG, UTENTE)
{

   $scope.pass_confirm= function()
   {
      if (!$scope.password && !$scope.password_confirm) return null;

      if ($scope.password && $scope.password_confirm &&
          $scope.password_confirm === $scope.password && $scope.password.length > 5)
         return "fa fa-check text-success";

      return "fa fa-times text-danger";
   }

   $scope.scriviPassword= function()
   {
      if (!$scope.password || $scope.password.length < 6)
         ngNotify.set(CONFIG.messages.password_corta, 'error');
      else if ($scope.password_confirm && $scope.password_confirm === $scope.password)
         UTENTE.change_password({ old_password: $scope.old_password, password: $scope.password })
            .then(function()
            {
               ngNotify.set(CONFIG.messages.password_creata, 'success');
               $location.path('/home');
            },function(err)
            {
               if (err.data.message.indexOf('ORA-20002') > -1)
                  ngNotify.set(CONFIG.messages.password_vecchia_sbagliata, 'error');
               else
                  ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
   }
}]);
