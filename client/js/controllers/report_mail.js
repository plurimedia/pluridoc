angular.module('intranetpluri').controller('report_mail',
['$rootScope', '$scope', '$http', 'ngNotify', 'CONFIG','REPORT','CANALE',
function ($rootScope, $scope, $http, ngNotify, CONFIG, REPORT, CANALE)
{
   $scope.loading = true;

   $scope.filtraCanali= function()
   {
      return function(item)
      {
         return ((!$scope.search.canale || $scope.search.canale == "" || item.canale == $scope.search.canale) &&
                 (!$scope.search.tipo || $scope.search.tipo == "" || item.tipo == $scope.search.tipo))
      };
   };

   $scope.resetSearch = function()
   {
      $scope.search = {};
   }

   getStato = function(mail)
   {
      $http
      ({
         url: '/rest/email/'+mail._id,
         method: 'GET'
      })
         .then(function(stato)
         {
            mail.stato = stato.data;
         });
   }

   $scope.getInvio = function(invio)
   {
      $scope.invio = invio;
      if (!invio.mails)
         REPORT.all_mail({ invio_id: invio._id })
            .then(function(mails)
            {
               mails = mails.data;
               _.each(mails, function(mail)
               {
                  getStato(mail);
               });
               invio.mails = mails;
            },function()
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
   }

   function read_invio()
   {
      REPORT.read_invio()
         .then(function(invii)
         {
            $scope.invii = invii.data;
            $scope.loading = false;
            $scope.search = {};

         },function()
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         });

      $scope.canali = [];
      if ($rootScope.user && $rootScope.user.admin)
          CANALE.all()
             .then(function(canali)
             {
                _.each(canali.data, function(el)
                {
                   $scope.canali.push(el.nome);
                });
             },function()
             {
                ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
             });
   }

   read_invio();
}]);
