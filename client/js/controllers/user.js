angular.module('intranetpluri').controller('user',
['$rootScope', '$scope', '$routeParams', '$location', 'ngNotify', 'UTENTE', 'CONFIG',
function($rootScope, $scope, $routeParams, $location, ngNotify, UTENTE, CONFIG)
{
   var $modaleelimina = $('#eliminautente');
   $scope.saving = 0;
   $scope.utente_id = $routeParams.id;

   var load= function()
   {
       UTENTE.get($routeParams.id)
           .then(function(user)
           {
              $scope.utente = user.data;
              if ($scope.utente.scadenza)
                  $scope.utente.scadenza = new Date($scope.utente.scadenza);
              if ($scope.utente.decorrenza)
                  $scope.utente.decorrenza = new Date($scope.utente.decorrenza);
           }, function()
           {
              ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
           });

       UTENTE.login_log($routeParams.id)
           .then(function(logs)
           {
               moment.locale();
               $scope.mesi = [];

               //trovo i mesi in cui ci sono attivita
               _.each(_.keys(_.groupBy(logs.data, function(log){
                                                           return moment(log.cdate.substr(3,7), 'MM/YYYY').format('MM/YYYY');
                                                       })), function (el) {
                                                                       $scope.mesi.push(moment(el, 'MM/YYYY').format('MMM YYYY'));
                                                                   });

               //raggruppo mer mese e anno
               $scope.logs_mesi = _.groupBy(logs.data, function(log){
                                                           return moment(log.cdate.substr(3,7), 'MM/YYYY').format('MMM YYYY');
                                                       });
               //seleziono l'ultimo mese nella select
               $scope.mese_selected = $scope.mesi[0];
               $scope.change_mese($scope.mese_selected);

               //prendo l'ultimo access
               $scope.last_access = _.max(logs.data, function(log){ return parseInt(moment(log.cdate, 'DD/MM/YYYY HH:ss').format('YYYYMMDDHHss')); }).cdate;
           },function()
           {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
           });
   };

   $scope.change_mese = function(mese) {
       $scope.logs = $scope.logs_mesi[mese];
   }

   $scope.scriviUtente= function()
   {
       $scope.saving = 1;
       var ut = $scope.utente;
       if (ut.scadenza)
         ut.scadenza = moment(new Date(ut.scadenza)).format('YYYY-MM-DD');
       if (ut.decorrenza)
         ut.decorrenza = moment(new Date(ut.decorrenza)).format('YYYY-MM-DD');

       UTENTE.save(ut)
           .then(function()
           {
               ngNotify.set(CONFIG.messages.utente_modificato, 'success');
               $scope.saving = 0;
           },function(err)
           {
               err = err.data;
               $scope.saving = 0;
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
           });
   }

   $scope.elimina_utente= function()
   {
       UTENTE.delete($routeParams.id)
           .then(function () {
               ngNotify.set(CONFIG.messages.utente_eliminato, 'success');
               $modaleelimina.modal('hide');
               $location.path('/users');
           },function (err) {
               err = err.data;
               if (err.message.indexOf('ORA-02292') > -1)
                   ngNotify.set(CONFIG.messages.utente_documenti, 'error');
               else
                   ngNotify.set(CONFIG.messages.problema_tecnico, 'error');

               $modaleelimina.modal('hide');
           });
   }

   $scope.undelete= function()
   {
       UTENTE.delete(-$routeParams.id)
           .then(function () {
               ngNotify.set(CONFIG.messages.utente_ripristinato, 'success');
               $location.path('/users');
           },function (err) {
               err = err.data;
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
           });
   }

   load();
}]);
