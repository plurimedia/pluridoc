angular.module('intranetpluri').controller('docs-report1',
['$rootScope', '$scope', '$http', 'ngNotify', 'CONFIG', 'REPORT', 'CANALE',
function ($rootScope, $scope, $http, ngNotify, CONFIG, REPORT, CANALE)
{
   $scope.loading = true;

   $scope.resetSearch = function()
   {
      $scope.search = {};
   }

   getStato = function(mail)
   {
      $http
      ({
         url: '/rest/email/'+mail._id,
         method: 'GET'
      })
      .then(function(stato)
      {
         mail.stato = stato.data;
      });
   }

   $scope.getInvio = function(invio)
   {
      $scope.invio = invio;
      if (!invio.mails)
         REPORT.all_mail({ invio_id: invio._id })
            .then(function(mails)
            {
               mails = mails.data;
               _.each(mails, function(mail)
               {
                  getStato(mail);
               });
               invio.mails = mails;
            },function()
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
   }

   function read_invio()
   {
      REPORT.read_invio({ cid: CANALE.selectedChannel().canale_id })
         .then(function(invii)
         {
            $scope.invii = invii.data;
            $scope.loading = false;
            $scope.search = {};

         },function()
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         });
   }

   read_invio();
}]);
