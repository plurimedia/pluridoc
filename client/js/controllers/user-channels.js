angular.module('intranetpluri')
    .controller('user-channels',
    ['$rootScope', '$scope', '$timeout', 'ngNotify', '$routeParams', 'UTENTE', 'CONFIG', 'CANALE',
function ($rootScope, $scope, $timeout, ngNotify, $routeParams, UTENTE, CONFIG, CANALE)
{
   var $modale= $('#ruoloutente'), allChannels = [], soloatt = false;

   $scope.utente_id = $routeParams.id;

   $scope.filtra = function(reload)
   {
      if (!reload) soloatt = !soloatt;

      if (soloatt)
         $scope.canali = allChannels.filter(function(c) { return !c.pubblico && c.ruolo != 0; });
      else
         $scope.canali = allChannels;
   }

   var load = function()
   {
      CANALE.canali_utente({ utente_id: $routeParams.id, all: 1 })
         .then(function(ch)
         {
            allChannels = ch.data;
            allChannels.forEach(function(el) { el.ruolo = el.ruolo+""; });
            $scope.filtra(1);
         }, function()
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         });
   };

   $scope.associa_docs = function(ch) {
      CANALE.associa_docs({ utente_id: $routeParams.id, canale_id: ch.canale_id })
         .then((response) =>
            {
               ngNotify.set('Associazione avvenuta'/*CONFIG.messages.problema_tecnico*/, 'info');
               $scope.changeAccess(ch.canale_id, 1)
            }, function()
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
   }

   var $modalesceltautente= $('#sceltautente');
   $scope.modal_associa_docs_utente = function(ch) {
      UTENTE.all()
         .then(function(utenti) {
            $scope.users = _.filter(utenti.data, (el) => 
               el._id != $routeParams.id
            )
            $modalesceltautente.modal('show');
         }, function()
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         })
   }
   var $confermasceltautente = $('#confermasceltautente');
   $scope.modal_conferma_associa_docs_utente = function(u) {
      $scope.utente_a_cui_associare = u
      $modalesceltautente.modal('hide');
      $confermasceltautente.modal('show');
   }
   $scope.associa_docs_utente = function() {
      let u = $scope.utente_a_cui_associare
      console.log('associo da utente', u._id, ' a utente ', $routeParams.id)
      $confermasceltautente.modal('hide');
      $modalesceltautente.modal('hide');
      UTENTE.associa_docs_utente({ utente_id_from: u._id, utente_id_to: $routeParams.id })
         .then(function(ch)
            {
               ngNotify.set('Associazione avvenuta'/*CONFIG.messages.problema_tecnico*/, 'info');
            }, function()
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
   }

   $scope.change_ruolo = function(ch)
   {
      if (ch)
         $scope.selectedChannel = _.clone(ch);
      else
         $scope.selectedChannel = { canale_id: 0 };
   }

   $scope.changeAccess = function(id, ruolo)
   {
      var ch = [];

      if (id == 0 && !$scope.channelsSelected()) return;

      if (id == 0)
         $scope.canali.forEach(function(c)
         {
            if (c.selected) ch.push(c.canale_id);
         });
      else
         ch = [id]

      ch.forEach(function(c)
      {
         CANALE.associa(c, $routeParams.id, ruolo)
            .then(function(u)
            {
               ngNotify.set(CONFIG.messages.utente_associato_canale, 'success');
               $modale.modal('hide');
            },function()
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
               $modale.modal('hide');
            });
      });

      $timeout(function() { load(); }, 1000);
   }

   $scope.channelsSelected= function()
   {
      return _.filter($scope.canali, function(f) { return f.selected; }).length;
   }

   UTENTE.get($routeParams.id)
      .then(function(user)
      {
         $scope.utente = user.data;
      }, function()
      {
         ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
      });

   load();
}]);
