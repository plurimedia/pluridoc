angular.module('intranetpluri')
       .controller('docs-scaricati',
       ['$rootScope', '$scope', '$routeParams', 'ngNotify', 'CANALE',
function($rootScope, $scope, $routeParams, ngNotify, CANALE)
{
   $scope.ricerca = {};
   $scope.ricerca.da = moment().startOf('month').format('YYYY-MM-DD');
   $scope.ricerca.a = moment().endOf('month').format('YYYY-MM-DD');
   $scope.items= [];
   $scope.hasSearched= false;
   $rootScope.selectedChannel = CANALE.selectedChannel();

   /*$scope.popup1= { opened: false };
   $scope.open1= function($event)
   {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.popup1.opened = true;
   };

   $scope.popup2= { opened: false };
   $scope.open2= function($event)
   {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.popup2.opened = true;
   };*/

   $scope.cerca= function()
   {
      var a = "", da = "";

      if ($scope.ricerca.da) da = $scope.ricerca.da;
      if ($scope.ricerca.a) a = $scope.ricerca.a;

       CANALE.report_download({ cid: CANALE.selectedChannel().canale_id, da: da, a: a })
          .then(function(results)
          {
             results = results.data;
             $scope.hasSearched= true;
             $scope.items = results;

          },function()
          {
              ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
          });
   };
}]);
