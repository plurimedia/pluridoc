angular.module('intranetpluri')
       .controller('users',
       ['$rootScope', '$scope', '$http', 'ngNotify', 'UTENTE','CONFIG',
function($rootScope, $scope, $http, ngNotify, UTENTE, CONFIG)
{
   var $modaleuser = $('#creautente');
   $scope.saving = 0;
   $scope.loading = true;
   $scope.search = {};

   var load= function()
   {
      UTENTE.all()
         .then(function(users)
         {
            users = users.data;
            UTENTE.all_temp() //per leggere gli utenti in attesa di approvazione
               .then(function(users_temp)
               {
                  users_temp = users_temp.data;
                  //$scope.utenti_temp= users_temp;
                  $scope.utenti= _.union(users_temp, users);
                  $scope.loading = false;
               },function()
               {
                  ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
                  $scope.loading = false;
               });

         },function()
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            $scope.loading = false;
         });
   };

   $scope.filtraTemp= function()
   {
      return function(item)
      {
         if ($scope.solotemp) return item.fl_temp;
         if ($scope.showblocked) return item.blocked;

         return !item.blocked;
      };
   };

   $scope.filtraNomeMailAzienda= function()
   {
      return function(item)
      {
         if (!$scope.search.nome || $scope.search.nome == "") return true;

         if (item.nome.toLowerCase().indexOf($scope.search.nome.toLowerCase()) > -1 || item.mail.toLowerCase().indexOf($scope.search.nome.toLowerCase()) > -1 || item.azienda.toLowerCase().indexOf($scope.search.nome.toLowerCase()) > -1) return true;

         return false;
      };
   };

   $scope.scriviUtente= function()
   {
     $scope.saving = 1;
     $scope.inviaMail= false;
     var ut = $scope.utente;
     if ($scope.utente.inviaMail) ut.fl_temp = 1;
     if (ut.scadenza) ut.scadenza = moment(new Date(ut.scadenza)).format('YYYY-MM-DD');
     if (ut.decorrenza) ut.decorrenza = moment(new Date(ut.decorrenza)).format('YYYY-MM-DD');

     UTENTE.save(ut)
        .then(function(gr)
        {
           ngNotify.set(CONFIG.messages.utente_creato, 'success');
           $modaleuser.modal('hide');

           load();

           $scope.saving = 0;

           if ($scope.utente.inviaMail)
           {
              $scope.email = {};
              $scope.email.a = [];
              $scope.email.a.push(ut.mail);

              $http
              ({
                 url: '/rest/email',
                 data: JSON.stringify({email:$scope.email, utente:gr.data, tipo:'account'}),
                 method: 'POST'
              })
              .then(function(result)
              {
                 ngNotify.set(CONFIG.messages.notifica_inviata, 'success');
              });
           }
        },function(err)
        {
           err = err.data;
           $scope.saving = 0;
           if (err.message.indexOf('ORA-00001') > -1)
              ngNotify.set(CONFIG.messages.utente_esistente, 'error');
           else
              ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
        });
  }

  $scope.crea_utente= function()
  {
     $scope.utente = { inviaMail: true };
  }

  $scope.mailAtt= function(email)
  {
     UTENTE.riattiva({ mail: email })
        .then(function(gr)
        {
           load();
           $scope.email = {};
           $scope.email.a = [];
           $scope.email.a.push(email);

           $http
           ({
              url: '/rest/email',
              data: JSON.stringify({ email: $scope.email, utente: gr.data, tipo: 'account' }),
              method: 'POST'
           })
           .then(function(result)
           {
              ngNotify.set(CONFIG.messages.notifica_inviata, 'success');
           });
        },function(err)
        {
           err = err.data;
           ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
        });
  }

  load();
}]);
