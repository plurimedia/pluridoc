angular.module('intranetpluri')
    .controller('transfer',
    ['$rootScope', '$scope', '$http', '$location', 'ngNotify', 'Upload','CONFIG',
        function($rootScope, $scope, $http, $location, ngNotify, Upload, CONFIG)
        {
            function isEmail(email) {
                return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(email);
            }

//daniele.sergi@plurimedia.it, alessandro.it, mail@plurimedia.it

            $scope.destinatari = [];
            $scope.canSend = false;
            $scope.inviando = false;

            $scope.checkDestinatari = function() {
                $scope.destinatari = [];
                if ($scope.destinatari_txt) {
                    var allRight = true;
                    var dest = $scope.destinatari_txt.split(',');
                    _.each(dest, function (el) {
                        el = el.trim();
                        if (!isEmail(el)) {
                            ngNotify.set(CONFIG.messages.destinatario_errato + el, 'error');
                            allRight = false;
                        } else {
                            $scope.destinatari.push(el);
                        }
                    })
                    return allRight;
                } else {
                    ngNotify.set(CONFIG.messages.destinatari_errati, 'error');
                    return false;
                }
            }

            $scope.uploadFiles = function (files) {
                $scope.inviando = true;
                $scope.docs = files;

                var file = files[0];
                if (file) {
                    $scope.f = file;

                    var filetype = file.type !== "" ? file.type : 'application/octet-stream';

                    $scope.percorso = 'transfers/' + $rootScope.user._id + '/' + file.name.cleanFileName();

                    $http
                    ({
                        url: '/secure/transfer',
                        data: {percorso: $scope.percorso, tipo: filetype, size: file.size},
                        method: 'POST'
                    })
                    .catch(function(err)
                    {
                        console.error(err)
                        $scope.docs[key].uploaderr= true;
                        callback(err);
                    })
                    .then(function (url) {
                        // ho un signed url per l'upload su amazon
                        file.upload = Upload.http({
                            url: url.data,
                            data: file,
                            method: 'PUT',
                            headers: {'Content-Type': file.type}
                        });

                        file.upload.then(function (response) {
                            if (response.status === 200) {
                                $scope.inviando = false;
                                $scope.canSend = true;
                            }
                        });

                        file.upload.progress(function (evt) {
                            $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    });
                }
            }

            $scope.rimuoviDoc = function(file)
            {
                _.rm($scope.docs, file);
            }

            // togli tutti i file Docs
            $scope.rimuoviDocs = function()
            {
                $scope.docs = [];
            }

            $scope.confermaTransfer = function()
            {
                if ($scope.checkDestinatari() && $scope.f && !$scope.inviando && $scope.progress === 100) {
                    $scope.email = {};
                    $scope.email.a = $scope.destinatari;

                    $http
                    ({
                        url: '/rest/email',
                        data: JSON.stringify({
                            email: $scope.email,
                            contenuto: {file: $scope.percorso},
                            testo: $scope.testo,
                            tipo: 'transfer'
                        }),
                        method: 'POST'
                    })
                        .then(function (result) {
                            ngNotify.set(CONFIG.messages.documento_condiviso, 'success');
                            $scope.reset();
                        });
                }// else {
                 //   ngNotify.set(CONFIG.messages.campi_non_compilati, 'error');
                //}
            }

            $scope.reset = function () {
                $scope.f = undefined;
                $scope.destinatari = [];
                $scope.canSend = false;
                $scope.inviando = false;
                $scope.destinatari_txt = "";
                $scope.testo = "";
                $scope.rimuoviDocs();

            }

        }]);
