angular.module('intranetpluri')
       .controller('login',
       ['$rootScope', '$scope', '$http', '$location', '$window', 'CANALE', 'UTENTE', 'ngNotify', 'CONFIG',
function($rootScope, $scope, $http, $location, $window, CANALE, UTENTE, ngNotify, CONFIG)
{
   $rootScope.login = true
   var $modalepass = $('#resetpassword')
   // var $modalelogin = $('#loginutente')
   $scope.credentials = {}

   if ($rootScope.user && $rootScope.user._id) $location.path('/home')

   let redirectPath
   try {
      redirectPath = decodeURIComponent($location.$$absUrl.split('?')[1].replace('path=', ''))
   } catch (e) {
      redirectPath = undefined
   }

   $scope.submit = function()
   {
      if ($scope.submitting || !$scope.credentials.email || !$scope.credentials.password)
       return;

      $scope.submitting= true

      $http
      ({
         url: '/rest/login',
         method: 'POST',
         data: { email: $scope.credentials.email, password: $scope.credentials.password }
      })
      .then(function(response)
      {
         
         
         $http
         ({
            url: '/zoom/get',
            method: 'GET'
         })
         .then(function(response2)
         {
            response = response.data
            $scope.submitting= false
            response.joinURL= response2.data.join_url
            $rootScope.user= response
            UTENTE.setUser(response)
            CANALE.unsetSelectedChannel
            delete $rootScope.login

            if (redirectPath)
            $window.location.href = redirectPath
            else
            $location.url($rootScope.destination || '/home')

            $rootScope.destination= undefined
         }, function(err)
         {
            response = response.data
            $scope.submitting= false
            $rootScope.user= response
            UTENTE.setUser(response)
            CANALE.unsetSelectedChannel
            delete $rootScope.login
            $rootScope.joinURL = null
            if (redirectPath)
            $window.location.href = redirectPath
            else
            $location.url($rootScope.destination || '/home')

            $rootScope.destination= undefined
         })
      }, function(err)
      {
         console.log(err)
         if (err.data.message.indexOf('ORA-20999') > -1)
            ngNotify.set(CONFIG.messages.utente_non_riconosciuto, 'error')
         else
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error')

         $scope.submitting= false
      })
   }

   $scope.password_request = function ()
   {
      $scope.submitting_pass= true

      $http
      ({
         url: '/rest/user/password_request',
         method: 'POST',
         data: { email: $scope.credentials.email2 }
      })
      .then(function(response)
      {
         $scope.submitting_pass= false
         $modalepass.modal('hide')
         ngNotify.set(CONFIG.messages.utente_reset_password, 'success')
      }, function()
      {
         $scope.submitting_pass= false
         ngNotify.set(CONFIG.messages.utente_non_riconosciuto, 'error')
      })
   }

   $scope.onloadFun = function()
   {
      $('#mail').focus()
   }

   $scope.entraConSpid = function () {
      $http
      ({
         url: '/rest/entraconspid',
         method: 'POST',
         data: { applicationId : "test2" }
      })
      .then(function(response)
      {
         console.log(response)
         document.getElementById('linkSPIDSTART').href = response.data;
			document.getElementById('linkSPIDSTART').click();
      })
   }
}])
