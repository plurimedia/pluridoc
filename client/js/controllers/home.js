angular.module('intranetpluri')
.controller('home',
['$translate', '$rootScope', '$scope', '$location', 'ngNotify','CONFIG','CANALE','CARTELLA','UTENTE',
function ($translate, $rootScope, $scope, $location, ngNotify, CONFIG, CANALE, CARTELLA, UTENTE)
{
   var $modalechannel = $('#nuovocanale');

   delete $rootScope.login;
   $scope.loading = true;
   $scope.ctrl = { hidepublic: false };

   $scope.filtraPub= function()
   {
      return function(canale)
      {
         return (!canale.pubblico || !$scope.ctrl.hidepublic);
      };
   };

   $scope.selectCanale = function ()
   {
      var ch = this.canale;

      if (ch)
         CANALE.get_ruolo(ch.canale_id)
            .then(function (role)
            {
               role = role.data;
               CANALE.setSelectedChannel(ch);
               CARTELLA.unsetSelectedFolder();
               $rootScope.user.ruolo = role.ruolo;
               $rootScope.selectedChannel = ch; // mi serve nella docs_tabs.html
               $rootScope.uploader = (role.ruolo == 3);
               UTENTE.setUser(_.extend(UTENTE.user(), { ruolo : role.ruolo }));
               $location.path('/canale-documenti');
            }, function (err)
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
   }

   var get_canali = function ()
   {
      $scope.loading = true;

      var elabora = function (canali)
      {
         canali = canali.data;
         canali.forEach(function (c) { c.cid = c._id; });
         return canali;
      }

      if ($rootScope.user.admin)
         CANALE.all()
            .then(function(canali)
               {
                  $scope.canali= elabora(canali);
                  $scope.loading = false;
               }, function()
               {
                  ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
                  $scope.loading = false;
               });
      else
         CANALE.canali_utente()
            .then(function (canali)
            {
               $scope.canali = elabora(canali);
               $scope.loading = false;
            }, function (err)
            {
               ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
               $scope.loading = false;
            });
   }

   get_canali();

   $scope.nuovoCanale = function ()
   {
      $scope.nomeEsistente = false;
      $scope.channel = { pubblico: '0' };
   }

   $scope.scriviCanale = function ()
   {
      if (!$scope.channel || !$scope.channel.nome) return;

      CANALE.save($scope.channel)
         .then(function () {
            ngNotify.set(CONFIG.messages.canale_creato, 'success');
            $modalechannel.modal('hide');
            get_canali();
         },function(err)
         {
            if (err.data.message.indexOf('ORA-00001') > -1)
            {
               ngNotify.set(CONFIG.messages.canale_nome_esistente, 'error');
               $scope.nomeEsistente = true;
            }
            else
               console.log(err);
         });
   }
}]);
