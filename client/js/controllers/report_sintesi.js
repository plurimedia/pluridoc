angular.module('intranetpluri').controller('report_sintesi',
['$rootScope', '$scope', '$translate', 'ngNotify', 'REPORT','CONFIG',
function ($rootScope, $scope, $translate, ngNotify, REPORT, CONFIG)
{
   formatSizeUnits = function (bytes)
   {
      if (bytes >= 1073741824)
         bytes = (bytes / 1073741824).toFixed(2) + ' GB';
      else if (bytes >= 1048576)
         bytes = (bytes / 1048576).toFixed(2) + ' MB';
      else if (bytes >= 1024)
         bytes = (bytes / 1024).toFixed(2) + ' KB';
      else if (bytes > 1)
         bytes = bytes + ' bytes';
      else if (bytes == 1)
         bytes = bytes + ' byte';
      else
         bytes = '0 byte';

      return bytes;
   }

   $scope.hasSearched = false;
   $scope.anno = new Date().getFullYear();
   $scope.ultimo = {};

   var lbls = {}, mesi = [];
   var dati_canali = [];
   var dati_utenti = [];
   var dati_documenti = [];
   var ctx1, ctx2, ctx3, myChartCanali, myChartUtenti, myChartDocumenti;

   setTimeout(function(){ 
      $translate(['m1', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'm9', 'm10', 'm11', 'm12', 'utenti', 'canali', 'stat_spazio'])
         .then(function(translations)
         {
            lbls = translations;
            init_vars();
            for (var month = 1; month < 13; month++)
               mesi.push(lbls['m'+month]);
         });
   }, 150);

   var init_vars = function()
   {
      /*report di sintesi: canali */
      ctx1 = "StatsCanali";
      myChartCanali = new Chart(ctx1, {
         type: 'bar',
         data: {
            labels: mesi,
            datasets: [{
               label: lbls.canali,
               data: dati_canali,
               backgroundColor: 'rgba(237, 85, 101, 1)',
               borderColor: 'rgba(237,85,101,1)',
               borderWidth: 1
            }]
         },
         options: {
            scales: {
               yAxes: [{
                  ticks: {
                     beginAtZero:true
                  }
               }]
            }
         }
      });

      /* report di sintesi: utenti */
      ctx2 = "StatsUtenti";
      myChartUtenti = new Chart(ctx2, {
         type: 'bar',
         data: {
            labels: mesi,
            datasets: [{
               label: lbls.utenti,
               data: dati_utenti,
               backgroundColor: 'rgba(248, 172, 89, 1)',
               borderColor: 'rgba(248, 172, 89, 1)',
               borderWidth: 1
            }]
         },
         options: {
            scales: {
               yAxes: [{
                  ticks: {
                     beginAtZero:true
                  }
               }]
            }
         }
      });

      /* report di sintesi: utenti */
      ctx3 = "StatsSpazio";
      myChartDocumenti = new Chart(ctx3, {
         type: 'bar',
         data: {
            labels: mesi,
            datasets: [{
               label: lbls.stat_spazio,
               data: dati_documenti,
               backgroundColor: 'rgba(21, 106, 171, 1)',
               borderColor: 'rgba(21, 106, 171, 1)',
               borderWidth: 1
            }]
         },
         options: {
            scales: {
               yAxes: [{
                  ticks: {
                     beginAtZero:true
                  }
               }]
            }
         }
      });
   };

   $scope.cerca = function ()
   {
      REPORT.get_job_status()
         .then(function(dati) { $scope.stato = dati.data; });

      REPORT.read_anno($scope.anno)
         .then(function(dati)
         {
            dati_canali = [0,0,0,0,0,0,0,0,0,0,0,0];
            dati_utenti = [0,0,0,0,0,0,0,0,0,0,0,0];
            dati_documenti = [0,0,0,0,0,0,0,0,0,0,0,0];

            if (dati.data.length)
            {
               dati = dati.data;

               $scope.ultimo = dati[dati.length - 1];

               _.each(dati, function(el)
               {
                  dati_canali[el.mese] = el.canali;
                  dati_utenti[el.mese] = el.utenti;
                  dati_documenti[el.mese] = (el.documenti / 1048576).toFixed(2);
               });
            }

            myChartCanali.data.datasets[0].data = dati_canali;
            myChartUtenti.data.datasets[0].data = dati_utenti;
            myChartDocumenti.data.datasets[0].data = dati_documenti;

            myChartUtenti.update();
            myChartCanali.update();
            myChartDocumenti.update();
      }, function()
      {
         ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
      });
   };

   $scope.cerca();
}]);
