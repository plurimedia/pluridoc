angular.module('intranetpluri')
       .controller('docs-caricati',
       ['$rootScope', '$scope', '$routeParams', 'ngNotify', 'CANALE',
function($rootScope, $scope, $routeParams, ngNotify, CANALE)
{
   $scope.ricerca = {};
   $scope.ricerca.da = moment().startOf('month').format('YYYY-MM-DD');
   $scope.ricerca.a = moment().endOf('month').format('YYYY-MM-DD');
   $scope.items= [];
   $scope.hasSearched= false;
   $rootScope.selectedChannel = CANALE.selectedChannel();

   $scope.cerca= function()
   {
      var a, da;

      if ($scope.ricerca.da) da = moment(new Date($scope.ricerca.da)).format('YYYY-MM-DD');
      if ($scope.ricerca.a) a = moment(new Date($scope.ricerca.a)).format('YYYY-MM-DD');

      $scope.labels = [];
      $scope.data = [];

      CANALE.report_upload({ cid: CANALE.selectedChannel().canale_id, da: da, a: a })
          .then(function(results)
          {
             results = results.data;
             $scope.hasSearched= true;
             $scope.items = results;

             /*if (results.length)
             {
                var user_prec = results[0].nome_uploader;
                var user_size = 0;
                results.forEach(function(c)
                {
                   if (c.nome_uploader != user_prec)
                   {
                      $scope.labels.push(user_prec);
                      $scope.data.push(user_size.round(2));
                      user_size = 0;
                   }

                   user_size = user_size + c.bytes.bytes_to_mega();
                   user_prec = c.nome_uploader;
                });

                $scope.labels.push(user_prec);
                $scope.data.push(user_size.round(2));
             }*/
          },function()
          {
             ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
          });
   };
}]);
