angular.module('intranetpluri')
   .controller('docs-upload',
        ['$scope', '$location', '$http', '$timeout', 'ngNotify','Upload','CONFIG','CARTELLA','CANALE','DOCUMENTO',
function ($scope, $location, $http, $timeout, ngNotify, Upload, CONFIG, CARTELLA, CANALE, DOCUMENTO)
{
   $scope.users= [];
   $scope.opts = { scadenza: null };
   var selAll= false;
   var $modalemeta = $('#metafile');

   var getUsers= function()
   {
      CANALE.utenti_canale(CANALE.selectedChannel().canale_id, 1, CARTELLA.selectedFolder()._id)
         .then(function(users)
         {
            $scope.users = users.data;
            $scope.users.forEach(function(u)
            {
               if (u.enabled) u.enabled = true;
               else u.enabled = false;
            });

            CANALE.gruppi_canale(CANALE.selectedChannel().canale_id)
               .then(function(gruppi)
               {
                  gruppi = gruppi.data;
                  $scope.groups = _.where(gruppi, { visibile: 1 });
               }, function()
               {
                  ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
               });
         },function()
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         });
   }

   $scope.openMeta = function(file)
   {
      $scope.doc = file;
      $timeout(function()
      {
         $modalemeta.modal('show');
      }, 500);
   }

   $scope.cercaTag = function (chiave) 
   {
       return DOCUMENTO.find_tag(chiave);
   }

   $scope.rimuoviDoc = function(file)
   {
      _.rm($scope.docs, file);
   }

   // togli tutti i file Docs
   $scope.rimuoviDocs = function()
   {
      $scope.docs = [];
      $scope.opts.scadenza= null;
   }

   // Click su seleziona tutti
   $scope.selectAll = function(nomail)
   {
      selAll= !selAll;
      $scope.users.forEach(function(u)
      {
         u.enabled= selAll;
         u.enabled_mail= nomail ? false : selAll;
      });
   }

   // Tooltip per elenco utenti di un gruppo
   $scope.getNames= function(usrs)
   {
      var utenti= [];

      if (usrs && usrs.length)
         utenti= _.toArray(_.pluck(usrs, 'nome'));

      return utenti.toString();
   }

   // Click su checkbox gruppo
   $scope.grpClick= function(grp)
   {
      if (grp.utenti && grp.utenti.length)
         _.each(grp.utenti, function(u)
         {
            var ut= _.findWhere($scope.users, { utente_id: u.utente_id });

            if (ut) { ut.enabled= grp.enabled; ut.enabled_mail= grp.enabled; }
         });
   }
   $scope.grpClickNoMail= function(grp)
   {
      if (grp.utenti && grp.utenti.length)
         _.each(grp.utenti, function(u)
         {
            var ut= _.findWhere($scope.users, { utente_id: u.utente_id });

            if (ut) { ut.enabled= grp.enabled2; ut.enabled_mail= false; }
         });
   }

   $scope.uploadFiles = function(files)
   {
      $scope.docs = _.union($scope.docs, files);
      $scope.inviando = true;

      if ($scope.docs.length)
      {
         async.forEachOf($scope.docs, function(file, key, callback)
         {
            if (!$scope.docs[key].uploaded)
            {
               $scope.docs[key].name_orig = file.name
               $scope.docs[key].size2 = file.size
               $scope.docs[key].name2 = moment()+file.name.cleanFileName()
               $scope.docs[key].mimetype = file.type
               $scope.docs[key].folder = CARTELLA.selectedFolder()
               $scope.docs[key].tags = []

               var filetype = file.type !== "" ? file.type : 'application/octet-stream', // mi serve per il signed url
                  percorso = CANALE.selectedChannel().canale_id + '/' + $scope.docs[key].name2;

               $http
               ({
                  url: '/secure/transfer',
                  data: { percorso: percorso.cleanFileName(), tipo: filetype, size: file.size, /* file */ },
                  method: 'POST'
               }).then(function(url)
               {
                  url = url.data;
                  // ho un signed url per l'upload su amazon
                  file.upload = Upload.http({
                     url: url,
                     data: file,
                     method: 'PUT',
                     headers: { 'Content-Type': file.type }
                  });

                  file.upload.then(function(response)
                  {
                     if (response.status === 200)
                     {
                        $scope.docs[key].uploaded= true;
                        $scope.docs[key].progress= 100;
                        callback();
                     }
                     else
                     {
                        $scope.docs[key].uploaderr= true;
                        callback(response);
                     }
                  });

                  file.upload.progress(function(evt)
                  {
                     $scope.docs[key].progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                  });
               });
            }
            else
               callback() // devo chiamare comunque la callback per ogni file
         }, function (err)
         {
            if (err)
               console.log(err);
            else
               $scope.inviando = false;
         })
      }
   }

   $scope.utenti_selezionati= function ()
   {
      return _.filter($scope.users, function(u){ return u.enabled; }).length;
   }

   $scope.invia= function ()
   {
      var us = _.filter($scope.users, function(u){ return u.enabled; }) || [];
      us = _.map(us, function(el) { return _.pick(el, 'nome','mail','_id','utente_id'); });

      var usm = _.filter($scope.users, function(u){ return u.enabled_mail; }) || [];
      usm = _.map(usm, function(el) { return _.pick(el, 'nome','mail','_id','utente_id'); });

      var postdata= { cid: CANALE.selectedChannel().canale_id, utenti: us, utenti_mail: usm };
      postdata.documenti= $scope.docs;

      if ($scope.opts.scadenza)
         postdata.scadenza = moment(new Date($scope.opts.scadenza)).format('YYYY-MM-DD');

      // se dimensione > 10000000 (size 390019))
      // se estensione = pdf (type "application/pdf"))

      DOCUMENTO.create(postdata)
         .then(function(documenti)
         {
            documenti = documenti.data;
            if (postdata.utenti && postdata.utenti.length && postdata.utenti_mail && postdata.utenti_mail.length)
            {
               $scope.email = {};
               $scope.email.a = postdata.utenti_mail;

               $http
               ({
                  url: '/rest/email',
                  data: JSON.stringify({ email: $scope.email,
                     contenuto: { folder: CARTELLA.selectedFolder(), channel: CANALE.selectedChannel() },
                     tipo: 'caricamento',
                     documenti: documenti
                  }),
                  method: 'POST'
               }).then(function(result)
               {
                  ngNotify.set(CONFIG.messages.documenti_caricati, 'success');
               });
            }

            try {
               const docsToCompress = documenti.filter(el => (el.size2 > 10000000 && el.mimetype === 'application/pdf'))
               if (docsToCompress.length > 0)
                  $http
                  ({
                     url: '/rest/docs/compress',
                     data: docsToCompress,
                     method: 'POST'
                  }).then(function(result)
                  {
                     // ngNotify.set(CONFIG.messages.documenti_caricati, 'success');
                     // console.log('chiamata funzione di compressione')
                  });
            } catch(e) {
               // non voglio che si fermi
               console.error(e)
            }

            delete $scope.docs;
            getUsers();
            $location.path('/canale-documenti');
         }, function(err)
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         });
   }

   function init() {
      if (CANALE.selectedChannel())
      {
         breadCrumb();
         getUsers();
      }
      else
      {
         ngNotify.set(CONFIG.messages.selezionare_canale, 'error');
         $location.path('home');
      }
   }

   function breadCrumb()
   {
      $scope.breadcrumb = CARTELLA.breadCrumb();
   }

   init();
}]);
