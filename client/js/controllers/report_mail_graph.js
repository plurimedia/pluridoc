angular.module('intranetpluri').controller('report_mail_graph',
['$rootScope', '$scope', '$http', '$translate', 'ngNotify', 'CONFIG','REPORT','CANALE',
function ($rootScope, $scope, $http, $translate, ngNotify, CONFIG, REPORT, CANALE)
{
   var contatori = {}, lbls;
   var contatori_tipo = { tot : [] };
   var myDoughnutChart;
   var ctx4 = "totMail";

   $scope.loading = true;
   $scope.invii = 0;

   function read_invio()
   {
      REPORT.read_invio()
         .then(function(invii)
         {
            contatori_tipo["tot"] = [0,0,0,0];

            _.each(invii.data, function(el)
            {
               if (el.numero_mail)
                  $scope.invii += el.numero_mail;

               if (!el.tipo) el.tipo = el.tipo_invio;

               if (el.canale)
               {
                  if (contatori[el.canale])
                     contatori[el.canale]++;
                  else
                     contatori[el.canale] = parseInt(el.numero_mail);

                  if (!contatori_tipo[el.canale])
                     contatori_tipo[el.canale] = [0,0,0,0];

                  if (el.tipo)
                  {
                     if (el.tipo == 'caricamento')
                        contatori_tipo[el.canale][0] = parseInt(el.numero_mail);

                     if (el.tipo == 'transfer')
                        contatori_tipo[el.canale][1] = parseInt(el.numero_mail);

                     if (el.tipo == 'forget-password')
                        contatori_tipo[el.canale][2] = parseInt(el.numero_mail);

                     if (el.tipo == 'account')
                        contatori_tipo[el.canale][3] = parseInt(el.numero_mail);
                  }
               }

               if (el.tipo)
               {
                  if (el.tipo == 'caricamento')
                     contatori_tipo["tot"][0] = contatori_tipo["tot"][0] + parseInt(el.numero_mail);

                  if (el.tipo == 'transfer')
                     contatori_tipo["tot"][1] = contatori_tipo["tot"][1] + parseInt(el.numero_mail);

                  if (el.tipo == 'forget-password')
                     contatori_tipo["tot"][2] = contatori_tipo["tot"][2] + parseInt(el.numero_mail);

                  if (el.tipo == 'account')
                     contatori_tipo["tot"][3] = contatori_tipo["tot"][3] + parseInt(el.numero_mail);
               }
            });

            CANALE.all()
               .then(function(canali)
               {
                  $scope.canali = [];
                  $scope.mail_per_canali = [];
                  _.each(canali.data, function(el)
                  {
                     $scope.canali.push(el.nome);
                     $scope.mail_per_canali.push(contatori[el.nome] || 0);
                  });

                  /* report mail inviate: mail canale */
                  var ctx5 = "mailCanale";
                  var myChart = new Chart(ctx5, {
                     type: 'bar',
                     data: {
                        labels: $scope.canali,
                        datasets: [{
                           label: lbls.mailxcanale,
                           data: $scope.mail_per_canali,
                           backgroundColor: 'rgba(13, 103, 114, 1)',
                           borderColor: 'rgba(13, 103, 114, 1)',
                           borderWidth: 1
                        }]
                     },
                     options: {
                        scales: {
                           yAxes: [{
                              ticks: {
                                 beginAtZero:true
                              }
                           }]
                        }
                     }
                  });

                  myDoughnutChart = new Chart(ctx4, {
                     type: 'doughnut',
                     data: {
                        labels: [lbls.uploads, lbls.filexfer, lbls.cambiapwd, lbls.filtraxcrea],
                        datasets: [
                           {
                              data: contatori_tipo['tot'],
                              backgroundColor: [
                                 "#ED5565",
                                 "#f8ac59",
                                 "#23c6c8",
                                 "#a0cf4f"
                              ],
                              hoverBackgroundColor: [
                                 "#d54c5b",
                                 "#e19c51",
                                 "#1ca3a5",
                                 "#92bd48"
                              ]
                           }]
                     },
                     options: {
                        animation: {
                           animateScale: true
                        }
                     }
                  });

                  $scope.loading = false;
               }, function()
               {
                  ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
               });
         }, function()
         {
            ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
         });
   }

   setTimeout(function(){ 
      $translate(['uploads', 'filexfer', 'cambiapwd', 'filtraxcrea', 'mailxcanale'])
         .then(function(translations)
         {
            lbls = translations;
            read_invio();
         }, function(err) {
            console.error(err)
         });
   }, 150);
   
}]);
