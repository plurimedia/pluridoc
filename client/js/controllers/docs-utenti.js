angular.module('intranetpluri')
    .controller('docs-utenti',
    ['$rootScope','$scope','$location', 'ngNotify','CONFIG','CANALE','UTENTE',
function ($rootScope, $scope, $location, ngNotify, CONFIG, CANALE, UTENTE)
{
    $scope.search = {};

    $scope.recuperaUtenti= function()
    {
        if (!$scope.searchUtenti || $scope.searchUtenti.length < 3) return;

        UTENTE.cerca($scope.searchUtenti)
            .then(function(utenti)
            {
                utenti = utenti.data;
                $scope.utenti_associa= utenti;
                _.intersect_by_field($scope.utenti_associa, $scope.utenti, 'mail', function(el1, el2) { el1.ruolo = el2.ruolo+"" });
            },function()
            {
                ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
    }

    $scope.filtraNomeMailAzienda= function()
    {
       return function(item)
       {
          if (!$scope.search.nome || $scope.search.nome == "") return true;

          return (item.nome.toLowerCase().indexOf($scope.search.nome.toLowerCase()) > -1 || item.mail.toLowerCase().indexOf($scope.search.nome.toLowerCase()) > -1);
       };
    };

    var load= function()
    {
        if (CANALE.selectedChannel())
        {
            $rootScope.selectedChannel = CANALE.selectedChannel();
            CANALE.utenti_canale(CANALE.selectedChannel().canale_id)
                .then(function(users)
                {
                    users.data = users.data.filter(function(c) { return c.ruolo > 0 });

                    users.data.forEach(function(c) {
                        //hack per far andare nello stesso modo la changeAccess nelle due modali
                        c._id = c.utente_id;
                        //non prende bene i value delle select con un valore numerico
                        c.ruolo = ""+c.ruolo;
                    });

                    $scope.utenti= users.data;
                },function()
                {
                    ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
                });
        }
        else
        {
            ngNotify.set(CONFIG.messages.selezionare_canale, 'error');
            $location.path('canali');
        }
    };

    load();
}]);
