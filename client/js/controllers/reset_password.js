angular.module('intranetpluri')
    .controller('reset_password',
    ['$rootScope', '$scope', '$http', '$location', 'ngNotify', '$routeParams', 'UTENTE','CONFIG',
function ($rootScope, $scope, $http, $location, ngNotify, $routeParams, UTENTE, CONFIG) {

    //nasconde la navigazione a lato
    $rootScope.login = true;

    if ($routeParams.key)
          $http.get('/rest/user/'+$routeParams.key)
            .then(function (users) {
                $scope.user = users.data[0];
                // console.log($scope.user);
            }, function () {
                ngNotify.set(CONFIG.messages.utente_non_trovato_rowid, 'error');
            });

    $scope.pass_confirm = function () {
        if ($scope.password && $scope.password_confirm && $scope.password_confirm === $scope.password)
            return "fa fa-check text-success";

        return "fa fa-times text-danger";
    }

    $scope.scriviPassword = function () {
        if (!$scope.password || $scope.password.length < 5)
            ngNotify.set(CONFIG.messages.password_corta, 'error');
        else
            $scope.user.password = $scope.password;

        $http.post('/rest/user/reset_password',
            {utente_id: $scope.user._id, password: $scope.password})
            .then(function () {
                ngNotify.set(CONFIG.messages.password_modificata, 'success');
                $location.path('/login');
            }, function () {
                ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });

    }
}]);
