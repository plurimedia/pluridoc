angular.module('intranetpluri')
    .controller('user-upload',
    ['$rootScope', '$scope', 'ngNotify', '$routeParams', 'UTENTE', 'CONFIG',
function ($rootScope, $scope, ngNotify, $routeParams, UTENTE, CONFIG) {
    $scope.ricerca = {};
    $scope.ricerca.da = moment().startOf('month').format('YYYY-MM-DD');
    $scope.ricerca.a = moment().endOf('month').format('YYYY-MM-DD');
    $scope.items = [];
    $scope.hasSearched = false;

    //per i link dei tab
    $scope.utente_id = $routeParams.id;

    var load = function () {
        UTENTE.get($routeParams.id)
            .then(function (user) {
                $scope.utente = user.data;
            }, function () {
                ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });

        UTENTE.total_upload($routeParams.id)
            .then(function (dim) {
                $scope.totale = dim.data.dimensione;
            }, function () {
                ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
    };


    $scope.cerca = function () {
        var a = "", da = "";

        if ($scope.ricerca.da) da = moment(new Date($scope.ricerca.da)).format('YYYY-MM-DD');
        if ($scope.ricerca.a) a = moment(new Date($scope.ricerca.a)).format('YYYY-MM-DD');

        UTENTE.report_upload({_id: $routeParams.id, da: da, a: a})
            .then(function (results) {
                $scope.hasSearched = true;
                $scope.items = results.data;
            }, function () {
                ngNotify.set(CONFIG.messages.problema_tecnico, 'error');
            });
    };

    load();
}]);
