var rootResolver= {
   user: ['$rootScope','$q','$location','$timeout',
      function ($rootScope,$q,$location,$timeout)
      {
         var p;

         if ($rootScope.user) // && $rootScope.user.mail !== 'anonimo@anonimo.it'
            p= $rootScope.user;
         else
         {
            p= $q.defer();

            $rootScope.$watch('user',function (user)
            {
               // console.log('watch user', user, $rootScope.user)
               if (user===undefined) return;
               // console.log('path nel router', $location.path())

               if ($rootScope.user) // && $rootScope.user.mail !== 'anonimo@anonimo.it'
                  p.resolve($rootScope.user);
               else if ($location.path().indexOf('/reset_password')==0)
                  p.resolve();
               else
                  if ($location.path()!='/login')
                     $timeout(function() { $rootScope.destination= $location.url(); $location.path('/login'); }, 200);
            });
         }

         $('body').removeClass('mini-navbar') // minimizza il menu laterale
         return p.promise;
      }]
};

app.config(['$routeProvider', '$httpProvider',
function ($routeProvider, $httpProvider)
{
   $routeProvider
      .when('/login', { controller: 'login', templateUrl: '/html/views/login.html' })
      .when('/home', { controller: 'home', templateUrl: '/html/views/home.html', resolve: rootResolver, activenav: 'canali' })
      .when('/tags', { controller: 'report_tags', templateUrl: '/html/views/report_tags.html', resolve: rootResolver, activenav: 'tags' })
      .when('/canale-documenti', { controller: 'docs', templateUrl: '/html/views/docs.html', resolve: rootResolver, activetab: 'documenti', activenav: 'canali' })
      .when('/canale-utenti', { controller: 'docs-utenti', templateUrl: '/html/views/docs-utenti.html', resolve: rootResolver, activetab: 'utenti', activenav: 'canali' })
      .when('/canale-upload', { controller: 'docs-upload', templateUrl: '/html/views/docs-upload.html', resolve: rootResolver, activetab: 'documenti', activenav: 'canali' })
      .when('/canale-gruppi', { controller: 'docs-gruppi', templateUrl: '/html/views/docs-gruppi.html', resolve: rootResolver, activetab: 'gruppi', activenav: 'canali' })
      .when('/canale-impostazioni', { controller: 'docs-impostazioni', templateUrl: '/html/views/docs-impostazioni.html', resolve: rootResolver, activetab: 'impostazioni', activenav: 'canali' })
      .when('/canale-caricati', { controller: 'docs-caricati', templateUrl: '/html/views/docs-caricati.html', resolve: rootResolver, activetab: 'caricati', activenav: 'canali' })
      .when('/canale-scaricati', { controller: 'docs-scaricati', templateUrl: '/html/views/docs-scaricati.html', resolve: rootResolver, activetab: 'scaricati', activenav: 'canali' })
      .when('/canale-mail', { controller: 'docs-report1', templateUrl: '/html/views/docs-report1.html', resolve: rootResolver, activetab: 'canalerpt1', activenav: 'canali' })
      .when('/users', { controller: 'users', templateUrl: '/html/views/users.html', resolve: rootResolver, activenav: 'users' })
      .when('/user/:id', { controller: 'user', templateUrl: '/html/views/user.html', resolve: rootResolver, activetab: 'profilo', activenav: 'users' })
      .when('/user-channels/:id', { controller: 'user-channels', templateUrl: '/html/views/user-channels.html', resolve: rootResolver, activetab: 'channels', activenav: 'users' })
      .when('/user-download/:id', { controller: 'user-download', templateUrl: '/html/views/user-download.html', resolve: rootResolver, activetab: 'download', activenav: 'users' })
      .when('/user-upload/:id', { controller: 'user-upload', templateUrl: '/html/views/user-upload.html', resolve: rootResolver, activetab: 'upload', activenav: 'users' })
      .when('/reset_password/:key', { controller: 'reset_password', templateUrl: '/html/views/reset_password.html', resolve: rootResolver })
      .when('/change_password', { controller: 'change_password', templateUrl: '/html/views/change_password.html', resolve: rootResolver, activenav: 'impostazioni', activetab: 'password' })
      .when('/report/sintesi', { controller: 'report_sintesi', templateUrl: '/html/views/report_sintesi.html', resolve: rootResolver, activenav: 'report', activetab: 'report-sintesi' })
      .when('/report/canali', { controller: 'report_canali', templateUrl: '/html/views/report_canali.html', resolve: rootResolver, activenav: 'report', activetab: 'report-canali' })
      .when('/report/mail', { controller: 'report_mail', templateUrl: '/html/views/report_mail.html', resolve: rootResolver, activenav: 'report', activetab: 'report-mail' })
      .when('/report/mail/grafici', { controller: 'report_mail_graph', templateUrl: '/html/views/report_mail_graph.html', resolve: rootResolver, activenav: 'report', activetab: 'report-mail' })
      .when('/transfer', { controller: 'transfer', templateUrl: '/html/views/transfer.html', resolve: rootResolver, activenav: 'transfer' })
      .otherwise({ redirectTo: '/login' });
}])
.run(['$rootScope', '$route',
function ($rootScope, $route)
{
   $rootScope.$route = $route;
}]);
