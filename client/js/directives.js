/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function () {
                element.metisMenu();
            });
        }
    };
};
/**
 * minimalizaSidebar - Directive for minimalize sidebar
 */
 function minimalizaSidebar($timeout) {
    return {
        restrict: 'A',
        template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-default " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: function ($scope, $element) {
            $scope.minimalize = function () {
                $("body").toggleClass("mini-navbar");
                if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                    // Hide menu in order to smoothly turn on when maximize menu
                    $('#side-menu').hide();
                    // For smoothly turn on menu
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 200);
                } else if ($('body').hasClass('fixed-sidebar')){
                    $('#side-menu').hide();
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 100);
                } else {
                    // Remove all inline style from jquery fadeIn function to reset menu state
                    $('#side-menu').removeAttr('style');
                }
            }
        }
    };
};

function fastclick(){
    return {
        restrict: 'A',
        link: function(scope, element) {
            FastClick.attach(document.body);
        }
    };
}

function menusize(){
    return {
        restrict: 'A',
        link: function(scope, element) {
            window.addEventListener('resize', function (e) {
                if (this.innerWidth < 801) {
                    document.body.classList.add('body-small')
                } else {
                    document.body.classList.remove('body-small')
                }
            })
            window.addEventListener('load', function (e) {
                if (this.innerWidth < 801) {
                    document.body.classList.add('body-small')

                } else {
                    document.body.classList.remove('body-small')
                }
            })
        }
    };
}


/**
 *
 * Pass theme functions into module
 */

app
    .directive('sideNavigation', sideNavigation)
    .directive('minimalizaSidebar', minimalizaSidebar)
    .directive('fastclick', fastclick)
    .directive('menusize', menusize)
//Per treeView delle cartelle - Start ------------------------------------------------------------
    .value('treeViewDefaults',
    {
        foldersProperty: 'folders',
        filesProperty: 'files',
        displayProperty: 'nome',
        collapsible: false
    })
    .directive('treeView', ['$q', 'treeViewDefaults', function ($q, treeViewDefaults)
    {
        return {
            restrict: 'A',
            scope: {
                treeView: '=treeView',
                treeViewOptions: '=treeViewOptions'
            },
            replace: true,
            template:
            '<div class="tree">' +
            '<div tree-view-node="treeView">' +
            '</div>' +
            '</div>',
            controller: ['$scope', function ($scope) {
                var self = this,
                    selectedNode,
                    selectedFile;

                var options = angular.extend({}, treeViewDefaults, $scope.treeViewOptions);

                self.selectNode = function (node, breadcrumbs) {
                    if (selectedFile) {
                        selectedFile = undefined;
                    }
                    selectedNode = node;

                    if (typeof options.onNodeSelect === "function") {
                        options.onNodeSelect(node, breadcrumbs);
                    }
                };

                self.selectFile = function (file, breadcrumbs) {
                    if (selectedNode) {
                        selectedNode = undefined;
                    }
                    selectedFile = file;

                    if (typeof options.onNodeSelect === "function") {
                        options.onNodeSelect(file, breadcrumbs);
                    }
                };

                self.isSelected = function (node) {
                    return node === selectedNode || node === selectedFile;
                };

                self.getOptions = function () {
                    return options;
                };
            }]
        };
    }])
    .directive('treeViewNode', ['$q', '$compile', function ($q, $compile)
    {
        return {
            restrict: 'A',
            require: '^treeView',
            link: function (scope, element, attrs, controller)
            {
                var options = controller.getOptions(),
                    foldersProperty = options.foldersProperty,
                    filesProperty = options.filesProperty,
                    displayProperty = options.displayProperty,
                    collapsible = options.collapsible;

                scope.expanded = collapsible == false;

                scope.getFolderIconClass = function () {
                    if (scope.expanded && scope.hasChildren())
                       return 'fa fa-folder-open-o';
                    return 'fa fa-folder-o';
                };

                scope.getFileIconClass = typeof options.mapIcon === 'function'
                    ? options.mapIcon
                    : function (file) {
                    return 'fa fa-file-o';
                };

                scope.hasChildren = function () {
                    var node = scope.node;
                    return Boolean(node && (node[foldersProperty] && node[foldersProperty].length) || (node[filesProperty] && node[filesProperty].length));
                };

                scope.selectNode = function (event) {
                    event.preventDefault();

                    if (collapsible) {
                        toggleExpanded();
                    }

                    var breadcrumbs = [];
                    var nodeScope = scope;
                    while (nodeScope.node) {
                        breadcrumbs.push(nodeScope.node[displayProperty]);
                        nodeScope = nodeScope.$parent;
                    }
                    controller.selectNode(scope.node, breadcrumbs.reverse());
                };

                scope.selectFile = function (file, event) {
                    event.preventDefault();

                    var breadcrumbs = [file[displayProperty]];
                    var nodeScope = scope;
                    while (nodeScope.node) {
                        breadcrumbs.push(nodeScope.node[displayProperty]);
                        nodeScope = nodeScope.$parent;
                    }
                    controller.selectFile(file, breadcrumbs.reverse());
                };

                scope.isSelected = function (node) {
                    return controller.isSelected(node);
                };

                function toggleExpanded() {
                    //if (!scope.hasChildren()) return;
                    scope.expanded = !scope.expanded;
                }

                function render() {
                    var template =
                        '<div class="tree-folder" ng-repeat="node in ' + attrs.treeViewNode + '.' + foldersProperty + '">' +
                        '<a href="#" class="tree-folder-header inline" ng-click="selectNode($event)" ng-class="{ selected: isSelected(node) }">' +
                        '<i class="icon-folder-close" ng-class="getFolderIconClass()"></i> ' +
                        '<span class="tree-folder-name">{{ node.' + displayProperty + ' }}</span> ' +
                        '</a>' +
                        '<div class="tree-folder-content"'+ (collapsible ? ' ng-show="expanded"' : '') + '>' +
                        '<div tree-view-node="node">' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<a href="#" class="tree-item" ng-repeat="file in ' + attrs.treeViewNode + '.' + filesProperty + '" ng-click="selectFile(file, $event)" ng-class="{ selected: isSelected(file) }">' +
                        '<span class="tree-item-name"><i ng-class="getFileIconClass(file)"></i> {{ file.' + displayProperty + ' }}</span>' +
                        '</a>';

                    //Rendering template.
                    element.html('').append($compile(template)(scope));
                }

                render();
            }
        };
    }])
//Per treeView delle cartelle - End ------------------------------------------------------------
    /*.directive('checkboxAll', function ()
    {
        return function(scope, iElement, iAttrs)
        {
            var parts = iAttrs.checkboxAll.split('.');
            iElement.attr('type','checkbox');
            iElement.bind('change', function (evt)
            {
                scope.$apply(function ()
                {
                    var setValue = iElement.prop('checked');
                    angular.forEach(scope.$eval(parts[0]), function (v) {
                        v[parts[1]] = setValue;
                    });
                });
            });

            scope.$watch(parts[0], function (newVal)
            {
                var hasTrue= false, hasFalse= false;

                angular.forEach(newVal, function (v)
                {
                    if (v[parts[1]])
                        hasTrue = true;
                    else
                        hasFalse = true;
                });
                if (hasTrue && hasFalse)
                {
                    iElement.attr('checked', false);
                    iElement.prop('indeterminate', true);
                } else {
                    iElement.attr('checked', hasTrue);
                    iElement.prop('indeterminate', false);
                }
            }, true);
        };
    })*/
    .directive('uiSelectAll', ['$filter', function($filter)
    {
       return {
          restrict: 'E',
          template: '<input type="checkbox">',
          replace: true,
          link: function(scope, iElement, iAttrs) {
             function changeState(checked, indet) {
                iElement.prop('checked', checked).prop('indeterminate', indet);
             }
             function updateItems() {
                angular.forEach(scope.$eval(iAttrs.items), function(el) {
                   el[iAttrs.prop] = iElement.prop('checked');
                });
             }
             iElement.bind('change', function() {
                scope.$apply(function() { updateItems(); });
             });
             scope.$watch(iAttrs.items, function(newValue) {
                var checkedItems = $filter('filter')(newValue, function(el) {
                   return el[iAttrs.prop];
                });
                switch(checkedItems ? checkedItems.length : 0) {
                   case 0:                // none selected
                      changeState(false, false);
                      break;
                   case newValue.length:  // all selected
                      changeState(true, false);
                      break;
                   default:               // some selected
                      changeState(false, true);
                }
             }, true);
             updateItems();
          }
       };
    }]);
