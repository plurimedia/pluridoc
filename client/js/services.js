app
    .factory('CANALE', function (plsql, $rootScope, $q, store) {
        var dbc = plsql('no_canale');
        var dbuc = plsql('no_utenti_canali');
        var dbg = plsql('no_gruppo');
        return {
            selectedChannel : function () {
               return store.get('selectedChannel');
            },
            setSelectedChannel : function (c) {
                store.set('selectedChannel', c);
            },
            unsetSelectedChannel : function () {
                store.remove('selectedChannel');
            },
            all : function () {
                return dbc.readAll({});
            },
            canali_utente : function (obj) {
                return dbuc.method('read_canali_utente', obj || {});
            },
            utenti_canale : function (canale_id, ruolo, cartella_id) {
                var pars = { canale_id: canale_id };
                if (ruolo) pars.ruolo = ruolo;
                if (cartella_id) pars.cartella_id = cartella_id;
                return dbuc.method('read_utenti_canale', pars);
            },
            gruppi_canale : function (canale_id) {
                return dbg.method('read_gruppi_canale', { canale_id: canale_id });
            },
            save : function (channel) {
                if (channel._id) return dbc.update(channel);
                return dbc.create(channel);
            },
            get_ruolo : function (canale_id) {
                return dbuc.method('read_ruolo', { canale_id: canale_id });
            },
            associa : function (canale_id, utente_id, ruolo) {
                return dbuc.method('create_or_update', {
                    canale: canale_id,
                    utente: utente_id,
                    ruolo: ruolo
                });
            },
            blocca : function (canale) {
                canale.blocked = 1;
                return dbc.update(canale);
            },
            sblocca : function (canale) {
                canale.blocked = 0;
                return dbc.update(canale);
            },
            report_download : function (obj) {
                deferred= $q.defer();

                dbc.method('report_download',obj)
                    .then(function(res) {
                        res.data.forEach(function(el) {
                            //metto le date in formato iso altrimenti il formatter angular si lamenta
                            el.mdate = moment(el.mdate, 'DD-MM-YYYY HH.mm.ss').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                        });
                        deferred.resolve(res);
                    }, function(err) { deferred.reject(err); });

                return deferred.promise;
            },
            report_upload : function (obj) {
                deferred= $q.defer();

                dbc.method('report_upload',obj)
                    .then(function(res) {
                        res.data.forEach(function(el) {
                            //metto le date in formato iso altrimenti il formatter angular si lamenta
                            el.mdate = moment(el.mdate, 'DD-MM-YYYY HH.mm.ss').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                        });
                        deferred.resolve(res);
                    }, function(err) { deferred.reject(err); });

                return deferred.promise;
            },
            associa_docs : function (obj) {
                return new Promise(function(resolve, reject) {
                    dbc.method('associa_docs',obj)
                        .then(function(res) {
                            resolve(res)
                        }, function(err) { deferred.reject(err); });
                })
            }
        };
    })
    .factory('CARTELLA', function (plsql, $rootScope, store) {
        var dbc = plsql('no_cartella');
        return {
            selectedFolder : function () {
                return store.get('selectedFolder');
            },
            setSelectedFolder : function (f) {
                store.set('selectedFolder', f);
            },
            unsetSelectedFolder : function () {
                store.remove('selectedFolder');
            },
            breadCrumb : function () {
                if (this.selectedFolder()) {

                    var briciole = this.selectedFolder().percorso.split('/'), path;

                    var breadcrumb = [];

                    briciole.forEach(function (b) {
                        if (path)
                            path = path + '/' + b;
                        else {
                            path = b;
                            b = store.get('selectedChannel').nome;
                        }

                        breadcrumb.push({nome: b, percorso: path});
                    });

                    return breadcrumb;
                } else
                    throw "notAuthorized";
            },
            cartelle_path : function (canale_id, dt) {
                return dbc.method('read_cartelle_path', { cid: canale_id, data: dt });
            },
            save : function (folder) {
                if (folder._id) return dbc.update(folder);
                return dbc.create(folder);
            },
            delete : function (cartella_id) {
                return dbc.delete_(cartella_id);
            },
            rename : function (cartella_id, nome) {
                return dbc.method('rinomina', { id: cartella_id, nome: nome });
            },
            sposta : function (da_spostare, destinazione) {
                return dbc.method('sposta', { id: da_spostare, destinazione: destinazione });
            },
            associa : function (postdata) {
                return dbc.method('associa', postdata);
            },
            get_ruolo_canale_by_cartella : function (cartella_id) {
                return dbc.method('get_ruolo_canale_by_cartella', { id: cartella_id })
            }
        }
    })
    .factory('UTENTE', function (plsql, store, $q) {
        var dbu = plsql('no_utente'),
            dbl = plsql('no_logs');
        return {
            all : function (obj) {
                return dbu.readAll(obj || {});
            },
            get : function (id) {
                return dbu.read(id);
            },
            cerca : function (term) {
                return dbu.readAll({ search:term });
            },
            all_temp : function () {
                return dbu.readAll({ temp : 1 });
            },
            user : function () {
                return store.get('user');
            },
            setUser : function (u) {
                store.set('user', u);
            },
            unsetUser : function () {
                store.remove('user');
                return dbl.method('logout', {})
            },
            mails_by_ids : function (utenti) {
                return dbu.method('get_mails_by_ids',utenti);
            },
            riattiva : function (mail) {
                return dbu.method('attiva',mail);
            },
            save : function (user) {
                if (user._id) return dbu.update(user);
                return dbu.create(user);
            },
            login_log : function (id) {
                return dbl.readAll({ utente_id: id, tipo : 'LOGIN' })
            },
            delete : function (id) {
                return dbu.delete_(id);
            },
            report_download : function (obj) {
                deferred= $q.defer();

                dbu.method('report_download',obj)
                    .then(function(res) {
                        res.data.forEach(function(el) {
                            //metto le date in formato iso altrimenti il formatter angular si lamenta
                            el.mdate = moment(el.mdate, 'DD-MM-YYYY HH.mm.ss').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                        });
                        deferred.resolve(res);
                    }, function(err) { deferred.reject(err); });


                return deferred.promise;
            },
            report_upload : function (obj) {
                deferred= $q.defer();

                dbu.method('report_upload',obj)
                    .then(function(res) {
                        res.data.forEach(function(el) {
                            //metto le date in formato iso altrimenti il formatter angular si lamenta
                            el.mdate = moment(el.mdate, 'DD-MM-YYYY HH.mm.ss').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                        });
                        deferred.resolve(res);
                    }, function(err) { deferred.reject(err); });


                return deferred.promise;
            },
            total_upload : function (id) {
                return dbu.method('total_upload', { utente_id : id });
            },
            total_download : function (id) {
                return dbu.method('total_download', { utente_id : id });
            },
            change_password: function (obj) {
                return dbu.method('change_password', obj);
            },
            associa_docs_utente: function (obj) {
                return dbu.method('associa_docs_utente', obj);
            }
        }
    })
    .factory('GRUPPO', function (plsql) {
        var dbg = plsql('no_gruppo');
        return {
            all_canale : function (id_canale) {
                return dbg.method('read_gruppi_canale', { canale_id: id_canale });
            },
            all : function () {
                return dbg.readAll({});
            },
            save : function (group) {
                if (group.gruppo_id)
                {
                    group._id = group.gruppo_id;
                    //hack per le checkbox
                    if (group.visibile) group.visibile = 1; else group.visibile = 0;
                    return dbg.update(group);
                }
                return dbg.create(group);
            },
            delete : function (gruppo_id) {
                return dbg.delete_(gruppo_id);
            }
        }
    })
    .factory('DOCUMENTO', function (plsql) {
        var dbd = plsql('no_documento');
        return {
            all : function (cartella_id, canale_id, orderbydate) {
                var pars = { cartella_id: cartella_id, canale_id: canale_id }
                if (orderbydate) pars.orderbydate = 1
                return dbd.readAll(pars);
            },
            create : function (obj) {
                return dbd.create(obj);
            },
            assegna : function (postdata) {
                return dbd.method('assegna', postdata);
            },
            assegna_multiple : function (postdata) {
                return dbd.method('assegna_multiple', postdata);
            },
            delete_multiple : function (postdata) {
                return dbd.method('delete_multiple', postdata);
            },
            restore_multiple : function (postdata) {
                return dbd.method('restore_multiple', postdata);
            },
            rename : function (documento_id, name) {
                return dbd.method('rinomina', { id: documento_id, nome: name });
            },
            scadenza : function (documento_id, scadenza) {
                return dbd.method('scadenza', { id: documento_id, scadenza: scadenza });
            },
            find_tag : function (name) {
                return dbd.method('find_tag', { nome: name });
            },
            find_by_tags : function (postdata) {
                return dbd.method('find_by_tags', postdata);
            },
            save_meta : function (postdata) {
                return dbd.method('save_meta', postdata);
            },
            delete : function (documento_id) {
                return dbd.delete_(documento_id);
            }
        }
    })
    .factory('REPORT', function (plsql, $q) {
        var dbr = plsql('no_report_sintesi');
        var dbrc = plsql('no_report_canali');
        var dbml = plsql('no_mail_log');
        return {
            get_job_status : function () {
                return dbr.method('get_job_status', {});
            },
            get_total_bytes : function () {
                return dbr.method('get_total_bytes', {});
            },
            read_anno : function (anno) {
                return dbr.method('read_anno', { anno : anno});
            },
            all_canali : function () {
                return dbrc.readAll({});
            },
            read_invio : function (obj) {
                deferred= $q.defer();

                dbml.method('read_invio', obj || {})
                    .then(function(res) {
                        res.data.forEach(function(el) {
                            //metto le date in formato iso altrimenti il formatter angular si lamenta
                            el.cdate = moment(el.cdate, 'DD-MM-YYYY HH.mm.ss').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                        });
                        deferred.resolve(res);
                    }, function(err) { deferred.reject(err); });


                return deferred.promise;
            },
            all_mail : function (obj) {
                return dbml.readAll(obj || {});
            }
        }
    });
