var app = angular.module('intranetpluri',['ngRoute','mgcrea.ngStrap','infinite-scroll','ngNotify','ngFileUpload','angularUtils.directives.dirPagination','angular-storage','ngSanitize','pascalprecht.translate','mwl.confirm','ngTagsInput'])
    .config(function ($datepickerProvider) {
        angular.extend($datepickerProvider.defaults, {
            dateFormat: 'dd/MM/yyyy',
            startWeek: 1,
            templateUrl: 'html/templates/datepicker.tpl.html'
        });
    })
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.put['Content-Type']='application/json';
        $httpProvider.defaults.headers.post['Content-Type']='application/json';
        //$httpProvider.responseInterceptors.push('myHttpInterceptor');
        var spinnerFunction = function (data, headersGetter) {
            $('.fa-refresh').show();
            return data;
        };
        $httpProvider.defaults.transformRequest.push(spinnerFunction);

    }])
    .factory('myHttpInterceptor', ['$q','$window',function ($q, $window) {
        return function (promise) {
            return promise.then(function (response) {
                $('.fa-refresh').hide();
                return response;

            }, function (response) {
                $('.fa-refresh').hide();
                return $q.reject(response);
            });
        };
    }])
    .run(['$rootScope', '$location', '$http', '$translate', '$timeout', 'REPORT', 'CONFIG', 'UTENTE', 'CANALE',
        function ($rootScope, $location, $http, $translate, $timeout, REPORT, CONFIG, UTENTE, CANALE)
        {
            // inizializzazione rootScope
            $rootScope.active= function (path,strict)
            {
                return (strict ? $location.path() : $location.path().substr(0, path.length)) == path;
            };

            /*if (UTENTE.user())
              $rootScope.user = UTENTE.user()
            else*/
               $http.get('/rest/user').then(function (user)
               {
                  $rootScope.user= user.data;
                  $rootScope.user.orderbydate = false;
                  UTENTE.setUser(user.data);
                  CANALE.unsetSelectedChannel;
               });

            $http.get('/rest/company').then(function (company)
            {
                $rootScope.company = company.data;

                REPORT.get_total_bytes()
                    .then(function(dati) {
                        $rootScope.tot_spazio_occupato = dati.data.tot;
                    }, function (err) { console.log(err.data);});
            });

            $rootScope.logout= function()
            {
                UTENTE.unsetUser()
                    .then(function() {
                        $http.get('/rest/logout').then(function ()
                        {
                            $rootScope.user= undefined;
                            CANALE.unsetSelectedChannel;
                            window.location = '/';
                        });
                });
            };

            $rootScope.$on('$locationChangeStart', function(evt, absNewUrl, absOldUrl)
            {
                $rootScope.previousPage = absOldUrl.substring(absOldUrl.indexOf("#") + 1);
                // console.log('on locationChangeStart', $rootScope.user, $location.path())

                function checkExpiredToken (obj) {
                    const dateNow = new Date().getTime();
                    // console.log('checkExpiredToken', $rootScope.user, dateNow, obj.expires, $location.path())
                    if ($rootScope.user.mail === 'anonimo@anonimo.it') return true
                    return dateNow <= obj.expires
                }

                $timeout(function (){
                    $rootScope.user = JSON.parse(localStorage.getItem('user'))
                    // console.log('on timeout', $rootScope.user, $location.path())
                    if (!$rootScope.user || !checkExpiredToken($rootScope.user) || (($rootScope.user.mail === 'anonimo@anonimo.it') && (
                        $location.path() === '/users' || $location.path().indexOf('/user/') === 0 || 
                        $location.path().indexOf('/user-channels/') === 0 || $location.path().indexOf('/user-download/') === 0 || 
                        $location.path().indexOf('/user-upload/') === 0 || $location.path().indexOf('/report/') === 0 || 
                        $location.path().indexOf('/transfer') === 0
                    ))) {
                        if ($location.path() !== '/login/' && $location.path().indexOf('/reset_password') === -1) {
                            $rootScope.user = undefined
                            $location.path('/login');
                        }
                    }
                }, 500)
            });

            $http.get('/rest/get_lang').then(function (res)
            {
               $translate.use(res.data.lang);
               CONFIG.messages = CONFIG['messages_'+res.data.lang];
               $rootScope.CONFIG = CONFIG;
            });
        }])
    //per i tab active
_.rm= function(arr, elem)
{
    if (arr) arr.splice(arr.indexOf(elem),1);
};

_.base64ToArrayBuffer= function(base64)
{
    var binary_string =  window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)
    {
        var ascii = binary_string.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes.buffer;
}

_.intersect_by_field= function(arr1, arr2, field, cb) {
    //se gli passo una callback chiama quella funzione quando trova un elemento comune
    //se non gliela passo, mi ritorna gli elementi del primo array intersecanti col secondo
    var result = [];
    _.each(arr1, function(el) {
        _.each(arr2, function(ele) {
            if (el[field])
                if (ele[field])
                    if (el[field] === ele[field])
                        if (cb)
                            cb(el, ele);
                        else
                            result.push(el);
        });
    });

    if (!cb)
        return result;
}

Number.prototype.round = function(p) {
    p = p || 10;
    return parseFloat( this.toFixed(p) );
};

Number.prototype.bytes_to_mega = function() {
    return this/1048576;
};

String.prototype.cleanFileName = function() {
    return this.replace('+', '_');
}
