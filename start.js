const app = require('./app')

const port = process.env.PORT || 8082

console.log('server on port ' + port)
const server = app.listen(port)
server.setTimeout(6000000)

process.on('SIGTERM', () => {
  console.info('SIGTERM signal received')
  process.exit(0)
})
process.on('SIGINT', () => {
  console.info('SIGINT signal received')
  process.exit(0)
})
