const { S3Client, GetObjectCommand, DeleteObjectCommand, PutObjectCommand, CopyObjectCommand } = require('@aws-sdk/client-s3')
const { getSignedUrl } = require('@aws-sdk/s3-request-presigner')
const conf = require('../config')

const s3 = new S3Client({
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_KEY
  },
  signatureVersion: 'v4',
  region: 'eu-west-1'/* ,
  useAccelerateEndpoint: true */
})

exports.read = async function (path, cb) {
  const command = new GetObjectCommand({
    Bucket: process.env.AWS_BUCKET,
    Key: path
  })
  const item = await s3.send(command)
  cb(item.Body)
}

exports.delete = async function (path, cb) {
  const command = new DeleteObjectCommand({
    Bucket: process.env.AWS_BUCKET,
    Key: path
  })
  const item = await s3.send(command)
  cb(item.Body)
}

exports.put = async function (path, file, tipo, cb) {
  const s3Params = {
    Bucket: conf.bucket,
    Body: file,
    Key: path,
    ACL: 'public-read',
    ContentType: tipo
  }

  const command = new PutObjectCommand(s3Params)
  const item = await s3.send(command)
  cb(item.Body)
}

exports.signedUrl = async (body, path, cb) => {
  // console.log('"' + process.env.AWS_ACCESS_KEY_ID + '"' + '"' + process.env.AWS_SECRET_KEY + '"')
  const s3Params = {
    Bucket: conf.bucket,
    // Body: body.file,
    Key: path,
    ACL: 'public-read',
    ContentType: body.tipo
  }

  const command = new PutObjectCommand(s3Params)

  const signedUrl = await getSignedUrl(s3, command, { expiresIn: 60 })
  // console.log(signedUrl)
  cb(signedUrl)
}

exports.signedUrlDownload = async (docName, path, inline, cb) => {
  const command = new GetObjectCommand({
    Bucket: conf.bucket,
    Key: path,
    ResponseContentDisposition: `${inline}; filename="${docName}"`
  })

  const signedUrl = await getSignedUrl(s3, command, { expiresIn: 10 })
  // console.log(signedUrl)
  cb(signedUrl)
}

const changeStorageClass = async () => {
  // procedura di test : cambia la storage class di un certo file. Da parametrizzare.
  console.log('cambio storage class')
  // x-amz-storage-class STANDARD | REDUCED_REDUNDANCY | STANDARD_IA | ONEZONE_IA | INTELLIGENT_TIERING | GLACIER | DEEP_ARCHIVE | OUTPOSTS | GLACIER_IR
  // x-amz-metadata-directive COPY | REPLACE
  const command = new CopyObjectCommand({
    Bucket: process.env.AWS_BUCKET,
    Key: 'localhost/16/1663154052007attestato-corso-sicurezza21008-51342.pdf',
    CopySource: process.env.AWS_BUCKET + '/localhost/16/1663154052007attestato-corso-sicurezza21008-51342.pdf',
    StorageClass: 'STANDARD_IA',
    MetadataDirective: 'REPLACE'
  })
  try {
    const item = await s3.send(command)
    console.log('risultato', item)
  } catch (err) {
    console.error('errore', err)
  }
}

/* INUTILI???

exports.write= function (path,cb)
{
   var wstream = new stream.Stream()
   wstream.writable = true
   var rstream = new stream.Stream()
   rstream.readable = true;

   wstream.write = function (data)
   {
       rstream.emit('data',data);
       return true; // true means 'yes i am ready for more data now'
       // OR return false and emit('drain') when ready later
   }

   wstream.end = function (data)
   {
       if (data) rstream.emit('data',data);
       rstream.emit('end');
   }

   new MultiPartUpload
   ({
        client: s3,
        objectName: path,
        stream: rstream
   },
   function(err, res)
   {
      if (err) console.log('file.s3.write',err);

       wstream.emit('close');
   });

   cb(wstream);
}

exports.size= function (path, cb)
{
   s3.headFile(path, function(err, res)
   {
      if (err) throw err;
      cb(parseInt(res.headers['content-length']));
   });
}

exports.copyDir= function (src,dest,cb)
{
   var queue= async.queue(function (key,done)
   {
        s3.copyFile(key,key.replace(src,dest),done);
   },10);

   queue.drain= cb;

   var found= false;

   s3.streamKeys({ prefix: src })
     .on('data', function (key)
     {
        found= true;
        queue.push(key,function (err) { console.log(err); });
     })
     .on('end', function ()
     {
        if (!found) cb();
     });
} */
