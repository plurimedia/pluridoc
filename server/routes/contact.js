var async= require('async'),
    uuid= require('node-uuid').v4,
    _= require('underscore'),
    Imap= require('imap'),
    mimelib= require('mimelib'),
    base64= require('base64-stream');

var parts= [], mailUser, mailPwd, mailBox;

function init_vars(req)
{
    mailUser= req.channel.mailUser;
    mailPwd= req.channel.mailPwd;
    mailBox= req.channel.mailBox;
}

function getbody(imap, msgid, partid, charset, encoding, cb)
{
    var body= {};

    imap.search([ ['X-GM-MSGID', msgid] ], function(err, results)
    {
      if (err)
         console.log(err);

      var f = imap.fetch(results,
              {
                bodies: [partid],
                struct: false
              });

      f.on('message', function(msg)
      {
          msg.on('body', function(stream, info)
          {
            var buffer= '';

            stream.on('data', function(chunk)
            {
              buffer += chunk.toString('utf8');
            });
            stream.once('end', function()
            {
              if (encoding === 'BASE64')
              {
                 body= { "text": mimelib.decodeBase64(buffer) };
              }
              else
                 body= { "text": mimelib.decodeQuotedPrintable(buffer, null, charset) };
            });
          });
      });

      f.once('error', function(err)
      {
        console.log('Fetch error: ' + err);
      });

      f.once('end', function()
      {
        cb(body);
      });
    });
};

size= function(bytes)
{
    if (bytes<1024) return bytes+' b';

    if (bytes<1024*1024) return (bytes/1024).toFixed(2)+' Kb';

    return (bytes/1024/1024).toFixed(2)+' Mb';
}

function parseDate(date)
{
    var d = new Date(date);
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    var aacurr= new Date().getFullYear();
    var mmcurr= new Date().getMonth() + 1;
    var ggcurr= new Date().getDate();

    if (year === aacurr &&
        month === mmcurr &&
        day === ggcurr)
       return d.getHours() + ":" + ("0" + d.getMinutes()).slice(-2);

    return ("0" + day).slice(-2) + '/' + ("0" + month).slice(-2) + '/' + year.toString().substr(2);
}

function getAttachments(struct)
{
    var attachments= [];

    if (struct.length > 1)
    {
      for (var i = 1; i < struct.length; i++)
      {
         if (struct[i][0].type != 'text' && struct[i][0].type != 'alternative' && struct[i][0].params && struct[i][0].params.name)
            attachments.push({ "partid": struct[i][0].partID, "nome": struct[i][0].params.name, "size": size(struct[i][0].size), "type": struct[i][0].type, "subtype": struct[i][0].subtype });
      }
    }

    return attachments;
}

function getParts(struct)
{
    _.each(struct, function(v, k)
    {
        if (_.isArray(v))
           getParts(v);
        else
           if (_.isObject(v))
           {
              if (_.has(v, "partID") && _.has(v, "type") && _.has(v, "params"))
                 parts.push({ "partid": v.partID, "type": v.type, "subtype": v.subtype, "encoding": v.encoding, "params": v.params });
              else if (_.has(v, "partID") && _.has(v, "type"))
                 parts.push({ "partid": v.partID, "type": v.type, "subtype": v.subtype, "encoding": v.encoding });
           }
    });
}

exports.emails= function (req, res)
{
    init_vars(req);

    if (!mailUser || !mailPwd || !mailBox) return;

    var imap = new Imap({
                  user: mailUser,
                  password: mailPwd,
                  host: 'imap.gmail.com',
                  port: 993,
                  tls: true,
                  tlsOptions: { rejectUnauthorized: false }
                });
    var anno= parseInt(req.query.anno),
        mese= parseInt(req.query.mese),
        myheader,
        mycrit,
        mymails= [],
        bodyid,
        done= function()
        {
            async.forEach(Object.keys(mymails),
            function (_key, done)
            {
                var key= parseInt(_key),
                    enc;

                if (mymails[key].bodyid)
                {
                   enc= _.findWhere(mymails[key].parts, {partid: mymails[key].bodyid }).encoding;
                   getbody(imap, mymails[key].msgid, mymails[key].bodyid, mymails[key].charset, enc, function (body)
                   {
                       mymails[key].body= body;
                       done();
                   });
                }
                else
                   mymails[key].body= '';
            },
            function (err)
            {
                res.send(mymails);
                imap.end();
            });
        };

    function openInbox(cb)
    {
      imap.openBox(mailBox, true, cb);
    }

    imap.once('ready', function()
    {
      openInbox(function(err, box)
      {
        if (err) throw err;

        if (mese == 12)
           mycrit= 'in:inbox subject:"extranet" after:'+anno+'/12/01 before:'+(anno+1)+'/01/01';
        else
           mycrit= 'in:inbox subject:"extranet" after:'+anno+'/'+("0" + mese).slice(-2)+'/01 before:'+anno+'/'+("0" + (mese+1)).slice(-2)+'/01';

        imap.search([ ['X-GM-RAW', mycrit] ], function(err, results)
        {
          if (err)
          {
             console.log(err);
             done();
          }
          else
          {
             try
             {
                  var f = imap.fetch(results,
                          {
                            bodies: ['HEADER.FIELDS (SUBJECT DATE X-GM-MSGID)'],
                            struct: true
                          });

                  f.on('message', function(msg, seqno)
                  {
                      msg.on('body', function(stream, info)
                      {
                        var buffer= '',
                            charset= 'UTF-8';
                        stream.on('data', function(chunk)
                        {
                          buffer += chunk.toString('utf8');
                        });
                        stream.once('end', function()
                        {
                          myheader= Imap.parseHeader(buffer);
                        });
                      });

                      msg.once('attributes', function(attrs)
                      {
                        myattrs= attrs;
                      });
                      msg.once('end', function()
                      {
                         var charset= 'UTF-8';
                         parts= [];
                         bodyid= null;

                         getParts(myattrs.struct);
                         for (var i = 0; i < parts.length; i++)
                         {
                             if (parts[i].type === 'text' && parts[i].subtype === 'plain')
                             {
                                bodyid= parts[i].partid;
                                if (parts[i].params && parts[i].params.charset)
                                   charset= parts[i].params.charset;
                             }
                         }

                         if (myheader.subject[0]!='' && myheader.subject[0].length > 90)
                            myheader.subject[0]= myheader.subject[0].substring(0,90) + '...';

                         if (myheader.subject=='')
                            myheader.subject[0]= '(Senza oggetto)';

                         mymails.push({"msgid"      : myattrs['x-gm-msgid'],
                                       "date"       : myheader.date[0],
                                       "subject"    : myheader.subject[0],
                                       "attachments": getAttachments(myattrs.struct),
                                       "bodyid"     : bodyid,
                                       "body"       : { "text": '???' },
                                       "charset"    : charset,
                                       "parts"      : parts
                                     });
                      });
                  });

                  f.once('error', function(err)
                  {
                    console.log('Fetch error: ' + err);
                  });

                  f.once('end', function()
                  {
                    mymails
                    .sort(function(a, b)
                    {
                      var dateA= new Date(a.date),
                          dateB= new Date(b.date);

                      return dateB - dateA; //sort by date descending
                    })
                    .forEach(function(mail)
                    {
                      mail.date= parseDate(mail.date);
                    });

                    done();
                  });
             }
             catch (error) { console.log("Error fetching Emails " + mycrit); imap.end(); done(); }
          }
        });
      });
    });

    imap.once('error', function(err)
    {
      console.log(err);
    });

    imap.once('end', function()
    {
      console.log('Connection ended');
    });
    imap.connect();
};
exports.getmesi= function (req, res)
{
    init_vars(req);

    if (!mailUser || !mailPwd || !mailBox) return;

    var imap = new Imap({
                  user: mailUser,
                  password: mailPwd,
                  host: 'imap.gmail.com',
                  port: 993,
                  tls: true,
                  tlsOptions: { rejectUnauthorized: false }
                });
    var anno= parseInt(req.query.anno),
        mesi= [],
        mycrit= '',
        myheader,
        done= function()
        {
           var corrente= new Date().getFullYear();
           if (corrente === anno)
              mesi.reverse();
           res.send(mesi);
        };

    mesi[0]= { "ordine": 0, "nome": 'Gennaio', "numero": 0 };
    mesi[1]= { "ordine": 1, "nome": 'Febbraio', "numero": 0 };
    mesi[2]= { "ordine": 2, "nome": 'Marzo', "numero": 0 };
    mesi[3]= { "ordine": 3, "nome": 'Aprile', "numero": 0 };
    mesi[4]= { "ordine": 4, "nome": 'Maggio', "numero": 0 };
    mesi[5]= { "ordine": 5, "nome": 'Giugno', "numero": 0 };
    mesi[6]= { "ordine": 6, "nome": 'Luglio', "numero": 0 };
    mesi[7]= { "ordine": 7, "nome": 'Agosto', "numero": 0 };
    mesi[8]= { "ordine": 8, "nome": 'Settembre', "numero": 0 };
    mesi[9]= { "ordine": 9, "nome": 'Ottobre', "numero": 0 };
    mesi[10]= { "ordine": 10, "nome": 'Novembre', "numero": 0 };
    mesi[11]= { "ordine": 11, "nome": 'Dicembre', "numero": 0 };

    function openInbox(cb)
    {
      imap.openBox(mailBox, true, cb);
    }

    imap.once('ready', function()
    {
      openInbox(function(err, box)
      {
        if (err) throw err;

        mycrit= 'in:inbox subject:"extranet" after:'+anno+'/01/01 before:'+(anno+1)+'/01/01';

        imap.seq.search([ ['X-GM-RAW', mycrit] ], function(err, results)
        {
          if (err)
          {
             console.log(err);
             done();
          }
          else
          {
             try
             {
                  var f = imap.seq.fetch(results,
                          {
                            bodies: 'HEADER.FIELDS (DATE)',
                            struct: false
                          });

                  f.on('message', function(msg)
                  {
                      msg.on('body', function(stream, info)
                      {
                        var buffer= '';
                        stream.on('data', function(chunk)
                        {
                          buffer += chunk.toString('utf8');
                        });
                        stream.once('end', function()
                        {
                          myheader= Imap.parseHeader(buffer);
                        });
                      });

                      msg.once('end', function()
                      {
                        if (myheader.date[0])
                        {
                           var date = new Date(myheader.date[0]);

                           mesi[date.getMonth()].numero++;
                        }
                        else
                           console.log('Error parsing header', myheader);
                      });

                  });

                  f.once('error', function(err)
                  {
                    console.log('Fetch error: ' + err);
                  });

                  f.once('end', function()
                  {
                    imap.end();
                    done();
                  });
             }
             catch (error) { console.log("Error fetching Emails " + mycrit); imap.end(); done(); }
          }
        });
      });
    });

    imap.once('error', function(err)
    {
      console.log(err);
    });

    imap.once('end', function()
    {
      console.log('Connection ended');
    });

    imap.connect();
};
exports.getattach= function (req, res)
{
    init_vars(req);

    var imap = new Imap({
                  user: mailUser,
                  password: mailPwd,
                  host: 'imap.gmail.com',
                  port: 993,
                  tls: true,
                  tlsOptions: { rejectUnauthorized: false }
                });
    var msgid= req.params.msgid,
        partid= req.params.partid,
        type= req.params.type,
        subtype= req.params.subtype;

    res.setHeader('Content-Type', type+'/'+subtype);
    //res.setHeader('Content-Disposition', 'attachment;');

    function openInbox(cb)
    {
      imap.openBox(mailBox, true, cb);
    }

    imap.once('ready', function()
    {
      openInbox(function(err, box)
      {
        if (err) throw err;

        imap.search([ ['X-GM-MSGID', msgid] ], function(err, results)
        {
          if (err)
             console.log(err);

          var f = imap.fetch(results,
                  {
                    bodies: [partid],
                    struct: false
                  });

          f.on('message', function(msg)
          {
            msg.on('body', function(stream, info)
            {
              stream.pipe(base64.decode()).pipe(res);
            });
          });

          f.once('error', function(err)
          {
            console.log('Fetch error: ' + err);
          });

          f.once('end', function()
          {
            imap.end();
          });
        });
      });
    });

    imap.once('error', function(err)
    {
      console.log(err);
    });

    imap.once('end', function()
    {
      console.log('Connection ended');
    });

    imap.connect();
};
exports.search= function (req, res)
{
    init_vars(req);

    var imap = new Imap({
                  user: mailUser,
                  password: mailPwd,
                  host: 'imap.gmail.com',
                  port: 993,
                  tls: true,
                  tlsOptions: { rejectUnauthorized: false }
                });
    var what= req.query.what,
        myheader,
        mycrit,
        mymails= [],
        bodyid,
        enc,
        done= function()
        {
            async.forEach(Object.keys(mymails),
            function (_key, done)
            {
                var key= parseInt(_key);

                if (mymails[key].bodyid)
                {
                   enc= _.findWhere(mymails[key].parts, {partid: mymails[key].bodyid }).encoding;
                   getbody(imap, mymails[key].msgid, mymails[key].bodyid, mymails[key].charset, enc, function (body)
                   {
                       mymails[key].body= body;
                       done();
                   });
                }
                else
                   mymails[key].body= '';
            },
            function (err)
            {
                res.send(mymails);
                imap.end();
            });
        };

    function openInbox(cb)
    {
      imap.openBox(mailBox, true, cb);
    }

    imap.once('ready', function()
    {
      openInbox(function(err, box)
      {
        if (err) throw err;

        mycrit= 'in:inbox subject:"extranet" ' + what;

        imap.search([ ['X-GM-RAW', mycrit] ], function(err, results)
        {
          if (err)
          {
             console.log(err);
             done();
          }
          else
          {
             try
             {
                  var f = imap.fetch(results,
                          {
                            bodies: ['HEADER.FIELDS (SUBJECT DATE X-GM-MSGID)'],
                            struct: true
                          });

                  f.on('message', function(msg, seqno)
                  {
                      msg.on('body', function(stream, info)
                      {
                        var buffer= '',
                            charset= 'UTF-8';

                        stream.on('data', function(chunk)
                        {
                          buffer += chunk.toString('utf8');
                        });
                        stream.once('end', function()
                        {
                          myheader= Imap.parseHeader(buffer);
                        });
                      });

                      msg.once('attributes', function(attrs)
                      {
                        myattrs= attrs;
                      });
                      msg.once('end', function()
                      {
                         parts= [];
                         bodyid= null;
                         getParts(myattrs.struct);
                         for (var i = 0; i < parts.length; i++)
                         {
                             if (parts[i].type === 'text' && parts[i].subtype === 'plain')
                             {
                                bodyid= parts[i].partid;
                                if (parts[i].params && parts[i].params.charset)
                                   charset= parts[i].params.charset;
                             }
                         }
                         if (myheader.subject[0].length > 90)
                            myheader.subject[0]= myheader.subject[0].substring(0,90) + '...';
                         else if (!myheader.subject[0])
                            myheader.subject[0]= '-';

                         mymails.push({"msgid"      : myattrs['x-gm-msgid'],
                                       "date"       : myheader.date[0],
                                       "subject"    : myheader.subject[0],
                                       "attachments": getAttachments(myattrs.struct),
                                       "bodyid"     : bodyid,
                                       "body"       : { "text": '???' },
                                       "charset"    : charset,
                                       "parts"      : parts
                                     });
                      });
                  });

                  f.once('error', function(err)
                  {
                    console.log('Fetch error: ' + err);
                  });

                  f.once('end', function()
                  {
                    mymails
                    .sort(function(a, b)
                    {
                      var dateA= new Date(a.date),
                          dateB= new Date(b.date);

                      return dateB - dateA; //sort by date descending
                    })
                    .forEach(function(mail)
                    {
                      mail.date= parseDate(mail.date);
                    });

                    done();
                  });
             }
             catch (error) { console.log("Error fetching Emails " + mycrit); imap.end(); done(); }
          }
        });
      });
    });

    imap.once('error', function(err)
    {
      console.log(err);
    });

    imap.once('end', function()
    {
      console.log('Connection ended');
    });
    imap.connect();
};
