const jwt = require('jsonwebtoken')
const request = require('request')
const express = require('express')
const app = express()
app.use(express.urlencoded({ extended: true }))
app.use(express.static('public'))
const port = 3000

const payload = {
	iss: process.env.ZOOM_API_KEY,
	exp: ((new Date()).getTime() + 5000)
}
const token = jwt.sign(payload, process.env.ZOOM_API_SECRET)

//console.log('token', token)

exports.getMeeting = (req, res) => {
  var options = {
		method: 'GET',
		url: 'https://api.zoom.us/v2/meetings/99958491758',
		headers: {
			authorization: 'Bearer ' + token
		}
	};
	
	request(options, function (error, response, body) {
		if (error) {
			res.status(500).send(error)
		} else {
			res.status(200).send(body)
		}
		// console.log(body)
	})
}