const passport = require('passport'),
	BasicStrategy = require('passport-http').BasicStrategy,
	plsql= require('../../node_modules/oracledb-plsql/oracle-plsql'),
	express = require('express')
	router = express.Router()

let cid

passport.use(new BasicStrategy(function (username, password, done) {
	// console.log('auth ws', username, password)
	if (username === 'rest' && password === 'user')
		done(null, { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: cid })
	else
		done('Username o password errati', null)
}))

// versione come per la local auth, non funziona come dovrebbe però
/* router.get('/test', function (req, res, next) {
	passport.authenticate('basic', {
		session: false
	}, function (err, user) {
		if (err) res.status(500).send('Username o password errati')
		else {
			console.log('user', req.user, 'err', err)
			res.status(200).send([
				{ oggetto: 'uno' },
				{ oggetto: 'due' },
				{ oggetto: 'tre' },
			])
		}
	})(req, res, next)
}) */


//aggiunge un gruppo (tag)
/**
 * @swagger
 * tags:
 *   name: Elenco
 */


router.get('/test', function (req, res, next) {
	cid = req.hostname
	console.log('user', req.user, 'err')
	res.status(200).send([
		{ oggetto: 'uno' },
		{ oggetto: 'due' },
		{ oggetto: 'tre' },
	])
})

/**
 * @swagger
 * /rest/api/utenti:
 *  get:
 *    tags: [Elenco]
 *    description: Restituisce la lista utenti
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/utenti', function (req, res, next) {
	cid = req.hostname
	req.params.pkg = 'no_utente'
	req.query = {}
	plsql.readAllRoute(req, res)
})

/**
 * @swagger
 * /rest/api/canali:
 *  get:
 *    tags: [Elenco]
 *    description: Restituisce la lista canali
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/canali', function (req, res, next) {
	cid = req.hostname
	req.params.pkg = 'no_canale'
	req.query = {}
	plsql.readAllRoute(req, res)
})


/* router.get('/canali', passport.authenticate('basic', { session: false }), function (req, res, next) {
	cid = req.hostname
	console.log('host', cid)
	req.params.pkg = 'no_canale'
	req.query = {}
	plsql.readAll(req, res)
}) */

module.exports = router
