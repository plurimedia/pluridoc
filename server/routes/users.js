const sha1= require('sha1'),
    GSON= require('gson'),
    zlib = require('zlib'),
    _= require('underscore'),
    moment= require('moment'),
    Q= require('q'),
    conf = require('../config'),
    fs= require('fs'),
    oracledb = require('oracledb'),
    plsql = require('oracledb-plsql'),
    mail_route= require('../routes/email'),
    passport = require('passport'),
    LocalStrategy = require('passport-local'),
    express = require('express')
    router = express.Router(),
    request = require('request')


oracledb.autoCommit = true;

var formatUser= function (usr) {
    if (!usr) return;

    if (usr.nome)
       usr.nome = usr.nome.replace("'", "\'");

    return JSON.stringify(usr);
}

router.get('/rest/utenti/login_log/:id', async (req, res) => {
    try {
        const results = await plsql.readAll({ utente_id: req.params.id, tipo: 'LOGIN' }, 'no_logs', req.user, {})
        var txt_result = ''
        moment.locale('it')
        results.forEach(function(el) { txt_result = txt_result + moment(el.cdate, 'DD/MM/YYYY HH:mm').format('D/MMM/YYYY , h:mm:ss a') + "\r"; })
        res.set({"Content-Disposition":"attachment; filename=\"log.txt\""})
        res.status(200).send(txt_result)
    } catch (err) {
        res.status(500).send(err)
    }
})

router.post('/rest/user/password_request', async (req, res) => {
    var ut = {};

    const update_user = async () => {
        ut = await plsql.method('mail_request', 'no_utente', { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname }, { mail: req.body.email })
    }

    invia_mail = function () {

        req.body.tipo = 'forget-password';
        req.body.utente = ut;
        req.body.email = { a: [{ mail:req.body.email }] };
        req.user = ut;
        mail_route.send(req, res);
    }

    try {
        await update_user()
        invia_mail()
        delete req.user;
        res.status(200).send({esito: 'Richiesta completata'});
    } catch (err) {
        console.log(err);
        delete req.user;
        res.status(404).send('Si è verificato un Errore');
    }
})

router.post('/rest/user/reset_password', async (req, res) => {
    var ut = {};

    const update_user = async () => {
      ut = await plsql.method('create_password', 'no_utente', { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname }, req.body)
    }

    try {
        await update_user()
        res.status(200).send({esito: 'Richiesta completata'})
    } catch (err) {
        console.log(err)
        res.status(404).send('Si è verificato un Errore')
    }
})

require('../config/passport')(passport)

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, _authenticate));

function _authenticate (req, username, password, done) {
    // console.log('authenticate', req.body, req.spidLogin)
    let sql 
    if (req.spidLogin)
        sql = "BEGIN :ret := auth.authenticate_spid(:par); END;"
    else
        sql = "BEGIN :ret := auth.authenticate(:par); END;"

    oracledb.getConnection(conf.oracle_cn,
        function(err, connection)
        {
            if (err)
            {
                console.error("authenticate err 1 " + err.message);
                return({error: err});
            }
            connection.execute(
                sql,
                {  par: { val:JSON.stringify({ cid: req.company._id, email: username, password: password }), dir: oracledb.BIND_IN, type: oracledb.STRING, maxSize:32000 },
                   ret: { dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize:32000 }
                },
                function (err, result)
                {
                    if (err)
                    {
                        console.error("authenticate err 2 " + err.message);
                        connection.release(
                        function(err)
                        {
                            if (err) console.error(err.message);
                        });
                        // res.status(500).send({message:err.message,sql:sql});
                        done(JSON.stringify({message:err.message,sql:sql}))
                        console.log(sql);
                    }
                    else
                    {
                        if (result && result.outBinds && result.outBinds.ret) {
                            var user = JSON.parse(result.outBinds.ret);
                            //TODO : pezza per far si che funzioni, altrimenti si schianta, non ho capito dove...magari indagare...
                            //user.id_numerico = user._id;
                            //user._id = user.mail;
                            connection.release(
                                function(err)
                                {
                                    if (err) console.error(err.message);
                                });

                            //if (_.contains(user.companies,req.company._id))
                            done(null, user)
                        }
                        else
                        {
                            connection.release(
                                function(err)
                                {
                                    if (err) console.error(err.message);
                                });

                            // res.send();
                            done('nulla di fatto?')
                        }
                    }
                });
        });
}

const authenticateCallback = (err, user) => {
    return new Promise((resolve, reject) => {
        // console.log('user', user, 'err', JSON.parse(err))
        if (!err) {
            user.expires = new Date().getTime() + 2592000000 // sono 30 giorni, occhio. Valore piu accettabile 7200000 (2 ore)
            zlib.gzip(new Buffer.from(GSON.stringify(user),'utf8'), function (err, result)
            {
                if (err)
                    reject({message:err.message})
                else
                {
                    resolve({ user, sessionUser: result.toString('base64') })
                }
            })
        } else {
            reject(JSON.parse(err))
        }
    })
}

router.post('/rest/login', function (req, res, next) {
    passport.authenticate('local', async (err, userAuth) => {
        try {
            const { user, sessionUser } = await authenticateCallback(err, userAuth)
            req.session.user = sessionUser
            res.status(200).send(user)
        } catch (e) {
            res.status(500).json(e)
        }
    })(req, res, next)
})

const makeSpidRequest = (urlApi, body, method = 'POST') => {
    return new Promise((resolve, reject) => {
        const token = process.env.SPID_SECRET_TOKEN // `YmyairiJyA7FSs+KuZf4+7j8zmnw7UHZdW8DonKogM1PRSxaEG2bd3lCuOLSmZyK/bwEV8LiDb/gXgABPg4gaouchNq8hTf6VDtrFWJ9M3dye3v0PqGpP0b7uBL51fEb/OSySN0Ki7T6CYnDpOMCfgFKe9iq18HeWJLWdNcMEPAm3SXnbeU5pU/MWhx79z2ubrpgDmGHREy1SAHRRbogd3aS92jtPWOy1koSl4OCcHmHIqaxIOE/WJqXKIeSSU2QAHR84T5luURaTTumQd6i2TkcGFeBUf6sVukeY+muRn4f6rD7rd9YFOVzNbt2C4ajCrE+Xg/hhH9ktw+hkeLoHg==`
        var options = {
            method: method,
            url: baseUrl + urlApi,
            headers: {
                "authorization": `spidSecret ${token}`,
                "Content-Type": 'application/json'
            },
            json: body
        };
        
        request(options, function (error, response, body) {
            if (error) {
                reject(error)
            } else {
                resolve(body.data)
            }
        })
    })
}

const baseUrl = process.env.SPID_BASE_URL
router.post('/rest/entraconspid', async (req, res, next) => {
    try {
        const result = await makeSpidRequest('api/v2/Auth/Token', {
            'applicationId': 'test2',
            'returnURL': 'https://demo.pluridoc.it/rest/returnFromSpid',
            'returnURLError': 'https://demo.pluridoc.it/rest/returnFromSpidError',
            'autoRedirectOnError': true,
            'type': "spid"
        })
        let ret = baseUrl + 'Profile?SpidToken=' + result
        res.status(200).send(ret)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/rest/returnFromSpid', async (req, res, next) => {
    try {
        const user = await makeSpidRequest('api/v2/Auth/GetProfile', {
            'token': req.query.SpidToken
        })
        // console.log('risultato spid profile', user)

        req.spidLogin = true
        req.body.email = user.email
        req.body.password = 'cazzoculo'
        passport.authenticate('local', async (err, userAuth) => {
            // console.log('nella authenticate per spid err ', err, 'user', userAuth)
            try {
                const { user, sessionUser } = await authenticateCallback(err, userAuth)
                req.session.user = sessionUser
                res.redirect('/#!/home')
            } catch (e) {
                res.status(500).json(e)
            }
        })(req, res, next)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/rest/returnFromSpidError', async (req, res) => {
    console.log('return from spid error GET')
    res.status(200).send('ok')
})

router.post('/rest/returnFromSpidError', async (req, res) => {
    console.log('return from spid error POST')
    res.status(200).send('ok')
})

router.get('/rest/user/:rowid', async (req, res) =>
{
    try {
        const results = await plsql.readAll({ rid: req.params.rowid }, 'no_utente', { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname }, {})
        res.status(200).send(results)
    } catch (err) {
        res.status(500).send(err);
    }
})

router.get('/rest/logout', function (req, res)
{
    req.session.user= undefined;
    req.user= { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname }
    zlib.gzip(new Buffer.from(GSON.stringify(req.user), 'utf8'), function (err, result)
    {
        req.session.user= result.toString('base64');
        res.redirect('/');
    });
})

router.get('/rest/get_lang', function (req, res)
{
    res.status(200).send({ lang: conf.language });
})

module.exports = router


/*
versione SENZA passport

router.post('/rest/login', function (req, res) {
    _authenticate(req.body.email, req.body.password, function(err, user) {
        if (err) res.status(500).send(err)
        else {
            zlib.gzip(new Buffer(GSON.stringify(user),'utf8'), function (err, result)
            {
                if (err)
                    done({message:err.message})
                else
                {
                    req.session.user= result.toString('base64');
                    res.status(200).send(user)
                }
            });
        }
    })
})
*/
