var { expressjwt: jwt } = require("express-jwt")
const express = require('express'),
  router = express.Router(),
  plsql = require('oracledb-plsql'),
  jwks = require('jwks-rsa');

const fs = require('fs');
const jose = require('node-jose');

// crea la rotta che restituisce le chiavi pubbliche contro cui autenticare le richieste
router.get('/.well-known/jwks.json', async (req, res) => {
  const ks = fs.readFileSync('keys.json');

  const keyStore = await jose.JWK.asKeyStore(ks.toString());

  res.send(keyStore.toJSON());
});

// crea la rotta che, con basic auth, restituisce i token da allegare alle richieste server
router.get('/tokens', async (req, res) => {
  if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
    return res.status(401).json({ message: 'Missing Authorization Header' });
  }

  const base64Credentials =  req.headers.authorization.split(' ')[1];
  const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
  const [username, password] = credentials.split(':');

  if ((username === 'BACKEND' && password === 'eda313c994e10b11fc50c90224265c73fbf5b04f0914d89b934a192b28246b8c') || 
    (username === 'LOGUTENTI' && password === 'hheyyg37756639fjjtuhstger9988200gooth7fudhh53620967gdte51242l98v')) {

    const JWKeys = fs.readFileSync('keys.json');

    const keyStore = await jose.JWK.asKeyStore(JWKeys.toString());

    const [key] = keyStore.all({ use: 'sig' });

    const opt = { compact: true, jwk: key, fields: { typ: 'jwt' } };

    const payload = JSON.stringify({
      exp: Math.floor((Date.now() + 1 * 24 * 60 * 60 * 1000) / 1000),
      iat: Math.floor(Date.now() / 1000),
      sub: 'test',
    });

    const token = await jose.JWS.createSign(opt, key).update(payload).final();

    res.status(200).send({ token });
  } else {
    res.status(401).json({ message: 'Non autorizzato' })
  }
});

var authenticateRevenge = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: 'https://extranet.brianzacque.it/ext-auth/.well-known/jwks.json'
  }),
  algorithms: ['RS256']
});

router.use('/secure', authenticateRevenge)

// rotte protette
router.get('/secure/utenti', async (req, res, next) => {
   try {
      const clob_string = await plsql.method('public_',
        'no_utente',
        { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname },
        {})
      res.status(200).send(clob_string)
   } catch (err) {
      res.status(500).send(err)
   }
})

const loginEntries = async (req, res, next) =>
{
   let pars = { da: req.params.daData }
   if (req.params.aData) pars.a = req.params.aData

   try {
      const clob_string = await plsql.method('public_', 
         'no_logs', 
         { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname }, 
         pars)

      res.status(200).send(clob_string)
   } catch (err) {
      res.status(500).send(err)
   }
}

router.get('/secure/loginEntries/:daData/:aData', loginEntries)
router.get('/secure/loginEntries/:daData', loginEntries)

module.exports = router
