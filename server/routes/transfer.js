const file = require('../lib/file')

exports.upload = async (req, res, next) => {
  try {
    file.signedUrl(req.body, req.hostname + '/' + req.body.percorso, async (signedUrl) =>
      {
          res.status(200).send(signedUrl);
      })
  } catch (error) {
    console.error(error);
    res.status(500).send({message: error})
 }
}
