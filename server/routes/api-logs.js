const /* conf = require('../config'),
    oracledb = require('oracledb'), */
    plsql= require('oracledb-plsql');

/* oracledb.autoCommit = true; */

/* function doRelease(connection)
{
   connection.release(function(err)
   {
      if (err) console.error('release() error', err.message);
   });
} */

exports.loginEntries = async (req, res, next) =>
{
   // const sql = "begin nodeoracle_gateway.method(:par, 'no_logs.public_', :user, :ret); end;";
   let pars = { da: req.params.daData }
   if (req.params.aData) pars.a = req.params.aData

   const clob_string = await plsql.method('public_', 
      'no_logs', 
      { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname }, 
      pars)

   res.status(200).send(clob_string)

   /* try {
      oracledb.getConnection(conf.oracle_cn, function(err, connection)
      {
         if (err)
         {
            console.error(err.message);
            res.status(500).send({message: err.message, sql: sql});
         }

         connection.execute(sql,
         {
            par: {
               val: JSON.stringify(pars),
               dir: oracledb.BIND_IN, type: oracledb.STRING,
               maxSize:32000
            },
            user: {
               val: JSON.stringify({ nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname }),
               dir: oracledb.BIND_IN, type: oracledb.STRING,
               maxSize: 32000
            },
            ret: { dir: oracledb.BIND_OUT, type: oracledb.CLOB }
         },
         function(err, result)
         {
            if (err)
            {
               console.error('download err 2 ' + err.message);
               doRelease(connection);
               res.status(500).send({message: err.message, sql: sql});
            }
            else
            {
               if (result && result.outBinds && result.outBinds.ret)
               {
                  var lob = result.outBinds.ret;

                  var clob_string = '';
                  lob.setEncoding('utf8');

                  lob.on('data', function(chunk)
                  {
                     clob_string += chunk;
                  });

                  lob.on('close', function (err)
                  {
                     if (err)
                     {
                        doRelease(connection);
                        next(new Error('Server Error' + err.message));
                        return;
                     }

                     doRelease(connection);
                     res.status(200).send(clob_string);
                  });

                  lob.on('error', function(err)
                  {
                     console.log('lob error event: ' + err.message, sql, req.path);
                     res.status(500).send({ message: err.message, sql: sql });
                     doRelease(connection);
                  });
               }
               else
               {
                  doRelease(connection);
                  res.status(500).send({sql: sql});
               }
            }
         });
      });
   } catch(e) {
      return next(new Error(e));
   } */
}
