const file = require('../lib/file'),
   plsql = require('oracledb-plsql'),
   ILovePDFApi = require('@ilovepdf/ilovepdf-nodejs'),
   ilovepdf = new ILovePDFApi("project_public_f3efa95f75834df8ec27e7553d2d6e66_YtrzTea934bff535a3ea5ba1de8fbfbe74401", "secret_key_f8ac180265c488a4352d37bf0bb5e475_9Fd7od0925058e905c864e48347e8bb89f219"),
   fs = require('fs'),
   request = require('request')


const nomeCompresso = (nome) => {
   return nome.substring(0, nome.lastIndexOf('.')) + 'COMPRESSO.' + nome.substring(nome.lastIndexOf('.') + 1)
}

exports.download = async (req, res) => {
   /*var file_size = 0
   var file_name = ''
   var file_name_orig = ''*/

   const process = async () => {
      const doc = await plsql.read(req.params.id, 
         'no_documento', 
         req.user,
         {})

      if (doc.deleted_flg)
      {
         // secondo me dovrebbe solo tirare un errore e gestire il send nella callback di errore fuori
         res.status(404).send({ message: 'Documento non trovato' });
         // deferred.reject();
      }
      else
      {
         let filename = req.company.cid + '/' + req.params.cid + '/' + doc.name
         if (req.params.compress && req.params.compress === 'Y' && doc.name_compressed)
            filename = req.company.cid + '/' + req.params.cid + '/' + doc.name_compressed

         file.signedUrlDownload(doc.name_orig,
            filename, 
            req.params.attach === 'Y' ? 'attachment' : 'inline',
            (signedUrl) => {
               res.redirect(signedUrl)
            }
         )

         /* if (req.params.attach === 'Y')
            res.setHeader('Content-Disposition', 'attachment; filename*=UTF-8\'\''+encodeURIComponent(doc.name_orig));
         else
            res.setHeader('Content-Disposition', 'inline; filename*=UTF-8\'\''+encodeURIComponent(doc.name_orig));

         res.setHeader('Content-Type', doc.mimetype || 'application/octet-stream');
         res.charset = 'UTF-8';
         file_size = doc.size;
         file_name = doc.name;
         file_name_orig = doc.name_orig; */
         // deferred.resolve();
      }
   }

   const log_download = async () => {
      await plsql.create('no_logs', req.user, {
         tipo:'DOWNLOAD',
         documento_id: req.params.id
      })
   }

   // con il nuovo sistema di redirect a s3 questa diventa inutile
   /* const file_stream = () => {
      file.read(req.company.cid + '/' + req.params.cid + '/' + file_name, function(rstream)
      {
         if (rstream.statusCode==404)
         {
            console.log('non trovato doc', req.company.cid + '/' + req.params.cid + '/' + file_name);
            res.status(404).send('Documento non trovato');
         }
         else
            rstream.pipe(res);
      });
   } */

   try {
      await process()
      await log_download()
      /* await file_stream() */
   } catch (error) {
      console.error(error);
      res.status(500).send('Si è verificato un errore');
   }

}

exports.delete = async (req, res) => {
   try {
      const doc = await plsql.method('can_delete', 'no_documento', req.user, { doc: req.params.id })

      file.delete(req.company.cid+'/'+req.params.cid+'/'+doc.name, async (rstream) =>
      {
         if (doc.name_compressed) {
            file.delete(req.company.cid+'/'+req.params.cid+'/'+doc.name_compressed, async (rstream) =>
            {})
         }

         await plsql.method('elimina', 'no_documento', req.user, { doc: req.params.id })
         res.status(200).send({ message:'delete ok' });
      })
   } catch (error) {
      console.error(error);
      res.status(500).send({message: error})
   }
}

exports.compressPdf = async (req, res) => {
   const files = req.body
   function fixedEncodeURIComponent(str) {
      return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
         return '%' + c.charCodeAt(0).toString(16);
      });
   }

   const elabora = async (f) => {
      const doc = await plsql.read(f.documento_id, 
         'no_documento', 
         req.user,
         {})
      let urls3 = req.company.cid + '/' + doc.canale_id + '/' + doc.name

      const task = ilovepdf.newTask('compress')
      let a = await task.start()
      try {
         await task.addFile('https://pluridoc.s3.eu-west-1.amazonaws.com/' + fixedEncodeURIComponent(urls3))
      } catch (e) {
         console.error('Errore durante la compressione', e.response?.status)
         await plsql.create('no_logs', req.user, {
            tipo:'COMPRESS',
            dati: JSON.stringify(e)
         })
      }
      await task.process()
      const data = await task.download()
      // fs.writeFileSync('scripts/' + doc.name, data) // per debug posso farmi scrivere il file in locale

      file.put(req.company.cid + '/' + doc.canale_id + '/' + nomeCompresso(doc.name), 
         data, 
         doc.mimetype, 
         async (response) => {
            console.log('upload su s3 riuscito')

            await plsql.method('name_compressed', 'no_documento', req.user, { id: f.documento_id, name_compressed: nomeCompresso(doc.name) })
         }
      )
   }

   for (const f of files) {
      try {
         elabora(f)
      } catch (e) {
         console.error('Errore durante la compressione', e.response?.status)
         await plsql.create('no_logs', req.user, {
            tipo:'COMPRESS',
            dati: JSON.stringify(e)
         })
      }
   }
   res.status(200).send('ok')
}

/*const init = () => {
   const data = fs.readFileSync('scripts/167413612968870324_1.pdf')
   file.put('localhost/12/167413612968870324_1COMPRESSO.pdf', data, 'application/pdf', (response) => {
      console.log(response)
   })
   /* file.signedUrl({ tipo: 'application/pdf' }, 'localhost/12/167413612968870324_1COMPRESSO.pdf',
      (signedUrl) => {
         var options = {
            method: 'PUT',
            url: signedUrl,
            headers: {
               'Content-Type': 'multipart/form-data'
            },
            formData: data
         };
         
         try {
            console.log(options)
            request(options, function (error, response, body) {
               if (error) {
                  console.error('Errore durante la compressione', error)
                  
               } else {
                  console.log('upload su s3 riuscito')
               }
               // console.log(body)
            })
            console.log('dopo request')
         } catch(e) {
            console.log(e)
         }
      }
   ) * /
}*/
