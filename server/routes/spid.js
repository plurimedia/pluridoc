const passport = require('passport'),
  SpidStrategy = require('spid-passport'),
  fs = require('fs'),
  express = require('express')
  router = express.Router()

// console.log(fs.readFileSync("../spid-testenv2/conf/idp.crt", "utf-8"))
const spidStrategy = new SpidStrategy({
  sp: {
    callbackUrl: "http://localhost:8080/spid/acs",
    issuer: "http://localhost:8080",
    privateCert: fs.readFileSync("./server/routes/certs/key.pem", "utf-8"), // vi conf/idp.key sul progetto di esempio idp
    decryptionCert: fs.readFileSync("./server/routes/certs/key.pem", "utf-8"),
    decryptionPvk: fs.readFileSync("../spid-testenv2/conf/idp.crt", "utf-8"),
    attributeConsumingServiceIndex: 1,
    identifierFormat: "urn:oasis:names:tc:SAML:2.0:nameid-format:transient",
    authnContext: "https://www.spid.gov.it/SpidL1",
    attributes: {
      name: "Required attributes",
      attributes: ["fiscalNumber", "name", "familyName", "email"]
    },
    organization: {
      name: "Plurimedia",
      displayName: "Plurimedia SRL",
      URL: "https://www.plurimedia.it"
    }
  },
  idp: {
    test: {
      entryPoint: "http://localhost:8088/sso",
      cert: "MIIDCzCCAfOgAwIBAgIJAMWEV7c+E3zNMA0GCSqGSIb3DQEBCwUAMA0xCzAJBgNV BAYTAklUMB4XDTIwMDUxMjExMjg0OVoXDTIwMDYxMTExMjg0OVowDTELMAkGA1UE BhMCSVQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDIfAbUsaoTeoFd aW5+TIyYtkG2ilrAQSccIQJzlT9lZD9TTZQfG8ARreWTVrgV3PkKFOKQrY+QXmhd n+U7F2NG+T3YE6wp77xYuHVjLAXK0vCWh24wRko9C23+I3unkAesh0i+Eos2oDUw 7HTDynrbF2cGrOCJsaHtCD5RTklP4CUDm0o4VCA+9GY81KEZrfKuujd3DAqZ9zOM /q77XrQEK9PdCp2gVW43ZaCuiycnw/WGvEJJtAkhVNjMlg9IapNhotMaCecOE03w vwm+jf2ShzHw9tJjnXO1SewPyDVxROf+I+eCrNEK+WQu012e2X+Ja+rA7jhgnUgH o2J5lM9/AgMBAAGjbjBsMB0GA1UdDgQWBBQf8gsaLfdPzdezIWoeIThIcaCQmDA9 BgNVHSMENjA0gBQf8gsaLfdPzdezIWoeIThIcaCQmKERpA8wDTELMAkGA1UEBhMC SVSCCQDFhFe3PhN8zTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQCz RSknLf3HlwN+qCTF0F9VzXqfb87xAifdts/b/Bjk5LKfZU1Cj96UTG0yUXOarVNo BDovuOn5r5b0e72UXaD+LdQhvGGhfPF+MLTBRdA5AXKy1w7GH8Lohqy823iCvxR9 ryX5JAeJWxwkclM+Lvv8BfLGQT6mIjNvC+Dnglmz13Kgy0GvMdSaGzYcmUwj3rtg OwsAuD6tfw45OWEj/vDrHWKo52qIXyp1haGJhoDqTyM5Oj/S3qdsTg0Pf8ZsQEuC ocQPR6x9DyjuTE4Gv4+EtIkKBa5uHd7Vd4YXkH2RQISrShLzI13DHZ8pcYb4KoEC uHs+l7hqb7FdOQqkmCw5" // preso nella pagina /metadata dell'idp di esempio
    }
  }
}, function(profile, done){

  // Find or create user
  console.log('callback strategy', profile)
  done(null, profile);
})
passport.use(spidStrategy)

router.get('/login', passport.authenticate('spid'))

router.post('/acs',
  passport.authenticate('spid', {session: false}),
  function(req, res){
    console.log(req.user)
    res.send(`Hello ${req.user.name_id}`)
  })

// Create xml metadata
router.get('/metadata', spidStrategy.generateServiceProviderMetadata())

router.get('/logout', passport.authenticate('basic', { session: false }), function (req, res, next) {
	cid = req.hostname
	console.log('host', cid)
	req.params.pkg = 'no_utente'
	req.query = {}
	plsql.readAll(req, res)
})

module.exports = router
