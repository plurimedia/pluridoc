exports.get = function (req, res) {
  res.writeHead(200, {"Content-Type": "text/css"});
  res.write(':root { --colorPrimary: #31b7bc; }');
  res.end();
}