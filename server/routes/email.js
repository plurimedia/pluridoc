var CONFIG = require(process.cwd() + '/server/config.js'),
    mandrill = require('mandrill-api/mandrill'),
    mandrill_client = new mandrill.Mandrill(CONFIG.MANDRIL_API_KEY),
    fs = require('fs'),
    jsrender= require('../../node_custom/jsrender'),
    plsql = require('oracledb-plsql'),
    usr =  require('../middleware/user'),
    _ = require('underscore');

exports.send = function (req, res)
{
  var tipoEmail = req.body.tipo,
    emailContent = req.body.contenuto, // contenuto della mail
    emailParams = req.body.email, // parametri di invio
    _to = function () {
      var to = [],
        _formattaIndirizzi = function (indirizzi, tipo) {
          return _.map(indirizzi, function (ind) {
            return {
              email: ind.mail ? ind.mail : ind, //gli indirizzi mi possono arrivare sotto forma di array di utenti, o di array di email
              type: tipo
            }
          })
        };
      // mandrill li vuole in questo formato
      if (emailParams.a.length)
        to = _.union(to, _formattaIndirizzi(emailParams.a, 'to'));
      if (emailParams.cc && emailParams.cc.length)
        to = _.union(to, _formattaIndirizzi(emailParams.cc, 'cc'));
      if (emailParams.ccn && emailParams.ccn.length)
        to = _.union(to, _formattaIndirizzi(emailParams.ccn, 'bcc'));

      return to;
    },
    // mi arrivano array di nomifiles, creo l'url
    _fileUrls = function (files) {
      return _.map(files, function (f) {
        return {
          url: CONFIG.AWS_S3_BUCKET_PATH + '/' + emailContent._id + '/' + f,
          name: f
        }
      })
    },
    templateContenuto;

  // stabilisco il template del contenuto in base al tipo di mail
  var Tmail = {};
  var from = req.company.mail_from_name;
  if (tipoEmail === 'caricamento')
  {
    var notifica = {};
    notifica.company = req.company;
    notifica.documenti = req.body.documenti.documenti;
    notifica.utente = req.user;
    notifica.folder = req.body.contenuto.folder;
    if (!req.body.contenuto.channel.mail_nuovi_docs)
      req.body.contenuto.channel.mail_nuovi_docs = 'Sono stati inseriti nuovi documenti a te riservati';
    notifica.channel = req.body.contenuto.channel;
    if (req.body.contenuto.channel.mail_from_name)
      from = req.body.contenuto.channel.mail_from_name;
    Tmail= {
      html: jsrender.compile(fs.readFileSync('server/template/mail-'+tipoEmail+'-intranetpluri.html','utf8')),
      text: jsrender.compile(fs.readFileSync('server/template/mail-'+tipoEmail+'-intranetpluri.txt','utf8'))
    };
    notifica.subject = 'Nuovo documento caricato su Pluridoc';
  }
  else if (tipoEmail === 'transfer')
  {
    var notifica = { company: req.company,
      file: 'https://s3-eu-west-1.amazonaws.com/' + CONFIG.bucket + '/' + req.hostname + '/' + req.body.contenuto.file,
      utente: req.user,
      testo: req.body.testo
    };
    Tmail= {
      html: jsrender.compile(fs.readFileSync('server/template/mail-'+tipoEmail+'-intranetpluri.html','utf8')),
      text: jsrender.compile(fs.readFileSync('server/template/mail-'+tipoEmail+'-intranetpluri.txt','utf8'))
    };
    notifica.subject = 'Nuovo documento condiviso';
  }
  else if (tipoEmail === 'forget-password')
  {
    req.body.utente.rowid = encodeURIComponent(req.body.utente.rowid);
    req.user = { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname };
    var notifica = { company: req.company, utente: req.body.utente };
    Tmail= {
      html: jsrender.compile(fs.readFileSync('server/template/mail-'+tipoEmail+'-intranetpluri.html','utf8')),
      text: jsrender.compile(fs.readFileSync('server/template/mail-'+tipoEmail+'-intranetpluri.txt','utf8'))
    };
    notifica.subject = 'Modifica la tua password su Pluridoc';
  }
  else if (tipoEmail === 'account')
  {
    req.body.utente.rid = encodeURIComponent(req.body.utente.rid);
    var notifica = {};
    notifica.company = req.company;
    notifica.utente = req.body.utente;
    notifica.subject = 'Un nuovo account è stato creato per te su Pluridoc';
    var utenti = [];
    utenti.push(_.pick(req.user, ['email', 'nome']));
    Tmail= {
      html: jsrender.compile(fs.readFileSync('server/template/mail-'+tipoEmail+'-intranetpluri.html','utf8')),
      text: jsrender.compile(fs.readFileSync('server/template/mail-'+tipoEmail+'-intranetpluri.txt','utf8'))
    };
  }

  var message = {
    html: Tmail.html.render(notifica),
    text: Tmail.text.render(notifica),
    subject: notifica.subject,
    from_email: req.company.mail_from,
    from_name: from,
    to: _to(),
    headers: { "Reply-To": req.company.mail_from },
    track_opens: 1,
    track_clicks: 1
  };

  const userBck = req.user // Aggiunta perché dopo il send req.user è undefined???
  mandrill_client.messages.send({
    message: message,
    async: false
  },
    async (result) =>
    {
      req.user = userBck // Aggiunta perché dopo il send req.user è undefined???
      plsql.create('no_mail_log', req.user, { mail: result, json_invio: req.body })
      res.status(200).send();
    },
    function (e) {
      // Mandrill returns the error as an object with name and message keys
      res.status(500).send('Errore mail: ' + e.name + ' - ' + e.message)
    });
}

exports.test = function (req, res) {
   console.log("company nel test", req.company);
   res.status(200).send();
}

/* controlla lo stato di una mail */
exports.stato = function (req, res) {
    mandrill_client.messages.info({
            id: req.params.id
        },
        function (result) {
            res.status(200).send(result)
        },
        function (err) {
            res.status(200).send(err)
        }
    )
}
