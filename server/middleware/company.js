const plsql = require('oracledb-plsql');

exports.load = async function (req, res, next)
{
   if (req.path.indexOf('/rest') === 0)
   {
      // identifica i domini ngrok.com come localhost per demo in localtunnel
      var company = ((req.hostname.indexOf('ngrok.io') != -1) || (req.hostname.indexOf('elasticbeanstalk.com') != -1)) ? 'localhost' : req.hostname;
      const clob_string = await plsql.readAll({ cid: company }, 
         'no_azienda', 
         { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname }, 
         {})

      req.company = clob_string[0]
      next();
   }
   else
      next();
}

exports.get = function (req, res) {
   res.send(req.company);
}
