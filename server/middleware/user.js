var GSON= require('gson'),
   zlib = require('zlib'),
   fs= require('fs');

exports.load= function (req, res, next)
{
   if (req.session.user)
      zlib.gunzip(new Buffer.from(req.session.user, 'base64'), function (err, gson)
      {
         req.user= GSON.parse(gson.toString('utf8'));
         next();
      });
   else
   {
      req.user= { nome: 'Anonimo', mail: 'anonimo@anonimo.it', cid: req.hostname }
      zlib.gzip(new Buffer.from(GSON.stringify(req.user), 'utf8'), function (err, result)
      {
         req.session.user= result.toString('base64');
         next();
      });
   }
}

exports.get= function (req, res)
{
   res.send(req.user);
}

var formatUser= function (usr)
{
   if (!usr) return;

   if (usr.nome)
      usr.nome = usr.nome.replace("'", "\'");

   return JSON.stringify(usr);
}

exports.formatUser = formatUser;
