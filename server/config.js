// Selezione delle variabili utilizzate
require('dotenv-flow').config()
var config = {
   oracle_cn : {
      user         : process.env.DB_USER,
      password     : process.env.DB_PWD,
      connectString: process.env.DB_CNTSTRING
   },
   language: process.env.LANGUAGE || 'it',
   MANDRIL_API_KEY: process.env.MANDRIL_API_KEY,
   bucket: process.env.AWS_BUCKET
}

_setEnv = function()
{
   return config
}
  
// dipende dalla variabile di env "ambiente" settato su heroku
module.exports = _setEnv()
